SELECT
    iqiper.activity_constraint.id,
    iqiper.activity_constraint.created_at,
    iqiper.activity_constraint.updated_at
FROM
    iqiper.activity_constraint
WHERE
    iqiper.activity_constraint.updated_at >= $1
    AND iqiper.activity_constraint.updated_at > iqiper.activity_constraint.created_at
ORDER BY
    iqiper.activity_constraint.created_at ASC,
    iqiper.activity_constraint.id ASC
LIMIT $2;


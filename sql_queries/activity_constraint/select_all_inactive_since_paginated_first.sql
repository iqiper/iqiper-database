SELECT
    iqiper.activity_constraint.id,
    iqiper.activity_constraint.created_at,
    iqiper.activity_constraint.updated_at
FROM
    iqiper.activity_constraint
    INNER JOIN iqiper.activity ON iqiper.activity.id = iqiper.activity_constraint.activity_id
WHERE (iqiper.activity_constraint.start_date <= $1
    AND iqiper.activity_constraint.end_date > $1
    AND iqiper.activity_constraint.end_date <= $2)
    OR (iqiper.activity.end_date IS NOT NULL
        AND iqiper.activity.end_date <= $2)
ORDER BY
    iqiper.activity_constraint.created_at ASC,
    iqiper.activity_constraint.id ASC
LIMIT $3;


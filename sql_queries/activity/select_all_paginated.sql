SELECT
    iqiper.activity.id,
    iqiper.activity.created_at,
    iqiper.activity.updated_at
FROM
    iqiper.activity
WHERE ((iqiper.activity.created_at, iqiper.activity.id)) > (($1, $2))
    AND iqiper.activity.id != $2
ORDER BY
    iqiper.activity.created_at ASC,
    iqiper.activity.id ASC
LIMIT $3;


SELECT
    iqiper.activity.id,
    iqiper.activity.created_at,
    iqiper.activity.updated_at
FROM
    iqiper.activity
ORDER BY
    iqiper.activity.created_at ASC,
    iqiper.activity.id ASC
LIMIT $1;


use crate::contact::{SelectContact, CONTACT_SELECT_COLUMNS};
use crate::diesel::Connection;
use crate::error::IqiperDatabaseError;
use crate::schema::iqiper::{
    activity_constraint, activity_contact, communication, communication_recipient, contact,
};
use chrono::{DateTime, Utc};
use diesel::pg::expression::dsl::*;
use diesel::{ExpressionMethods, PgConnection, QueryDsl, QueryableByName, RunQueryDsl};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

/// Select communication logs by communication id
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `communication_id` - The uuid of the `communication` record
///
/// # Return Value
/// The communication log record
pub fn select_by_communication_id(
    conn: &PgConnection,
    uid: &Uuid,
    communication_id: &Uuid,
) -> Result<Vec<SelectCommunicationRecipient>, IqiperDatabaseError> {
    Ok(communication_recipient::table
        .filter(
            communication_recipient::columns::communication_id.eq(any(communication::table
                .select(communication::columns::id)
                .filter(communication::columns::uid.eq(uid))
                .find(communication_id))),
        )
        .select(COMMUNICATION_RECIPIENT)
        .order_by((
            communication_recipient::columns::communication_id,
            communication_recipient::columns::contact_id,
        ))
        .load::<SelectCommunicationRecipient>(conn)?)
}

/// Select communication logs by communication id
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `communication_id` - The uuid of the `communication` record
///
/// # Return Value
/// The communication log record
pub fn select_by_communication_id_with_contacts(
    conn: &PgConnection,
    uid: &Uuid,
    communication_id: &Uuid,
) -> Result<Vec<SelectContact>, IqiperDatabaseError> {
    Ok(communication_recipient::table
        .inner_join(contact::table)
        .filter(
            communication_recipient::columns::communication_id.eq(any(communication::table
                .select(communication::columns::id)
                .filter(communication::columns::uid.eq(uid))
                .find(communication_id))),
        )
        .select(CONTACT_SELECT_COLUMNS)
        .order_by((
            communication_recipient::columns::communication_id,
            communication_recipient::columns::contact_id,
        ))
        .load::<SelectContact>(conn)?)
}

/// Select a single communication log
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `communication_id` - The communication id of the `communication_recipient` record
/// - `contact_id` - The contact id of the `communication_recipient` record
///
/// # Return Value
/// The communication log record
pub fn select_by_id(
    conn: &PgConnection,
    uid: &Uuid,
    communication_id: &Uuid,
    contact_id: &Uuid,
) -> Result<SelectCommunicationRecipient, IqiperDatabaseError> {
    Ok(communication_recipient::table
        .filter(
            communication_recipient::columns::communication_id.eq(any(communication::table
                .select(communication::columns::id)
                .filter(communication::columns::uid.eq(uid)))),
        )
        .select(COMMUNICATION_RECIPIENT)
        .find((communication_id, contact_id))
        .first::<SelectCommunicationRecipient>(conn)?)
}

/// Create a new communication log in the database
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `new_communication_recipient` - The struct containing the new communication log informations
///
/// # Return Value
///  The created record
pub fn create(
    conn: &PgConnection,
    uid: &Uuid,
    new_communication_recipient: NewCommunicationRecipient,
) -> Result<SelectCommunicationRecipient, IqiperDatabaseError> {
    conn.transaction::<SelectCommunicationRecipient, IqiperDatabaseError, _>(|| {
        let (_uid, _created_at) = communication::table
            .filter(communication::columns::uid.eq(uid))
            .select((
                communication::columns::uid,
                communication::columns::created_at,
            ))
            .find(new_communication_recipient.communication_id)
            .first::<(Uuid, DateTime<Utc>)>(conn)?;
        Ok(diesel::insert_into(communication_recipient::table)
            .values(new_communication_recipient)
            .returning(COMMUNICATION_RECIPIENT)
            .get_result::<SelectCommunicationRecipient>(conn)?)
    })
}

/// Create a new communication log in the database from an activity constraint
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `communication_id` - The communication id to create with
/// - `constraint_id` - The constraint id to create from
///
/// # Return Value
///  The created records
pub fn create_from_activity_constraint(
    conn: &PgConnection,
    uid: &Uuid,
    communication_id: &Uuid,
    constraint_id: &Uuid,
) -> Result<Vec<SelectCommunicationRecipient>, IqiperDatabaseError> {
    conn.transaction::<Vec<SelectCommunicationRecipient>, IqiperDatabaseError, _>(|| {
        let communication_id: Uuid = communication::table
            .filter(communication::columns::uid.eq(uid))
            .select(communication::columns::id)
            .find(communication_id)
            .first::<Uuid>(conn)?;
        let activity_contacts: Vec<Uuid> = activity_contact::table
            .filter(
                activity_contact::columns::activity_id.eq(any(activity_constraint::table
                    .select(activity_constraint::columns::activity_id)
                    .find(constraint_id))),
            )
            .select(activity_contact::columns::contact_id)
            .get_results::<Uuid>(conn)?;
        let to_insert: Vec<NewCommunicationRecipient> = activity_contacts
            .into_iter()
            .map(|x| NewCommunicationRecipient {
                communication_id,
                contact_id: x,
            })
            .collect();
        Ok(diesel::insert_into(communication_recipient::table)
            .values(&to_insert)
            .returning(COMMUNICATION_RECIPIENT)
            .get_results::<SelectCommunicationRecipient>(conn)?)
    })
}

/// Create a new communication log in the database from an activity
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `communication_id` - The communication id to create with
/// - `activity_id` - The activity id to create from
///
/// # Return Value
///  The created records
pub fn create_from_activity(
    conn: &PgConnection,
    uid: &Uuid,
    communication_id: &Uuid,
    activity_id: &Uuid,
) -> Result<Vec<SelectCommunicationRecipient>, IqiperDatabaseError> {
    conn.transaction::<Vec<SelectCommunicationRecipient>, IqiperDatabaseError, _>(|| {
        let communication_id: Uuid = communication::table
            .filter(communication::columns::uid.eq(uid))
            .select(communication::columns::id)
            .find(communication_id)
            .first::<Uuid>(conn)?;
        let activity_contacts: Vec<Uuid> = activity_contact::table
            .filter(activity_contact::columns::activity_id.eq(activity_id))
            .select(activity_contact::columns::contact_id)
            .get_results::<Uuid>(conn)?;
        let to_insert: Vec<NewCommunicationRecipient> = activity_contacts
            .into_iter()
            .map(|x| NewCommunicationRecipient {
                communication_id,
                contact_id: x,
            })
            .collect();
        Ok(diesel::insert_into(communication_recipient::table)
            .values(&to_insert)
            .returning(COMMUNICATION_RECIPIENT)
            .get_results::<SelectCommunicationRecipient>(conn)?)
    })
}

/// Delete a communication
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `communication_id` - The communication id of the record to select
/// - `contact_id` - The contact id of the record to select
///
/// # Return Value
///  The number of affected records
pub fn delete(
    conn: &PgConnection,
    uid: &Uuid,
    communication_id: &Uuid,
    contact_id: &Uuid,
) -> Result<usize, IqiperDatabaseError> {
    Ok(diesel::delete(
        communication_recipient::table
            .filter(
                communication_recipient::columns::communication_id.eq(any(communication::table
                    .select(communication::columns::id)
                    .filter(communication::columns::uid.eq(uid)))),
            )
            .find((communication_id, contact_id)),
    )
    .execute(conn)?)
}

type CommunicationRecipientColumns = (
    communication_recipient::columns::communication_id,
    communication_recipient::columns::contact_id,
);

pub const COMMUNICATION_RECIPIENT: CommunicationRecipientColumns = (
    communication_recipient::columns::communication_id,
    communication_recipient::columns::contact_id,
);

#[derive(
    PartialEq,
    Debug,
    Clone,
    Queryable,
    QueryableByName,
    Identifiable,
    Serialize,
    Associations,
    Deserialize,
)]
#[belongs_to(
    crate::communication::SelectCommunication,
    foreign_key = "communication_id"
)]
#[belongs_to(crate::contact::SelectContact, foreign_key = "contact_id")]
#[primary_key(communication_id, contact_id)]
#[table_name = "communication_recipient"]
pub struct SelectCommunicationRecipient {
    #[serde(skip_serializing)]
    pub communication_id: Uuid,
    pub contact_id: Uuid,
}

#[derive(PartialEq, Debug, Clone, Insertable, Serialize)]
#[table_name = "communication_recipient"]
pub struct NewCommunicationRecipient {
    #[serde(skip_serializing)]
    pub communication_id: Uuid,
    pub contact_id: Uuid,
}

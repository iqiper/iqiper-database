pub mod iqiper {
    table! {
        use diesel::sql_types::*;
        use crate::enums::*;

        iqiper.activity (id) {
            id -> Uuid,
            uid -> Uuid,
            name -> Varchar,
            icon -> Nullable<Int4>,
            alert_message -> Nullable<Varchar>,
            start_date -> Timestamptz,
            end_date -> Nullable<Timestamptz>,
            created_at -> Timestamptz,
            updated_at -> Timestamptz,
        }
    }

    table! {
        use diesel::sql_types::*;
        use crate::enums::*;

        iqiper.activity_communication (activity_id, communication_id) {
            activity_id -> Uuid,
            communication_id -> Uuid,
            created_at -> Timestamptz,
        }
    }

    table! {
        use diesel::sql_types::*;
        use crate::enums::*;

        iqiper.activity_constraint (id) {
            id -> Uuid,
            activity_id -> Uuid,
            #[sql_name = "type"]
            type_ -> Activity_constraint_type,
            alert_message -> Nullable<Varchar>,
            start_date -> Timestamptz,
            end_date -> Timestamptz,
            notify_date -> Nullable<Timestamptz>,
            created_at -> Timestamptz,
            updated_at -> Timestamptz,
        }
    }

    table! {
        use diesel::sql_types::*;
        use crate::enums::*;

        iqiper.activity_constraint_communication (constraint_id, communication_id) {
            constraint_id -> Uuid,
            communication_id -> Uuid,
            created_at -> Timestamptz,
        }
    }

    table! {
        use diesel::sql_types::*;
        use crate::enums::*;

        iqiper.activity_constraint_template (id) {
            id -> Uuid,
            activity_template_id -> Uuid,
            #[sql_name = "type"]
            type_ -> Activity_constraint_template_type,
            start_offset -> Int4,
            end_offset -> Int4,
            alert_message -> Nullable<Varchar>,
            notify_offset -> Nullable<Int4>,
            created_at -> Timestamptz,
            updated_at -> Timestamptz,
        }
    }

    table! {
        use diesel::sql_types::*;
        use crate::enums::*;

        iqiper.activity_contact (activity_id, contact_id) {
            activity_id -> Uuid,
            contact_id -> Uuid,
            created_at -> Timestamptz,
        }
    }

    table! {
        use diesel::sql_types::*;
        use crate::enums::*;

        iqiper.activity_contact_template (activity_template_id, contact_id) {
            activity_template_id -> Uuid,
            contact_id -> Uuid,
            created_at -> Timestamptz,
        }
    }

    table! {
        use diesel::sql_types::*;
        use crate::enums::*;

        iqiper.activity_template (id) {
            id -> Uuid,
            uid -> Uuid,
            icon -> Nullable<Int4>,
            name -> Varchar,
            alert_message -> Nullable<Varchar>,
            created_at -> Timestamptz,
            updated_at -> Timestamptz,
        }
    }

    table! {
        use diesel::sql_types::*;
        use crate::enums::*;

        iqiper.communication (id) {
            id -> Uuid,
            uid -> Uuid,
            #[sql_name = "type"]
            type_ -> Communication_type,
            reason -> Communication_reason_type,
            created_at -> Timestamptz,
        }
    }

    table! {
        use diesel::sql_types::*;
        use crate::enums::*;

        iqiper.communication_log (id) {
            id -> Uuid,
            communication_id -> Uuid,
            contact_id -> Uuid,
            status -> Communication_status,
            created_at -> Timestamptz,
        }
    }

    table! {
        use diesel::sql_types::*;
        use crate::enums::*;

        iqiper.communication_recipient (communication_id, contact_id) {
            communication_id -> Uuid,
            contact_id -> Uuid,
        }
    }

    table! {
        use diesel::sql_types::*;
        use crate::enums::*;

        iqiper.contact (id) {
            id -> Uuid,
            nickname -> Varchar,
            favorite -> Bool,
            uid -> Uuid,
            #[sql_name = "type"]
            type_ -> Contact_type,
            email -> Nullable<Varchar>,
            target_uid -> Nullable<Uuid>,
            created_at -> Timestamptz,
            updated_at -> Timestamptz,
        }
    }

    table! {
        use diesel::sql_types::*;
        use crate::enums::*;

        iqiper.distant_user (id) {
            id -> Uuid,
            created_at -> Timestamptz,
        }
    }

    table! {
        use diesel::sql_types::*;
        use crate::enums::*;

        iqiper.fcm_token (uid, device_id) {
            uid -> Uuid,
            device_id -> Uuid,
            token -> Text,
            created_at -> Timestamptz,
            updated_at -> Timestamptz,
        }
    }

    joinable!(activity -> distant_user (uid));
    joinable!(activity_communication -> activity (activity_id));
    joinable!(activity_communication -> communication (communication_id));
    joinable!(activity_constraint -> activity (activity_id));
    joinable!(activity_constraint_communication -> activity_constraint (constraint_id));
    joinable!(activity_constraint_communication -> communication (communication_id));
    joinable!(activity_constraint_template -> activity_template (activity_template_id));
    joinable!(activity_contact -> activity (activity_id));
    joinable!(activity_contact -> contact (contact_id));
    joinable!(activity_contact_template -> activity_template (activity_template_id));
    joinable!(activity_contact_template -> contact (contact_id));
    joinable!(activity_template -> distant_user (uid));
    joinable!(communication -> distant_user (uid));
    joinable!(communication_log -> communication (communication_id));
    joinable!(communication_log -> contact (contact_id));
    joinable!(communication_recipient -> communication (communication_id));
    joinable!(communication_recipient -> contact (contact_id));
    joinable!(fcm_token -> distant_user (uid));

    allow_tables_to_appear_in_same_query!(
        activity,
        activity_communication,
        activity_constraint,
        activity_constraint_communication,
        activity_constraint_template,
        activity_contact,
        activity_contact_template,
        activity_template,
        communication,
        communication_log,
        communication_recipient,
        contact,
        distant_user,
        fcm_token,
    );
}

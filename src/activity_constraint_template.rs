use crate::common::UpdateResponse;
use crate::enums::ActivityConstraintTemplateType;
use crate::error::IqiperDatabaseError;
use crate::schema::iqiper::{activity_constraint_template, activity_template};
use chrono::{DateTime, Utc};
use diesel::{
    Associations, Connection, ExpressionMethods, Insertable, PgConnection, QueryDsl, RunQueryDsl,
};
use serde::{
    de::{self, Deserializer, MapAccess, Visitor},
    Deserialize, Serialize,
};
use std::fmt;
use uuid::Uuid;

/// Check that a constraint respect some basic logic before inserting it into
/// the database.
///
/// # Arguments
/// - `constraint` - The constraint that will be inserted
///
/// # Return Value
/// A result containing nothing
fn check_logic(constraint: &NewActivityConstraintTemplate) -> Result<(), IqiperDatabaseError> {
    if constraint.end_offset < constraint.start_offset {
        return Err(IqiperDatabaseError::LogicError(String::from(
            "The end date can't be lesser than the date start",
        )));
    }
    match constraint.type_ {
        ActivityConstraintTemplateType::Timer => {
            let notify_offset = constraint.notify_offset.as_ref().unwrap();
            if notify_offset <= &constraint.start_offset || notify_offset >= &constraint.end_offset
            {
                Err(IqiperDatabaseError::LogicError(String::from("The notify_offset can't be lesser/equal with start_offset or greater/equal with end_offset")))
            } else {
                Ok(())
            }
        }
    }
}

/// Select every activity constraints template for an user.
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
///
/// # Return Value
///  A vector of activity constraints template
pub fn select_all(
    conn: &PgConnection,
    uid: &Uuid,
) -> Result<Vec<SelectActivityConstraintTemplate>, IqiperDatabaseError> {
    Ok(activity_constraint_template::table
        .inner_join(activity_template::table)
        .filter(activity_template::columns::uid.eq(uid))
        .order((
            activity_constraint_template::columns::created_at.asc(),
            activity_constraint_template::columns::updated_at.asc(),
        ))
        .select(SELECT_COLUMNS)
        .load::<SelectActivityConstraintTemplate>(conn)?)
}

/// Select every activity constraints template's id for an user.
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
///
/// # Return Value
///  A vector of activity constraint template id
pub fn select_all_id(conn: &PgConnection, uid: &Uuid) -> Result<Vec<Uuid>, IqiperDatabaseError> {
    Ok(activity_constraint_template::table
        .inner_join(activity_template::table)
        .filter(activity_template::columns::uid.eq(uid))
        .order((
            activity_constraint_template::columns::created_at.asc(),
            activity_constraint_template::columns::updated_at.asc(),
        ))
        .select(activity_constraint_template::columns::id)
        .load::<Uuid>(conn)?)
}

/// Select every activity constraints template for an activity template.
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `activity_template_id` - The uuid of the `activity_template` record
///
/// # Return Value
///  A vector of activity constraints template
pub fn select_all_by_activity(
    conn: &PgConnection,
    uid: &Uuid,
    activity_template_id: &Uuid,
) -> Result<Vec<SelectActivityConstraintTemplate>, IqiperDatabaseError> {
    Ok(activity_constraint_template::table
        .inner_join(activity_template::table)
        .filter(activity_template::columns::uid.eq(uid))
        .filter(
            activity_constraint_template::columns::activity_template_id.eq(activity_template_id),
        )
        .order((
            activity_constraint_template::columns::created_at.asc(),
            activity_constraint_template::columns::updated_at.asc(),
        ))
        .select(SELECT_COLUMNS)
        .load::<SelectActivityConstraintTemplate>(conn)?)
}

/// Select every activity constraints template's id for an activity template.
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `activity_template_id` - The uuid of the `activity_template` record
///
/// # Return Value
///  A vector of activity constraints template id
pub fn select_all_id_by_activity(
    conn: &PgConnection,
    uid: &Uuid,
    activity_template_id: &Uuid,
) -> Result<Vec<Uuid>, IqiperDatabaseError> {
    Ok(activity_constraint_template::table
        .inner_join(activity_template::table)
        .filter(activity_template::columns::uid.eq(uid))
        .filter(
            activity_constraint_template::columns::activity_template_id.eq(activity_template_id),
        )
        .order((
            activity_constraint_template::columns::created_at.asc(),
            activity_constraint_template::columns::updated_at.asc(),
        ))
        .select(activity_constraint_template::columns::id)
        .load::<Uuid>(conn)?)
}

/// Select a single activity constraint template
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `activity_template_id` - The activity template id
/// - `select_id` - The uuid of the record to pull out of the database
///
/// # Return Value
///  The selected activity constraint template or `None` if not found
pub fn select_by_id(
    conn: &PgConnection,
    uid: &Uuid,
    activity_template_id: &Uuid,
    select_id: &Uuid,
) -> Result<SelectActivityConstraintTemplate, IqiperDatabaseError> {
    Ok(activity_constraint_template::table
        .inner_join(activity_template::table)
        .filter(activity_template::columns::uid.eq(uid))
        .filter(activity_constraint_template::columns::id.eq(select_id))
        .filter(
            activity_constraint_template::columns::activity_template_id.eq(activity_template_id),
        )
        .select(SELECT_COLUMNS)
        .first::<SelectActivityConstraintTemplate>(conn)?)
}

/// Create a new activity constraint template in the database
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `new_constraint` - The struct containing the new activity constraint template informations
///
/// # Return Value
///  The created record
pub fn create(
    conn: &PgConnection,
    uid: &Uuid,
    new_constraint_template: NewActivityConstraintTemplate,
) -> Result<SelectActivityConstraintTemplate, IqiperDatabaseError> {
    check_logic(&new_constraint_template)?;
    conn.transaction(
        || -> Result<SelectActivityConstraintTemplate, IqiperDatabaseError> {
            activity_template::table
                .filter(
                    activity_template::columns::id.eq(new_constraint_template.activity_template_id),
                )
                .filter(activity_template::columns::uid.eq(uid))
                .select(activity_template::columns::id)
                .for_share()
                .first::<Uuid>(conn)?;
            Ok(diesel::insert_into(activity_constraint_template::table)
                .values(new_constraint_template)
                .get_result::<SelectActivityConstraintTemplate>(conn)?)
        },
    )
}

/// Modify an activity constraint template info
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `activity_template_id` - The uuid of the `activity_template` record
/// - `constraint_template_id` - The uuid of the `activity_constraint_template` record
/// - `constraint_template` - The struct containing the new activity constraint template informations
///
/// # Return Value
///  The updated response
pub fn modify(
    conn: &PgConnection,
    uid: &Uuid,
    activity_template_id: &Uuid,
    constraint_template_id: &Uuid,
    constraint_template: NewActivityConstraintTemplate,
) -> Result<UpdateResponse, IqiperDatabaseError> {
    check_logic(&constraint_template)?;
    conn.transaction(|| -> Result<UpdateResponse, IqiperDatabaseError> {
        let type_ = activity_constraint_template::table
            .inner_join(activity_template::table)
            .filter(activity_template::columns::uid.eq(uid))
            .filter(activity_template::columns::id.eq(activity_template_id))
            .filter(activity_constraint_template::columns::id.eq(constraint_template_id))
            .select(activity_constraint_template::columns::type_)
            .for_update()
            .first::<ActivityConstraintTemplateType>(conn)?;
        if type_ != constraint_template.type_ {
            return Err(IqiperDatabaseError::TypeChanging);
        }
        Ok(
            diesel::update(activity_constraint_template::table.find(constraint_template_id))
                .set(&constraint_template)
                .returning((
                    activity_constraint_template::columns::id,
                    activity_constraint_template::columns::updated_at,
                ))
                .get_result::<UpdateResponse>(conn)?,
        )
    })
}

/// Delete an activity constraint template
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `activity_template_id` - The uuid of the activity template this constraint template is bound to
/// - `constraint_template_id` - The uuid of the record to pull out of the database
///
///
/// # Return Value
///  The number of affected records
pub fn delete(
    conn: &PgConnection,
    uid: &Uuid,
    activity_template_id: &Uuid,
    constraint_template_id: &Uuid,
) -> Result<usize, IqiperDatabaseError> {
    Ok(conn.transaction(|| {
        activity_constraint_template::table
            .filter(
                activity_constraint_template::columns::activity_template_id
                    .eq(activity_template_id),
            )
            .select(activity_constraint_template::columns::id)
            .for_update()
            .execute(conn)?;
        let select_activity_template_id = activity_template::table
            .filter(activity_template::columns::uid.eq(uid))
            .find(activity_template_id)
            .select(activity_template::columns::id);
        let select_activity_constraint_template = activity_constraint_template::table
            .filter(
                activity_constraint_template::columns::activity_template_id
                    .eq_any(select_activity_template_id),
            )
            .find(constraint_template_id);
        let size = diesel::delete(select_activity_constraint_template).execute(conn)?;
        match size {
            0 => Err(IqiperDatabaseError::DatabaseError(
                diesel::result::Error::NotFound,
            )),
            1 => Ok(1),
            _ => Err(IqiperDatabaseError::UnexpectedResult(1, size)),
        }
    })?)
}

type ActivityConstraintTemplateColumns = (
    activity_constraint_template::columns::id,
    activity_constraint_template::columns::activity_template_id,
    activity_constraint_template::columns::type_,
    activity_constraint_template::columns::start_offset,
    activity_constraint_template::columns::end_offset,
    activity_constraint_template::columns::alert_message,
    activity_constraint_template::columns::notify_offset,
    activity_constraint_template::columns::created_at,
    activity_constraint_template::columns::updated_at,
);

pub const SELECT_COLUMNS: ActivityConstraintTemplateColumns = (
    activity_constraint_template::columns::id,
    activity_constraint_template::columns::activity_template_id,
    activity_constraint_template::columns::type_,
    activity_constraint_template::columns::start_offset,
    activity_constraint_template::columns::end_offset,
    activity_constraint_template::columns::alert_message,
    activity_constraint_template::columns::notify_offset,
    activity_constraint_template::columns::created_at,
    activity_constraint_template::columns::updated_at,
);

#[derive(PartialEq, Debug, Clone, Queryable, Identifiable, Associations, Serialize, Deserialize)]
#[belongs_to(
    crate::activity_template::SelectActivityTemplate,
    foreign_key = "activity_template_id"
)]
#[table_name = "activity_constraint_template"]
pub struct ActivityConstraintId {
    pub id: Uuid,
    #[serde(skip_serializing)]
    pub activity_template_id: Uuid,
}

#[derive(PartialEq, Debug, Clone, Queryable, Identifiable, Associations, Serialize, Deserialize)]
#[belongs_to(
    crate::activity_template::SelectActivityTemplate,
    foreign_key = "activity_template_id"
)]
#[table_name = "activity_constraint_template"]
pub struct SelectActivityConstraintTemplate {
    pub id: Uuid,
    pub activity_template_id: Uuid,
    #[serde(rename = "type")]
    pub type_: ActivityConstraintTemplateType,
    pub start_offset: i32,
    pub end_offset: i32,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub alert_message: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub notify_offset: Option<i32>,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
}

#[derive(PartialEq, Debug, Clone, Insertable, AsChangeset, Serialize)]
#[table_name = "activity_constraint_template"]
pub struct NewActivityConstraintTemplate {
    #[serde(rename = "type")]
    pub type_: ActivityConstraintTemplateType,
    pub activity_template_id: Uuid,
    pub start_offset: i32,
    pub end_offset: i32,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub alert_message: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub notify_offset: Option<i32>,
}

impl<'de> Deserialize<'de> for NewActivityConstraintTemplate {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        #[derive(Deserialize)]
        #[serde(field_identifier, rename_all = "snake_case")]
        enum Field {
            ActivityId,
            StartOffset,
            EndOffset,
            NotifyOffset,
            AlertMessage,
        }

        struct NewConstraintTemplateVisitor;

        impl<'de> Visitor<'de> for NewConstraintTemplateVisitor {
            type Value = NewActivityConstraintTemplate;

            fn expecting(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
                formatter.write_str("struct NewActivityConstraintTemplate")
            }

            fn visit_map<V>(self, mut map: V) -> Result<NewActivityConstraintTemplate, V::Error>
            where
                V: MapAccess<'de>,
            {
                let mut activity_template_id = None;
                let mut notify_offset = None;
                let mut start_offset = None;
                let mut end_offset = None;
                let mut alert_message = None;
                let mut type_ = None;
                while let Some(key) = map.next_key()? {
                    match key {
                        Field::ActivityId => {
                            if activity_template_id.is_some() {
                                return Err(de::Error::duplicate_field("activity_template_id"));
                            }
                            activity_template_id = Some(map.next_value()?);
                        }
                        Field::NotifyOffset => {
                            // If more type, we should check there is no clash between types
                            if notify_offset.is_some() {
                                return Err(de::Error::duplicate_field("notify_offset"));
                            }
                            type_ = Some(ActivityConstraintTemplateType::Timer);
                            notify_offset = Some(map.next_value()?);
                        }
                        Field::StartOffset => {
                            if start_offset.is_some() {
                                return Err(de::Error::duplicate_field("start_offset"));
                            }
                            start_offset = Some(map.next_value()?);
                        }
                        Field::EndOffset => {
                            if end_offset.is_some() {
                                return Err(de::Error::duplicate_field("end_offset"));
                            }
                            end_offset = Some(map.next_value()?);
                        }
                        Field::AlertMessage => {
                            if alert_message.is_some() {
                                return Err(de::Error::duplicate_field("alert_message"));
                            }
                            alert_message = Some(map.next_value()?);
                        }
                    }
                }
                let activity_template_id = activity_template_id
                    .ok_or_else(|| de::Error::missing_field("activity_template_id"))?;
                let notify_offset =
                    notify_offset.ok_or_else(|| de::Error::missing_field("notify_offset"))?;
                let start_offset =
                    start_offset.ok_or_else(|| de::Error::missing_field("start_offset"))?;
                let end_offset =
                    end_offset.ok_or_else(|| de::Error::missing_field("end_offset"))?;
                let alert_message =
                    alert_message.ok_or_else(|| de::Error::missing_field("alert_message"))?;
                let type_ = type_.ok_or_else(|| de::Error::missing_field("Couldn't guess type"))?;
                Ok(NewActivityConstraintTemplate {
                    activity_template_id,
                    start_offset,
                    end_offset,
                    notify_offset,
                    type_,
                    alert_message,
                })
            }
        }
        deserializer.deserialize_map(NewConstraintTemplateVisitor)
    }
}

use serde::{Serialize, Deserialize};
use getset::{CopyGetters, Setters};

#[derive(Debug, Clone, Serialize, Deserialize, Default, CopyGetters, Setters)]
pub struct IncludeSub {
	#[getset(get_copy = "pub", set = "pub")]
	include_constraints: bool,
	#[getset(get_copy = "pub", set = "pub")]
	include_contacts: bool,
	#[getset(get_copy = "pub", set = "pub")]
	include_communications: bool,
	#[getset(get_copy = "pub", set = "pub")]
	include_communications_logs: bool,
	#[getset(get_copy = "pub", set = "pub")]
	include_communications_recipients: bool,
}

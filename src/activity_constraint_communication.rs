use crate::activity_constraint::SelectActivityConstraint;
use crate::communication::{SelectCommunication, COMMUNICATION_SELECT_COLUMNS};
use crate::schema::iqiper::{
    activity, activity_constraint, activity_constraint_communication, communication,
};
use chrono::{DateTime, Utc};
use diesel::{Connection, ExpressionMethods, Insertable, PgConnection, QueryDsl, RunQueryDsl};

use crate::error::IqiperDatabaseError;

use serde::{Deserialize, Serialize};

use uuid::Uuid;

/// Select all activity constraint communication
///
/// # Arguments
/// - `conn` - The connection object
/// - `uid` - The user id
/// - `activity_id` - The activity id to select
/// - `constraint_id` - The constraint id to select
///
/// # Return Value
///  A list of activity constraint communication
pub fn select_all(
    conn: &PgConnection,
    uid: &Uuid,
    activity_id: &Uuid,
    constraint_id: &Uuid,
) -> Result<Vec<SelectCommunication>, IqiperDatabaseError> {
    Ok(activity_constraint_communication::table
        .inner_join(communication::table)
        .inner_join(activity_constraint::table)
        .filter(communication::columns::uid.eq(uid))
        .filter(activity_constraint::columns::id.eq(constraint_id))
        .filter(activity_constraint::columns::activity_id.eq(activity_id))
        .order_by((
            communication::columns::created_at,
            communication::columns::id,
        ))
        .select(COMMUNICATION_SELECT_COLUMNS)
        .load::<SelectCommunication>(conn)?)
}

/// Select a single activity communication
///
/// # Arguments
/// - `conn` - The connection object
/// - `uid` - The user id
/// - `constraint_id` - The constraint id to select
/// - `communication_id` - The communication id to select
///
/// # Return Value
///  An activity constraint communication
pub fn select_by_id(
    conn: &PgConnection,
    uid: &Uuid,
    constraint_id: &Uuid,
    communication_id: &Uuid,
) -> Result<SelectCommunication, IqiperDatabaseError> {
    Ok(activity_constraint_communication::table
        .inner_join(communication::table)
        .filter(communication::columns::uid.eq(uid))
        .filter(communication::columns::id.eq(communication_id))
        .filter(activity_constraint_communication::columns::constraint_id.eq(constraint_id))
        .order_by((
            communication::columns::created_at,
            communication::columns::id,
        ))
        .select(COMMUNICATION_SELECT_COLUMNS)
        .first::<SelectCommunication>(conn)?)
}

/// Select a single activity constraint from a communication
///
/// # Arguments
/// - `conn` - The connection object
/// - `uid` - The user id
/// - `communication_id` - The communication id
///
/// # Return Value
///  An activity constraint
pub fn select_constraint_by_communication(
    conn: &PgConnection,
    uid: &Uuid,
    communication_id: &Uuid,
) -> Result<SelectActivityConstraint, IqiperDatabaseError> {
    Ok(activity_constraint_communication::table
        .inner_join(activity_constraint::table)
        .inner_join(communication::table)
        .filter(communication::columns::uid.eq(uid))
        .filter(communication::columns::id.eq(communication_id))
        .order_by((
            activity_constraint::columns::created_at,
            activity_constraint::columns::id,
        ))
        .select(crate::activity_constraint::SELECT_COLUMNS)
        .first::<SelectActivityConstraint>(conn)?)
}

/// Create a new activity constraint communication
///
/// # Arguments
/// - `conn` - The connection object
/// - `uid` - The user id
/// - `new_obj` - The object to create
///
/// # Return Value
///  The created record
pub fn create(
    conn: &PgConnection,
    uid: &Uuid,
    new_obj: NewActivityConstraintCommunication,
) -> Result<ActivityConstraintCommunication, IqiperDatabaseError> {
    Ok(conn.transaction(
        || -> Result<ActivityConstraintCommunication, IqiperDatabaseError> {
            let _activity_constraint_id: Uuid = activity_constraint::table
                .inner_join(activity::table)
                .filter(activity::columns::uid.eq(uid))
                .select(activity_constraint::columns::id)
                .for_update()
                .filter(activity_constraint::columns::id.eq(new_obj.constraint_id))
                .first::<Uuid>(conn)?;
            let _communication_id: Uuid = communication::table
                .filter(communication::columns::uid.eq(uid))
                .select(communication::columns::id)
                .for_update()
                .find(new_obj.communication_id)
                .first::<Uuid>(conn)?;
            let res = diesel::insert_into(activity_constraint_communication::table)
                .values(new_obj)
                .returning(SELECT_COLUMNS)
                .get_result::<ActivityConstraintCommunication>(conn)?;
            Ok(res)
        },
    )?)
}

type ActivityConstraintCommmunicationColumns = (
    activity_constraint_communication::columns::constraint_id,
    activity_constraint_communication::columns::communication_id,
    activity_constraint_communication::columns::created_at,
);

pub const SELECT_COLUMNS: ActivityConstraintCommmunicationColumns = (
    activity_constraint_communication::columns::constraint_id,
    activity_constraint_communication::columns::communication_id,
    activity_constraint_communication::columns::created_at,
);

#[derive(
    PartialEq,
    Debug,
    Clone,
    Queryable,
    Identifiable,
    Associations,
    Insertable,
    Serialize,
    Deserialize,
)]
#[belongs_to(
    crate::activity_constraint::SelectActivityConstraint,
    foreign_key = "constraint_id"
)]
#[belongs_to(
    crate::communication::SelectCommunication,
    foreign_key = "communication_id"
)]
#[primary_key(constraint_id, communication_id)]
#[table_name = "activity_constraint_communication"]
pub struct ActivityConstraintCommunication {
    pub constraint_id: Uuid,
    pub communication_id: Uuid,
    pub created_at: DateTime<Utc>,
}

#[derive(PartialEq, Debug, Clone, Queryable, Insertable, Serialize, Deserialize)]
#[table_name = "activity_constraint_communication"]
pub struct NewActivityConstraintCommunication {
    pub constraint_id: Uuid,
    pub communication_id: Uuid,
}

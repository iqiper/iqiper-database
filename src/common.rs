use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize, Queryable)]
pub struct UpdateResponse {
    pub id: Uuid,
    pub updated_at: DateTime<Utc>,
}

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize, Queryable)]
pub struct IdTsResponse {
    pub id: Uuid,
    pub created_at: DateTime<Utc>,
}

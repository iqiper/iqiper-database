use std::fmt;

/// Wrap all kind of error that could be encoutered by the service.
#[derive(Debug, PartialEq)]
pub enum IqiperDatabaseError {
    /// An logic error occured (i.e. trying to start a constraint after its end)
    LogicError(String),
    /// A logic error when trying to modify something that should be read-only
    ReadOnly,
    /// A existing object can be transformed into another type
    TypeChanging,
    /// Wrong number of affected rows. Expected, got.
    UnexpectedResult(usize, usize),
    /// A diesel error
    DatabaseError(diesel::result::Error),
}

impl fmt::Display for IqiperDatabaseError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            IqiperDatabaseError::LogicError(s) => {
                write!(f, "[iqiper-database] (logic error): {}", s)
            }
            IqiperDatabaseError::DatabaseError(e) => {
                write!(f, "[iqiper-database] (database error): {}", e)
            }
            IqiperDatabaseError::TypeChanging => write!(
                f,
                "[iqiper-database] (type changing error): Can't change object from type once set"
			),
			IqiperDatabaseError::ReadOnly => write!(
                f,
                "[iqiper-database] (type read-only): Can't change object which was marked as read-only"
			),
			IqiperDatabaseError::UnexpectedResult(x, y) => write!(
                f,
                "[iqiper-database] (type changing error): Wrong number of affected row, expected {}, got {}", x, y
            ),
        }
    }
}

impl std::error::Error for IqiperDatabaseError {}

impl From<diesel::result::Error> for IqiperDatabaseError {
    fn from(err: diesel::result::Error) -> Self {
        IqiperDatabaseError::DatabaseError(err)
    }
}

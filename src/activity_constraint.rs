use crate::activity::select_activity_for_update;
use crate::common::{IdTsResponse, UpdateResponse};
use crate::enums::ActivityConstraintType;
use crate::error::IqiperDatabaseError;
use crate::schema::iqiper::{activity, activity_constraint};
use chrono::{DateTime, Utc};
use diesel::sql_types::{BigInt, Timestamptz, Uuid as PgUuid};
use diesel::{Connection, ExpressionMethods, Insertable, PgConnection, QueryDsl, RunQueryDsl};
use serde::{
    de::{self, MapAccess, Visitor},
    Deserialize, Deserializer, Serialize,
};
use std::fmt;
use uuid::Uuid;

/// Check that a constraint respect some basic logic before inserting it into
/// the database.
///
/// # Arguments
/// - `constraint` - The constraint that will be inserted
///
/// # Return Value
/// A result containing nothing
fn check_logic(constraint: &NewActivityConstraint) -> Result<(), IqiperDatabaseError> {
    if constraint.end_date < constraint.start_date {
        return Err(IqiperDatabaseError::LogicError(String::from(
            "The end date can't be lesser than the date start",
        )));
    }
    match constraint.type_ {
        ActivityConstraintType::Timer => {
            let notify_date = constraint.notify_date.as_ref().unwrap();
            if notify_date <= &constraint.start_date || notify_date >= &constraint.end_date {
                Err(IqiperDatabaseError::LogicError(String::from("The notify_date can't be lesser/equal with start_date or greater/equal with end_date")))
            } else {
                Ok(())
            }
        }
    }
}

/// Select every activity constraints for an user.
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
///
/// # Return Value
///  A vector of activity constraints
pub fn select_all(
    conn: &PgConnection,
    uid: &Uuid,
) -> Result<Vec<SelectActivityConstraint>, IqiperDatabaseError> {
    Ok(activity_constraint::table
        .inner_join(activity::table)
        .filter(activity::columns::uid.eq(uid))
        .order((
            activity_constraint::columns::created_at.asc(),
            activity_constraint::columns::updated_at.asc(),
        ))
        .select(SELECT_COLUMNS)
        .load::<SelectActivityConstraint>(conn)?)
}

/// Select every activity constraints for every users. (paginated)
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `offset` - The offset at which to begin returning records
/// - `limit` - The number of record to return
///
/// # Return Value
///  A vector of activity constraints
pub fn select_all_paginated_unlogged(
    conn: &PgConnection,
    offset: Option<(Uuid, DateTime<Utc>)>,
    limit: i64,
) -> Result<Vec<ActivityConstraintPaginated>, IqiperDatabaseError> {
    Ok(match offset {
        Some(x) => diesel::sql_query(include_str!(
            "../sql_queries/activity_constraint/select_all_paginated.sql"
        ))
        .bind::<Timestamptz, _>(x.1)
        .bind::<PgUuid, _>(x.0)
        .bind::<BigInt, _>(limit)
        .get_results(conn)?,
        None => diesel::sql_query(include_str!(
            "../sql_queries/activity_constraint/select_all_paginated_first.sql"
        ))
        .bind::<BigInt, _>(limit)
        .get_results(conn)?,
    })
}

/// Select every actvie activity constraints for every users. (paginated)
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `offset` - The offset at which to begin returning records
/// - `limit` - The number of record to return
///
/// # Return Value
///  A vector of activity constraints
pub fn select_all_active_paginated_unlogged(
    conn: &PgConnection,
    offset: Option<(Uuid, DateTime<Utc>)>,
    limit: i64,
) -> Result<Vec<ActivityConstraintPaginated>, IqiperDatabaseError> {
    let time = Utc::now();
    Ok(match offset {
        Some(x) => diesel::sql_query(include_str!(
            "../sql_queries/activity_constraint/select_all_active_paginated.sql"
        ))
        .bind::<Timestamptz, _>(time)
        .bind::<Timestamptz, _>(x.1)
        .bind::<PgUuid, _>(x.0)
        .bind::<BigInt, _>(limit)
        .get_results(conn)?,
        None => diesel::sql_query(include_str!(
            "../sql_queries/activity_constraint/select_all_active_paginated_first.sql"
        ))
        .bind::<Timestamptz, _>(time)
        .bind::<BigInt, _>(limit)
        .get_results(conn)?,
    })
}

/// Select every inactive activity constraints for every users. (paginated)
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `offset` - The offset at which to begin returning records
/// - `limit` - The number of record to return
///
/// # Return Value
///  A vector of activity constraints
pub fn select_all_inactive_since_paginated_unlogged(
    conn: &PgConnection,
    since: DateTime<Utc>,
    offset: Option<(Uuid, DateTime<Utc>)>,
    limit: i64,
) -> Result<Vec<ActivityConstraintPaginated>, IqiperDatabaseError> {
    let time = Utc::now();
    Ok(match offset {
        Some(x) => diesel::sql_query(include_str!(
            "../sql_queries/activity_constraint/select_all_inactive_since_paginated.sql"
        ))
        .bind::<Timestamptz, _>(since)
        .bind::<Timestamptz, _>(time)
        .bind::<Timestamptz, _>(x.1)
        .bind::<PgUuid, _>(x.0)
        .bind::<BigInt, _>(limit)
        .get_results(conn)?,
        None => diesel::sql_query(include_str!(
            "../sql_queries/activity_constraint/select_all_inactive_since_paginated_first.sql"
        ))
        .bind::<Timestamptz, _>(since)
        .bind::<Timestamptz, _>(time)
        .bind::<BigInt, _>(limit)
        .get_results(conn)?,
    })
}

/// Select every active activity constraints for every users since a point in time. (paginated)
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `offset` - The offset at which to begin returning records
/// - `limit` - The number of record to return
///
/// # Return Value
///  A vector of activity constraints
pub fn select_all_active_since_paginated_unlogged(
    conn: &PgConnection,
    since: DateTime<Utc>,
    offset: Option<(Uuid, DateTime<Utc>)>,
    limit: i64,
) -> Result<Vec<ActivityConstraintPaginated>, IqiperDatabaseError> {
    let time = Utc::now();
    Ok(match offset {
        Some(x) => diesel::sql_query(include_str!(
            "../sql_queries/activity_constraint/select_all_active_since_paginated.sql"
        ))
        .bind::<Timestamptz, _>(since)
        .bind::<Timestamptz, _>(time)
        .bind::<Timestamptz, _>(x.1)
        .bind::<PgUuid, _>(x.0)
        .bind::<BigInt, _>(limit)
        .get_results(conn)?,
        None => diesel::sql_query(include_str!(
            "../sql_queries/activity_constraint/select_all_active_since_paginated_first.sql"
        ))
        .bind::<Timestamptz, _>(since)
        .bind::<Timestamptz, _>(time)
        .bind::<BigInt, _>(limit)
        .get_results(conn)?,
    })
}

/// Select every actvie activity constraints for every users. (paginated)
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `offset` - The offset at which to begin returning records
/// - `limit` - The number of record to return
///
/// # Return Value
///  A vector of activity constraints
// TODO
// This function is currently not tested and can't be. The `updated_at` trigger
// will set the same date for every record in the transaction. The only way to
// to really achieve a test with this function would be by modifying the test
// database
pub fn select_all_updated_since_paginated_unlogged(
    conn: &PgConnection,
    since: DateTime<Utc>,
    offset: Option<(Uuid, DateTime<Utc>)>,
    limit: i64,
) -> Result<Vec<ActivityConstraintPaginated>, IqiperDatabaseError> {
    Ok(match offset {
        Some(x) => diesel::sql_query(include_str!(
            "../sql_queries/activity_constraint/select_all_updated_since_paginated.sql"
        ))
        .bind::<Timestamptz, _>(since)
        .bind::<Timestamptz, _>(x.1)
        .bind::<PgUuid, _>(x.0)
        .bind::<BigInt, _>(limit)
        .get_results(conn)?,
        None => diesel::sql_query(include_str!(
            "../sql_queries/activity_constraint/select_all_updated_since_paginated_first.sql"
        ))
        .bind::<Timestamptz, _>(since)
        .bind::<BigInt, _>(limit)
        .get_results(conn)?,
    })
}

/// Select every activity constraints for an activity.
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `activity_id` - The uuid of the `activity` record
///
/// # Return Value
///  A vector of activity constraints
pub fn select_all_by_activity(
    conn: &PgConnection,
    uid: &Uuid,
    activity_id: &Uuid,
) -> Result<Vec<SelectActivityConstraint>, IqiperDatabaseError> {
    Ok(activity_constraint::table
        .inner_join(activity::table)
        .filter(activity::columns::uid.eq(uid))
        .filter(activity_constraint::columns::activity_id.eq(activity_id))
        .order((
            activity_constraint::columns::created_at.asc(),
            activity_constraint::columns::updated_at.asc(),
        ))
        .select(SELECT_COLUMNS)
        .load::<SelectActivityConstraint>(conn)?)
}

/// Select a single activity constraint
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `activity_id` - The uuid of the activity, parent table of the activity constraint
/// - `select_id` - The uuid of the record to pull out of the database
///
/// # Return Value
///  The selected activity constraint or `None` if not found
pub fn select_by_id(
    conn: &PgConnection,
    uid: &Uuid,
    activity_id: &Uuid,
    select_id: &Uuid,
) -> Result<SelectActivityConstraint, IqiperDatabaseError> {
    Ok(activity_constraint::table
        .inner_join(activity::table)
        .filter(activity::columns::uid.eq(uid))
        .filter(activity_constraint::columns::id.eq(select_id))
        .filter(activity_constraint::columns::activity_id.eq(activity_id))
        .select(SELECT_COLUMNS)
        .first::<SelectActivityConstraint>(conn)?)
}

// Select a single activity constraint for every users
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `activity_id` - The uuid of the activity, parent table of the activity constraint
/// - `select_id` - The uuid of the record to pull out of the database
///
/// # Return Value
///  The selected activity constraint or `None` if not found
pub fn select_by_id_unlogged(
    conn: &PgConnection,
    activity_id: &Uuid,
    select_id: &Uuid,
) -> Result<SelectActivityConstraint, IqiperDatabaseError> {
    Ok(activity_constraint::table
        .inner_join(activity::table)
        .filter(activity_constraint::columns::id.eq(select_id))
        .filter(activity_constraint::columns::activity_id.eq(activity_id))
        .select(SELECT_COLUMNS)
        .first::<SelectActivityConstraint>(conn)?)
}

// Select a multiple activity constraints
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `select_ids` - The uuids of the activity constraints to fetch
///
/// # Return Value
///  The selected activity constraint or `None` if not found
pub fn select_bulk_by_id_unlogged(
    conn: &PgConnection,
    select_ids: Vec<Uuid>,
) -> Result<Vec<SelectActivityConstraint>, IqiperDatabaseError> {
    Ok(activity_constraint::table
        .filter(activity_constraint::columns::id.eq_any(select_ids))
        .order((
            activity_constraint::columns::created_at.asc(),
            activity_constraint::columns::updated_at.asc(),
        ))
        .select(SELECT_COLUMNS)
        .load::<SelectActivityConstraint>(conn)?)
}

/// Select a single activity constraint by its unique id.
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `select_id` - The uuid of the record to pull out of the database
///
/// # Return Value
///  The selected activity constraint or `None` if not found
pub fn select_by_unique_id(
    conn: &PgConnection,
    select_id: &Uuid,
) -> Result<SelectActivityConstraint, IqiperDatabaseError> {
    Ok(activity_constraint::table
        .filter(activity_constraint::columns::id.eq(select_id))
        .select(SELECT_COLUMNS)
        .first::<SelectActivityConstraint>(conn)?)
}

/// Select the user id from a constraint
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `select_id` - The uuid of the record to pull out of the database
///
/// # Return Value
/// The user id
pub fn select_uid(conn: &PgConnection, select_id: &Uuid) -> Result<Uuid, IqiperDatabaseError> {
    Ok(activity_constraint::table
        .inner_join(activity::table)
        .filter(activity_constraint::columns::id.eq(select_id))
        .select(activity::columns::uid)
        .first::<Uuid>(conn)?)
}

/// Create a new activity constraint in the database
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `new_constraint` - The struct containing the new activity constraint informations
///
/// # Return Value
///  The created record
pub fn create(
    conn: &PgConnection,
    uid: &Uuid,
    new_constraint: NewActivityConstraint,
) -> Result<IdTsResponse, IqiperDatabaseError> {
    check_logic(&new_constraint)?;
    conn.transaction::<IdTsResponse, IqiperDatabaseError, _>(|| {
        select_activity_for_update(&conn, &new_constraint.activity_id)?;
        Ok(create_no_logic_check(&conn, &uid, new_constraint)?)
    })
}

/// Create a new activity constraint in the database (no logic check)
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `new_constraint` - The struct containing the new activity constraint informations
///
/// # Return Value
///  The created record
pub fn create_no_logic_check(
    conn: &PgConnection,
    uid: &Uuid,
    new_constraint: NewActivityConstraint,
) -> Result<IdTsResponse, IqiperDatabaseError> {
    conn.transaction(|| -> Result<IdTsResponse, IqiperDatabaseError> {
        activity::table
            .filter(activity::columns::id.eq(new_constraint.activity_id))
            .filter(activity::columns::uid.eq(uid))
            .select(activity::columns::id)
            .for_share()
            .first::<Uuid>(conn)?;
        let _activity_id = new_constraint.activity_id;
        Ok(diesel::insert_into(activity_constraint::table)
            .values(new_constraint)
            .returning((
                activity_constraint::columns::id,
                activity_constraint::columns::created_at,
            ))
            .get_result::<IdTsResponse>(conn)?)
    })
}
/// Modify an activity constraint info
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `activity_id` - The uuid of the `activity` record
/// - `constraint_id` - The uuid of the `activity_constraint` record
/// - `constraint` - The struct containing the new activity constraint informations
///
/// # Return Value
///  The updated response
pub fn modify(
    conn: &PgConnection,
    uid: &Uuid,
    activity_id: &Uuid,
    constraint_id: &Uuid,
    constraint: NewActivityConstraint,
) -> Result<UpdateResponse, IqiperDatabaseError> {
    check_logic(&constraint)?;
    conn.transaction(|| -> Result<UpdateResponse, IqiperDatabaseError> {
        select_activity_for_update(&conn, &activity_id)?;
        let type_ = activity_constraint::table
            .inner_join(activity::table)
            .filter(activity::columns::uid.eq(uid))
            .filter(activity::columns::id.eq(activity_id))
            .filter(activity_constraint::columns::id.eq(constraint_id))
            .select(activity_constraint::columns::type_)
            .for_update()
            .first::<ActivityConstraintType>(conn)?;
        if type_ != constraint.type_ {
            return Err(IqiperDatabaseError::TypeChanging);
        }
        Ok(
            diesel::update(activity_constraint::table.find(constraint_id))
                .set(&constraint)
                .returning((
                    activity_constraint::columns::id,
                    activity_constraint::columns::updated_at,
                ))
                .get_result::<UpdateResponse>(conn)?,
        )
    })
}

/// Delete an activity constraint
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `activity_id` - The uuid of the activity the constraint belongs to
/// - `constraint_id` - The uuid of the record to pull out of the database
///
/// # Return Value
///  The number of affected records
pub fn delete(
    conn: &PgConnection,
    uid: &Uuid,
    activity_id: &Uuid,
    constraint_id: &Uuid,
) -> Result<usize, IqiperDatabaseError> {
    Ok(conn.transaction(|| {
        let select_activity_constraint = activity_constraint::table
            .filter(
                activity_constraint::columns::activity_id.eq_any(
                    activity::table
                        .filter(activity::columns::uid.eq(uid))
                        .find(activity_id)
                        .select(activity::columns::id),
                ),
            )
            .find(constraint_id);
        let size = diesel::delete(select_activity_constraint).execute(conn)?;
        match size {
            0 => Err(IqiperDatabaseError::DatabaseError(
                diesel::result::Error::NotFound,
            )),
            1 => Ok(1),
            _ => Err(IqiperDatabaseError::UnexpectedResult(1, size)),
        }
    })?)
}

type ActivityConstraintColumns = (
    activity_constraint::columns::id,
    activity_constraint::columns::activity_id,
    activity_constraint::columns::type_,
    activity_constraint::columns::alert_message,
    activity_constraint::columns::start_date,
    activity_constraint::columns::end_date,
    activity_constraint::columns::notify_date,
    activity_constraint::columns::created_at,
    activity_constraint::columns::updated_at,
);

pub const SELECT_COLUMNS: ActivityConstraintColumns = (
    activity_constraint::columns::id,
    activity_constraint::columns::activity_id,
    activity_constraint::columns::type_,
    activity_constraint::columns::alert_message,
    activity_constraint::columns::start_date,
    activity_constraint::columns::end_date,
    activity_constraint::columns::notify_date,
    activity_constraint::columns::created_at,
    activity_constraint::columns::updated_at,
);

#[derive(PartialEq, Debug, Clone, Queryable, Identifiable, Associations, Serialize, Deserialize)]
#[belongs_to(crate::activity::SelectActivity, foreign_key = "activity_id")]
#[table_name = "activity_constraint"]
pub struct ActivityConstraintId {
    pub id: Uuid,
    #[serde(skip_serializing)]
    pub activity_id: Uuid,
}

#[derive(
    PartialEq,
    Debug,
    Clone,
    Queryable,
    QueryableByName,
    Identifiable,
    Associations,
    Serialize,
    Deserialize,
)]
#[belongs_to(crate::activity::SelectActivity, foreign_key = "activity_id")]
#[table_name = "activity_constraint"]
pub struct SelectActivityConstraint {
    pub id: Uuid,
    pub activity_id: Uuid,
    #[serde(rename = "type")]
    pub type_: ActivityConstraintType,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub alert_message: Option<String>,
    pub start_date: DateTime<Utc>,
    pub end_date: DateTime<Utc>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub notify_date: Option<DateTime<Utc>>,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
}

#[derive(PartialEq, Debug, Clone, Insertable, AsChangeset, Serialize)]
#[table_name = "activity_constraint"]
pub struct NewActivityConstraint {
    #[serde(rename = "type")]
    pub type_: ActivityConstraintType,
    pub activity_id: Uuid,
    pub start_date: DateTime<Utc>,
    pub end_date: DateTime<Utc>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub notify_date: Option<DateTime<Utc>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub alert_message: Option<String>,
}

#[derive(PartialEq, Debug, Clone, Serialize, Deserialize, QueryableByName)]
#[table_name = "activity_constraint"]
pub struct ActivityConstraintPaginated {
    pub id: Uuid,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
}

impl<'de> Deserialize<'de> for NewActivityConstraint {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        #[derive(Deserialize)]
        #[serde(field_identifier, rename_all = "snake_case")]
        enum Field {
            ActivityId,
            DateStart,
            DateEnd,
            NotifyDate,
            AlertMessage,
        }

        struct NewConstraintVisitor;

        impl<'de> Visitor<'de> for NewConstraintVisitor {
            type Value = NewActivityConstraint;

            fn expecting(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
                formatter.write_str("struct NewActivityConstraint")
            }

            fn visit_map<V>(self, mut map: V) -> Result<NewActivityConstraint, V::Error>
            where
                V: MapAccess<'de>,
            {
                let mut activity_id = None;
                let mut start_date = None;
                let mut end_date = None;
                let mut alert_message = None;
                let mut notify_date = None;
                let mut type_ = None;
                while let Some(key) = map.next_key()? {
                    match key {
                        Field::ActivityId => {
                            if activity_id.is_some() {
                                return Err(de::Error::duplicate_field("activity_id"));
                            }
                            activity_id = Some(map.next_value()?);
                        }
                        Field::DateStart => {
                            if start_date.is_some() {
                                return Err(de::Error::duplicate_field("start_date"));
                            }
                            start_date = Some(map.next_value()?);
                        }
                        Field::DateEnd => {
                            if end_date.is_some() {
                                return Err(de::Error::duplicate_field("end_date"));
                            }
                            end_date = Some(map.next_value()?);
                        }
                        Field::AlertMessage => {
                            if alert_message.is_some() {
                                return Err(de::Error::duplicate_field("alert_message"));
                            }
                            alert_message = Some(map.next_value()?);
                        }
                        Field::NotifyDate => {
                            // If more type, we should check there is no clash between types
                            if notify_date.is_some() || type_.is_some() {
                                return Err(de::Error::duplicate_field("notify_date"));
                            }
                            type_ = Some(ActivityConstraintType::Timer);
                            notify_date = Some(map.next_value()?);
                        }
                    }
                }
                let activity_id =
                    activity_id.ok_or_else(|| de::Error::missing_field("activity_id"))?;
                let start_date =
                    start_date.ok_or_else(|| de::Error::missing_field("start_date"))?;
                let end_date = end_date.ok_or_else(|| de::Error::missing_field("end_date"))?;
                let notify_date =
                    notify_date.ok_or_else(|| de::Error::missing_field("notify_date"))?;
                let type_ = type_.ok_or_else(|| de::Error::missing_field("Couldn't guess type"))?;
                Ok(NewActivityConstraint {
                    activity_id,
                    start_date,
                    alert_message,
                    end_date,
                    notify_date,
                    type_,
                })
            }
        }
        deserializer.deserialize_map(NewConstraintVisitor)
    }
}

use crate::diesel::ExpressionMethods;
use crate::error::IqiperDatabaseError;
use crate::schema::iqiper::distant_user;
use chrono::{DateTime, Utc};
use diesel::{PgConnection, QueryDsl, QueryableByName, RunQueryDsl};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

/// Create a new `distant_user` in the database
///
/// # Arguments
/// *`conn` - The postgres connection
/// *`uid` - The user id, sent by the IdP
///
/// # Return Value
///  The created record
pub fn create_user(conn: &PgConnection, uid: &Uuid) -> Result<SelectUser, IqiperDatabaseError> {
    Ok(diesel::insert_into(distant_user::table)
        .values(NewUser { id: uid.clone() })
        .get_result::<SelectUser>(conn)?)
}

/// Select a single user from the database
///
/// # Arguments
/// *`conn` - The postgres connection
/// *`uid` - The uuid of the `distant_user` record
///
/// # Return Value
///  The user
pub fn select_by_id(conn: &PgConnection, uid: &Uuid) -> Result<SelectUser, IqiperDatabaseError> {
    Ok(distant_user::table
        .filter(distant_user::columns::id.eq(uid))
        .select((distant_user::columns::id, distant_user::columns::created_at))
        .get_result::<SelectUser>(conn)?)
}

/// Delete an `distant_user`
///
/// # Arguments
/// *`conn` - The postgres connection
/// *`uid` - The uuid of the `distant_user` record
///
/// # Return Value
///  The number of affected records
pub fn delete_user(conn: &PgConnection, uid: &Uuid) -> Result<usize, IqiperDatabaseError> {
    Ok(diesel::delete(distant_user::table.find(uid)).execute(conn)?)
}

#[derive(Debug, Clone, Insertable, Serialize, Deserialize)]
#[table_name = "distant_user"]
pub struct NewUser {
    pub id: Uuid,
}

#[derive(Debug, Clone, Identifiable, Serialize, Deserialize)]
#[table_name = "distant_user"]
pub struct UserId {
    pub id: Uuid,
}

#[derive(Debug, Clone, Queryable, QueryableByName, Serialize, Deserialize)]
#[table_name = "distant_user"]
pub struct SelectUser {
    pub id: Uuid,
    pub created_at: DateTime<Utc>,
}

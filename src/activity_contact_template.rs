use crate::contact::{SelectContact, CONTACT_SELECT_COLUMNS};
use crate::enums::ContactType;
use crate::error::IqiperDatabaseError;
use crate::schema::iqiper::{activity_contact_template, activity_template, contact};
use chrono::{DateTime, Utc};
use diesel::{
    Connection, ExpressionMethods, Insertable, JoinOnDsl, PgConnection, QueryDsl, RunQueryDsl,
};

use serde::{Deserialize, Serialize};

use uuid::Uuid;

/// Select every activity contacts template for an user.
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
///
/// # Return Value
///  A vector of activity contacts template
pub fn select_all(
    conn: &PgConnection,
    uid: &Uuid,
) -> Result<Vec<ActivityContactTemplate>, IqiperDatabaseError> {
    Ok(activity_contact_template::table
        .inner_join(activity_template::table)
        .inner_join(contact::table)
        .filter(activity_template::columns::uid.eq(uid))
        .filter(contact::columns::uid.eq(uid))
        .order(activity_contact_template::columns::created_at.asc())
        .select((
            activity_contact_template::columns::activity_template_id,
            activity_contact_template::columns::contact_id,
            activity_contact_template::columns::created_at,
        ))
        .load::<ActivityContactTemplate>(conn)?)
}

/// Select a single activity contact template
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `activity_template_id` - The uuid of the `activity` record
/// - `contact_id` - The uuid of the `contact` record
///
/// # Return Value
///  The selected activity contact template or `None` if not found
pub fn select_by_id(
    conn: &PgConnection,
    uid: &Uuid,
    activity_template_id: &Uuid,
    contact_id: &Uuid,
) -> Result<ActivityContactTemplate, IqiperDatabaseError> {
    Ok(activity_contact_template::table
        .inner_join(activity_template::table)
        .filter(activity_template::columns::uid.eq(uid))
        .filter(activity_contact_template::columns::activity_template_id.eq(activity_template_id))
        .filter(activity_contact_template::columns::contact_id.eq(contact_id))
        .select(SELECT_COLUMNS)
        .first::<ActivityContactTemplate>(conn)?)
}

/// Select an activity by its id joining the linked contact with it
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `activity_template_id` - The uuid of the `activity` record
/// - `contact_id` - The uuid of the `contact` record
///
/// # Return Value
///  The selected activity contact + contact
pub fn select_by_id_with_contact(
    conn: &PgConnection,
    uid: &Uuid,
    activity_template_id: &Uuid,
    contact_id: &Uuid,
) -> Result<SelectActivityContactTemplateWithContact, IqiperDatabaseError> {
    Ok(activity_contact_template::table
        .inner_join(contact::table)
        .order((
            activity_contact_template::columns::created_at.asc(),
            contact::columns::created_at.asc(),
            contact::columns::updated_at.asc(),
        ))
        .select(ACTIVITY_CONTACT_TEMPLATE_WITH_CONTACT_SELECT_COLUMNS)
        .inner_join(activity_template::table)
        .filter(activity_template::columns::uid.eq(uid))
        .filter(activity_contact_template::columns::contact_id.eq(contact_id))
        .filter(activity_contact_template::columns::activity_template_id.eq(activity_template_id))
        .first::<SelectActivityContactTemplateWithContact>(conn)?)
}

/// Select every contact linked to an activity template
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The user id
/// - `activity_template_id` - The uuid of the `activity` record
///
/// # Return Value
///  The selected activity contact + contact
pub fn select_contacts_for_activity_template(
    conn: &PgConnection,
    uid: &Uuid,
    activity_template_id: &Uuid,
) -> Result<Vec<SelectContact>, IqiperDatabaseError> {
    Ok(activity_contact_template::table
        .inner_join(contact::table)
        .order((
            activity_contact_template::columns::created_at.asc(),
            contact::columns::created_at.asc(),
            contact::columns::updated_at.asc(),
        ))
        .filter(contact::columns::uid.eq(uid))
        .select(CONTACT_SELECT_COLUMNS)
        .filter(activity_contact_template::columns::activity_template_id.eq(activity_template_id))
        .load::<SelectContact>(conn)?)
}

/// Create a new activity contact in the database
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `new_activity_contact` - The struct containing the new activity_contact_template informations
///
/// # Return Value
///  The created record
pub fn create(
    conn: &PgConnection,
    uid: &Uuid,
    new_activity_contact: NewActivityContactTemplate,
) -> Result<ActivityContactTemplate, IqiperDatabaseError> {
    Ok(activity_template::table
        .inner_join(contact::table.on(activity_template::uid.eq(contact::uid)))
        .select((activity_template::columns::id, contact::columns::id))
        .filter(activity_template::columns::id.eq(new_activity_contact.activity_template_id))
        .filter(contact::columns::id.eq(new_activity_contact.contact_id))
        .filter(activity_template::columns::uid.eq(uid))
        .filter(contact::columns::uid.eq(uid))
        .insert_into(activity_contact_template::table)
        .returning(SELECT_COLUMNS)
        .into_columns((
            activity_contact_template::columns::activity_template_id,
            activity_contact_template::columns::contact_id,
        ))
        .get_result::<ActivityContactTemplate>(conn)?)
}

/// Delete an activity contact template
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `activity_template_id` - The uuid of the record to pull out of the database
/// - `contact_id` - The uuid of the record to pull out of the database
///
/// # Return Value
///  The number of affected records
pub fn delete(
    conn: &PgConnection,
    uid: &Uuid,
    activity_template_id: &Uuid,
    contact_id: &Uuid,
) -> Result<usize, IqiperDatabaseError> {
    Ok(conn.transaction(move || {
        activity_contact_template::table
            .inner_join(activity_template::table)
            .filter(activity_template::columns::uid.eq(uid))
            .filter(activity_contact_template::columns::contact_id.eq(contact_id))
            .filter(
                activity_contact_template::columns::activity_template_id.eq(activity_template_id),
            )
            .select((
                activity_contact_template::columns::activity_template_id,
                activity_contact_template::columns::contact_id,
            ))
            .first::<(Uuid, Uuid)>(conn)?;
        diesel::delete(activity_contact_template::table.find((activity_template_id, contact_id)))
            .execute(conn)
    })?)
}

#[derive(PartialEq, Debug, Clone, Queryable, Identifiable, Associations, Serialize, Deserialize)]
#[belongs_to(
    crate::activity_template::SelectActivityTemplate,
    foreign_key = "activity_template_id"
)]
#[belongs_to(crate::contact::SelectContact, foreign_key = "contact_id")]
#[primary_key(activity_template_id, contact_id)]
#[table_name = "activity_contact_template"]
pub struct ActivityContactTemplateId {
    pub activity_template_id: Uuid,
    pub contact_id: Uuid,
}

type ActivityContactTemplateColumns = (
    activity_contact_template::columns::activity_template_id,
    activity_contact_template::columns::contact_id,
    activity_contact_template::columns::created_at,
);

pub const SELECT_COLUMNS: ActivityContactTemplateColumns = (
    activity_contact_template::columns::activity_template_id,
    activity_contact_template::columns::contact_id,
    activity_contact_template::columns::created_at,
);

type ActivityContactTemplateWithContactColumns = (
    activity_contact_template::columns::activity_template_id,
    activity_contact_template::columns::contact_id,
    contact::columns::nickname,
    contact::columns::favorite,
    contact::columns::uid,
    contact::columns::type_,
    contact::columns::email,
    contact::columns::target_uid,
    contact::columns::created_at,
    contact::columns::updated_at,
);

pub const ACTIVITY_CONTACT_TEMPLATE_WITH_CONTACT_SELECT_COLUMNS:
    ActivityContactTemplateWithContactColumns = (
    activity_contact_template::columns::activity_template_id,
    activity_contact_template::columns::contact_id,
    contact::columns::nickname,
    contact::columns::favorite,
    contact::columns::uid,
    contact::columns::type_,
    contact::columns::email,
    contact::columns::target_uid,
    contact::columns::created_at,
    contact::columns::updated_at,
);

#[derive(PartialEq, Debug, Clone, Queryable, Identifiable, Associations, Serialize, Deserialize)]
#[belongs_to(
    crate::activity_template::SelectActivityTemplate,
    foreign_key = "activity_template_id"
)]
#[belongs_to(crate::contact::SelectContact, foreign_key = "contact_id")]
#[primary_key(activity_template_id, contact_id)]
#[table_name = "activity_contact_template"]
pub struct SelectActivityContactTemplateWithContact {
    #[serde(skip_serializing)]
    pub activity_template_id: Uuid,
    #[serde(rename(serialize = "id"))]
    pub contact_id: Uuid,
    pub nickname: String,
    pub favorite: bool,
    #[serde(skip_serializing)]
    pub uid: Uuid,
    #[serde(skip_serializing)]
    pub type_: ContactType,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub email: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub target_uid: Option<Uuid>,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
}

#[derive(
    PartialEq,
    Debug,
    Clone,
    Queryable,
    Identifiable,
    Insertable,
    Associations,
    Serialize,
    Deserialize,
)]
#[belongs_to(
    crate::activity_template::SelectActivityTemplate,
    foreign_key = "activity_template_id"
)]
#[belongs_to(crate::contact::SelectContact, foreign_key = "contact_id")]
#[primary_key(activity_template_id, contact_id)]
#[table_name = "activity_contact_template"]
pub struct ActivityContactTemplate {
    pub activity_template_id: Uuid,
    pub contact_id: Uuid,
    pub created_at: DateTime<Utc>,
}

#[derive(PartialEq, Debug, Clone, Queryable, Insertable, Serialize, Deserialize)]
#[table_name = "activity_contact_template"]
pub struct NewActivityContactTemplate {
    pub activity_template_id: Uuid,
    pub contact_id: Uuid,
}

use crate::enums::ContactType;
use crate::error::IqiperDatabaseError;
use crate::schema::iqiper::contact;
use chrono::{DateTime, Utc};
use diesel::{ExpressionMethods, PgConnection, QueryDsl, QueryableByName, RunQueryDsl};
use serde::{
    de::{self, MapAccess, Visitor},
    Deserialize, Deserializer, Serialize,
};
use std::fmt;
use uuid::Uuid;

/// Select every contacts for an user.
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
///
/// # Return Value
///  A vector of contacts
pub fn select_all(
    conn: &PgConnection,
    uid: &Uuid,
) -> Result<Vec<SelectContact>, IqiperDatabaseError> {
    Ok(contact::table
        .filter(contact::columns::uid.eq(uid))
        .order((
            contact::columns::created_at.asc(),
            contact::columns::updated_at.asc(),
        ))
        .load::<SelectContact>(conn)?)
}

/// Select every contacts' id for an user.
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
///
/// # Return Value
///  A vector of contacts id
///
pub fn select_all_id(conn: &PgConnection, uid: &Uuid) -> Result<Vec<Uuid>, IqiperDatabaseError> {
    Ok(contact::table
        .filter(contact::columns::uid.eq(uid))
        .select(contact::columns::id)
        .order((
            contact::columns::created_at.asc(),
            contact::columns::updated_at.asc(),
        ))
        .load::<Uuid>(conn)?)
}

/// Select a single contact
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `contact_id` - The uuid of the `contact` record
///
/// # Return Value
///  The selected contact
pub fn select_by_id(
    conn: &PgConnection,
    uid: &Uuid,
    contact_id: &Uuid,
) -> Result<SelectContact, IqiperDatabaseError> {
    println!(
        "{}",
        diesel::debug_query::<diesel::pg::Pg, _>(
            &contact::table
                .filter(contact::columns::uid.eq(uid))
                .filter(contact::columns::id.eq(contact_id))
        )
    );
    Ok(contact::table
        .filter(contact::columns::uid.eq(uid))
        .filter(contact::columns::id.eq(contact_id))
        .first::<SelectContact>(conn)?)
}

/// Create a new contact in the database
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `new_contact` - The struct containing the new contact informations
///
/// # Return Value
/// The id and created date
pub fn create(
    conn: &PgConnection,
    new_contact: NewContact,
) -> Result<SelectContact, IqiperDatabaseError> {
    Ok(diesel::insert_into(contact::table)
        .values(new_contact)
        .get_result::<SelectContact>(conn)?)
}

/// Update a contact nickname
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `contact_id` - The uuid of the `contact` record to update
/// - `nickname` - The new nickname
///
/// # Return Value
///  The uuid and the update timestamp of the updated record
pub fn update_nickname(
    conn: &PgConnection,
    uid: &Uuid,
    contact_id: &Uuid,
    nickname: String,
) -> Result<(Uuid, DateTime<Utc>), IqiperDatabaseError> {
    Ok(diesel::update(
        contact::table
            .filter(contact::columns::uid.eq(uid))
            .find(contact_id),
    )
    .set(contact::columns::nickname.eq(nickname))
    .returning((contact::columns::id, contact::columns::updated_at))
    .get_result(conn)?)
}

/// Update a contact favorite
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `contact_id` - The uuid of the `contact` record to update
/// - `favorite` - The new favorite status
///
/// # Return Value
///  The uuid and the update timestamp of the updated record
pub fn update_favorite(
    conn: &PgConnection,
    uid: &Uuid,
    contact_id: &Uuid,
    favorite: bool,
) -> Result<(Uuid, DateTime<Utc>), IqiperDatabaseError> {
    Ok(diesel::update(
        contact::table
            .filter(contact::columns::uid.eq(uid))
            .find(contact_id),
    )
    .set(contact::columns::favorite.eq(favorite))
    .returning((contact::columns::id, contact::columns::updated_at))
    .get_result(conn)?)
}

/// Delete an contact
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `contact_id` - The uuid of the record to pull out of the database
///
///
/// # Return Value
///  The number of affected records
pub fn delete(
    conn: &PgConnection,
    uid: &Uuid,
    contact_id: &Uuid,
) -> Result<usize, IqiperDatabaseError> {
    Ok(diesel::delete(
        contact::table
            .filter(contact::columns::uid.eq(uid))
            .find(contact_id),
    )
    .execute(conn)?)
}

type ContactColumns = (
    contact::columns::id,
    contact::columns::nickname,
    contact::columns::favorite,
    contact::columns::uid,
    contact::columns::type_,
    contact::columns::email,
    contact::columns::target_uid,
    contact::columns::created_at,
    contact::columns::updated_at,
);

pub const CONTACT_SELECT_COLUMNS: ContactColumns = (
    contact::columns::id,
    contact::columns::nickname,
    contact::columns::favorite,
    contact::columns::uid,
    contact::columns::type_,
    contact::columns::email,
    contact::columns::target_uid,
    contact::columns::created_at,
    contact::columns::updated_at,
);

#[derive(
    PartialEq,
    Debug,
    Clone,
    Queryable,
    QueryableByName,
    Identifiable,
    Serialize,
    Associations,
    Deserialize,
)]
#[belongs_to(crate::distant_user::UserId, foreign_key = "uid")]
#[table_name = "contact"]
pub struct SelectContact {
    pub id: Uuid,
    pub nickname: String,
    pub favorite: bool,
    #[serde(skip_serializing)]
    pub uid: Uuid,
    #[serde(skip_serializing)]
    #[serde(rename(serialize = "type", deserialize = "type"))]
    pub type_: ContactType,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub email: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub target_uid: Option<Uuid>,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
}

#[derive(PartialEq, Debug, Clone, Insertable, Serialize)]
#[table_name = "contact"]
pub struct NewContact {
    pub nickname: String,
    pub favorite: bool,
    #[serde(skip_serializing)]
    pub uid: Uuid,
    #[serde(skip_serializing)]
    pub type_: ContactType,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub email: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub target_uid: Option<Uuid>,
}

impl<'de> NewContact {
    pub fn deserialize<D>(uid: Uuid, raw: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let tmp: NewContactTmp = Deserialize::deserialize(raw)?;
        Ok(NewContact {
            nickname: tmp.nickname,
            favorite: tmp.favorite,
            uid,
            type_: tmp.type_,
            email: tmp.email,
            target_uid: tmp.target_uid,
        })
    }
}

#[derive(PartialEq, Debug, Clone)]
struct NewContactTmp {
    pub nickname: String,
    pub favorite: bool,
    pub type_: ContactType,
    pub email: Option<String>,
    pub target_uid: Option<Uuid>,
}

impl<'de> Deserialize<'de> for NewContactTmp {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        #[derive(Deserialize)]
        #[serde(field_identifier, rename_all = "snake_case")]
        enum Field {
            Nickname,
            Favorite,
            Email,
            TargetUid,
        }

        struct NewContactVisitor;

        impl<'de> Visitor<'de> for NewContactVisitor {
            type Value = NewContactTmp;

            fn expecting(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
                formatter.write_str("struct NewContactTmp")
            }

            fn visit_map<V>(self, mut map: V) -> Result<NewContactTmp, V::Error>
            where
                V: MapAccess<'de>,
            {
                let mut nickname = None;
                let mut favorite = None;
                let mut type_ = None;
                let mut email = None;
                let mut target_uid = None;
                while let Some(key) = map.next_key()? {
                    match key {
                        Field::Nickname => {
                            if nickname.is_some() {
                                return Err(de::Error::duplicate_field("nickname"));
                            }
                            nickname = Some(map.next_value()?);
                        }
                        Field::Favorite => {
                            if favorite.is_some() {
                                return Err(de::Error::duplicate_field("favorite"));
                            }
                            favorite = Some(map.next_value()?);
                        }
                        Field::Email => {
                            if email.is_some() || type_.is_some() {
                                return Err(de::Error::duplicate_field("email"));
                            }
                            type_ = Some(ContactType::Email);
                            email = Some(map.next_value()?);
                        }
                        Field::TargetUid => {
                            if target_uid.is_some() || type_.is_some() {
                                return Err(de::Error::duplicate_field("target_uid"));
                            }
                            type_ = Some(ContactType::Friend);
                            target_uid = Some(map.next_value()?);
                        }
                    }
                }
                let nickname = nickname.ok_or_else(|| de::Error::missing_field("nickname"))?;
                let favorite = favorite.unwrap_or_default();
                let type_ =
                    type_.ok_or_else(|| de::Error::missing_field("email and target_uid"))?;
                Ok(NewContactTmp {
                    nickname,
                    favorite,
                    type_,
                    email,
                    target_uid,
                })
            }
        }
        deserializer.deserialize_map(NewContactVisitor)
    }
}

use log::debug;

/// A `HandleEvent` implementation which prints to the debug log.
#[derive(Copy, Clone, Debug)]
pub struct IqiperDatabaseEventHandler;

impl diesel::r2d2::HandleEvent for IqiperDatabaseEventHandler {
    /// Called when a new connection is acquired.
    ///
    /// The default implementation does nothing.
    fn handle_acquire(&self, event: diesel::r2d2::event::AcquireEvent) {
        debug!("New connection {} acquired", event.connection_id());
    }

    /// Called when a connection is released.
    ///
    /// The default implementation does nothing.
    fn handle_release(&self, event: diesel::r2d2::event::ReleaseEvent) {
        debug!("Connection {} released", event.connection_id());
    }

    /// Called when a checkout attempt times out.
    ///
    /// The default implementation does nothing.
    fn handle_timeout(&self, event: diesel::r2d2::event::TimeoutEvent) {
        debug!("Connection checkout timed out after {:#?}", event.timeout());
    }

    /// Called when a connection is checked back into the pool.
    fn handle_checkin(&self, event: diesel::r2d2::event::CheckinEvent) {
        debug!("Connection {} checked-in", event.connection_id());
    }

    /// Called when a connection is checked out from the pool.
    ///
    /// The default implementation does nothing.
    #[allow(unused_variables)]
    fn handle_checkout(&self, event: diesel::r2d2::event::CheckoutEvent) {
        debug!("Connection {} checked-out", event.connection_id());
    }
}

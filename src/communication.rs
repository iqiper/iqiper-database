use crate::common::IdTsResponse;

use crate::activity_communication::{self, NewActivityCommunication};
use crate::activity_constraint_communication::{self, NewActivityConstraintCommunication};
use crate::communication_recipient;
use crate::diesel::Connection;
use crate::enums::{CommunicationReasonType, CommunicationType};
use crate::error::IqiperDatabaseError;
use crate::schema::iqiper::communication;
use chrono::{DateTime, Utc};
use diesel::{ExpressionMethods, PgConnection, QueryDsl, QueryableByName, RunQueryDsl};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

/// Select a single communication
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `communication_id` - The uuid of the `communication` record
///
/// # Return Value
/// The communication record
pub fn select_by_id(
    conn: &PgConnection,
    uid: &Uuid,
    communication_id: &Uuid,
) -> Result<SelectCommunication, IqiperDatabaseError> {
    Ok(communication::table
        .filter(communication::columns::uid.eq(uid))
        .select(COMMUNICATION_SELECT_COLUMNS)
        .find(communication_id)
        .first::<SelectCommunication>(conn)?)
}

/// Select a single communication (unlogged)
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `communication_id` - The uuid of the `communication` record
///
/// # Return Value
/// The communication record
pub fn select_by_id_unlogged(
    conn: &PgConnection,
    communication_id: &Uuid,
) -> Result<SelectCommunication, IqiperDatabaseError> {
    Ok(communication::table
        .select(COMMUNICATION_SELECT_COLUMNS)
        .find(communication_id)
        .first::<SelectCommunication>(conn)?)
}

/// Create a new communication in the database
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `new_communication` - The struct containing the new communication informations
///
/// # Return Value
///  The id and creation date
pub fn create(
    conn: &PgConnection,
    new_communication: NewCommunication,
) -> Result<IdTsResponse, IqiperDatabaseError> {
    Ok(diesel::insert_into(communication::table)
        .values(new_communication)
        .returning((
            communication::columns::id,
            communication::columns::created_at,
        ))
        .get_result::<IdTsResponse>(conn)?)
}

/// Create a new communication in the database from a constraint
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `constraint_id` - The constraint id to create from
/// - `new_communication` - The struct containing the new communication informations
///
/// # Return Value
///  The id and creation date
pub fn create_from_constraint(
    conn: &PgConnection,
    constraint_id: &Uuid,
    new_communication: NewCommunication,
) -> Result<IdTsResponse, IqiperDatabaseError> {
    Ok(
        conn.transaction::<IdTsResponse, IqiperDatabaseError, _>(|| {
            let uid = new_communication.uid;
            let res: IdTsResponse = diesel::insert_into(communication::table)
                .values(new_communication)
                .returning((
                    communication::columns::id,
                    communication::columns::created_at,
                ))
                .get_result::<IdTsResponse>(conn)?;
            communication_recipient::create_from_activity_constraint(
                &conn,
                &uid,
                &res.id,
                &constraint_id,
            )?;
            activity_constraint_communication::create(
                &conn,
                &uid,
                NewActivityConstraintCommunication {
                    constraint_id: *constraint_id,
                    communication_id: res.id,
                },
            )?;
            Ok(res)
        })?,
    )
}

/// Create a new communication in the database from a activity
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `activity_id` - The activity id to create from
/// - `new_communication` - The struct containing the new communication informations
///
/// # Return Value
///  The id and creation date
pub fn create_from_activity(
    conn: &PgConnection,
    activity_id: &Uuid,
    new_communication: NewCommunication,
) -> Result<IdTsResponse, IqiperDatabaseError> {
    Ok(
        conn.transaction::<IdTsResponse, IqiperDatabaseError, _>(|| {
            let uid = new_communication.uid;
            let res: IdTsResponse = diesel::insert_into(communication::table)
                .values(new_communication)
                .returning((
                    communication::columns::id,
                    communication::columns::created_at,
                ))
                .get_result::<IdTsResponse>(conn)?;
            communication_recipient::create_from_activity(&conn, &uid, &res.id, &activity_id)?;
            activity_communication::create(
                &conn,
                &uid,
                NewActivityCommunication {
                    activity_id: *activity_id,
                    communication_id: res.id,
                },
            )?;
            Ok(res)
        })?,
    )
}

/// Delete a communication
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `communication_id` - The uuid of the record to pull out of the database
///
///
/// # Return Value
///  The number of affected records
pub fn delete(
    conn: &PgConnection,
    uid: &Uuid,
    communication_id: &Uuid,
) -> Result<usize, IqiperDatabaseError> {
    Ok(diesel::delete(
        communication::table
            .filter(communication::columns::uid.eq(uid))
            .find(communication_id),
    )
    .execute(conn)?)
}

type CommunicationColumns = (
    communication::columns::id,
    communication::columns::uid,
    communication::columns::type_,
    communication::columns::reason,
    communication::columns::created_at,
);

pub const COMMUNICATION_SELECT_COLUMNS: CommunicationColumns = (
    communication::columns::id,
    communication::columns::uid,
    communication::columns::type_,
    communication::columns::reason,
    communication::columns::created_at,
);

#[derive(
    PartialEq,
    Debug,
    Clone,
    Queryable,
    QueryableByName,
    Identifiable,
    Serialize,
    Associations,
    Deserialize,
)]
#[belongs_to(crate::distant_user::SelectUser, foreign_key = "uid")]
#[table_name = "communication"]
pub struct SelectCommunication {
    pub id: Uuid,
    pub uid: Uuid,
    #[serde(rename(serialize = "type", deserialize = "type"))]
    pub type_: CommunicationType,
    pub reason: CommunicationReasonType,
    pub created_at: DateTime<Utc>,
}

#[derive(PartialEq, Debug, Clone, Insertable, Serialize)]
#[table_name = "communication"]
pub struct NewCommunication {
    pub uid: Uuid,
    #[serde(rename(serialize = "type", deserialize = "type"))]
    pub type_: CommunicationType,
    pub reason: CommunicationReasonType,
}

//! `iqiper-database` is a lib to handle database related functions.
//!
//! ## Diesel
//!
//! `diesel-rs` is the `postgres` ORM used by `iqiper`. It can handle its own migrations.
//!
//! ## Migrations
//!
//! The migrations can be fetched using the `get_migrations` function exposed at the
//! root of this crate. One can then run them as wanted.
//!
//! The prerequisite for the migration is to have a database with ownership given
//! to the database user used by this crate. The database should have the `uuid-ossp`
//! extension activated.
//!
//! ## Query
//!
//! Nearly all the query are built at runtime using `diesel-rs`. They are then cached
//! for performance.
//!
//! Some query, related to pagination, are written in pure PSQL in the `sql_queries`
//! folder.
//!

#[macro_use]
extern crate diesel;
pub mod enums;
#[macro_use]
pub extern crate diesel_migrations;
mod db;
use db::schema;

pub mod activity;
pub mod activity_communication;
pub mod activity_constraint;
pub mod activity_constraint_communication;
pub mod activity_constraint_template;
pub mod activity_contact;
pub mod activity_contact_template;
pub mod activity_template;
pub mod common;
pub mod communication;
pub mod communication_log;
pub mod communication_recipient;
pub mod contact;
pub mod distant_user;
pub mod error;
pub mod events_logger;
pub mod fcm_token;
pub mod embedded_migrations {
    #[derive(EmbedMigrations)]
    struct _Dummy;
    pub fn get_migrations() -> Box<&'static [&'static dyn diesel_migrations::Migration]> {
        Box::new(ALL_MIGRATIONS)
    }
}
#[cfg(test)]
mod tests;

use crate::contact::SelectContact;
use crate::diesel::BelongingToDsl;
use crate::diesel::GroupedBy;
use crate::enums::CommunicationStatus;
use crate::error::IqiperDatabaseError;
use crate::schema::iqiper::{communication, communication_log};
use chrono::{DateTime, Utc};
use diesel::pg::expression::dsl::*;
use diesel::{ExpressionMethods, PgConnection, QueryDsl, QueryableByName, RunQueryDsl};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

/// Select a single communication log
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `communication_log` - The uuid of the `communication_log` record
///
/// # Return Value
/// The communication log record
pub fn select_by_id(
    conn: &PgConnection,
    uid: &Uuid,
    communication_log_id: &Uuid,
) -> Result<SelectCommunicationLog, IqiperDatabaseError> {
    Ok(communication_log::table
        .filter(
            communication_log::columns::communication_id.eq(any(communication::table
                .select(communication::columns::id)
                .filter(communication::columns::uid.eq(uid)))),
        )
        .select(COMMUNICATION_LOG_SELECT_COLUMNS)
        .find(communication_log_id)
        .first::<SelectCommunicationLog>(conn)?)
}

/// Select by recipient
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `communication_id` - The id of the communication
/// - `communication_recipient_id` - The id of the communication recipient
///
/// # Return Value
/// The communication log record
pub fn select_by_recipient(
    conn: &PgConnection,
    uid: &Uuid,
    communication_id: &Uuid,
    communication_recipient_id: &Uuid,
) -> Result<Vec<SelectCommunicationLog>, IqiperDatabaseError> {
    Ok(communication_log::table
        .filter(
            communication_log::columns::communication_id.eq(any(communication::table
                .select(communication::columns::id)
                .filter(communication::columns::id.eq(communication_id))
                .filter(communication::columns::uid.eq(uid)))),
        )
        .filter(communication_log::columns::contact_id.eq(communication_recipient_id))
        .select(COMMUNICATION_LOG_SELECT_COLUMNS)
        .load::<SelectCommunicationLog>(conn)?)
}

/// Select by recipient (unlogged)
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `communication_id` - The id of the communication
/// - `contacts` - An array of contacts for which to get the logs
///
/// # Return Value
/// The communication log record
pub fn select_by_contacts_unlogged(
    conn: &PgConnection,
    communication_id: &Uuid,
    contacts: Vec<SelectContact>,
) -> Result<Vec<(SelectContact, Vec<SelectCommunicationLog>)>, IqiperDatabaseError> {
    let logs: Vec<Vec<SelectCommunicationLog>> = SelectCommunicationLog::belonging_to(&contacts)
        .filter(communication_log::columns::communication_id.eq(communication_id))
        .select(COMMUNICATION_LOG_SELECT_COLUMNS)
        .load::<SelectCommunicationLog>(conn)?
        .grouped_by(&contacts);
    Ok(contacts.into_iter().zip(logs.into_iter()).collect())
}

/// Create a new communication log in the database
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `new_communication_log` - The struct containing the new communication log informations
///
/// # Return Value
///  The created record
pub fn create(
    conn: &PgConnection,
    new_communication_log: NewCommunicationLog,
) -> Result<SelectCommunicationLog, IqiperDatabaseError> {
    Ok(diesel::insert_into(communication_log::table)
        .values(new_communication_log)
        .get_result::<SelectCommunicationLog>(conn)?)
}

/// Create a (many) new communication log in the database
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `new_communication_logs` - Vec of struct containing the new communication logs informations
///
/// # Return Value
/// An array of the created records
pub fn create_multi(
    conn: &PgConnection,
    new_communication_log: Vec<NewCommunicationLog>,
) -> Result<Vec<SelectCommunicationLog>, IqiperDatabaseError> {
    Ok(diesel::insert_into(communication_log::table)
        .values(new_communication_log)
        .get_results::<SelectCommunicationLog>(conn)?)
}

/// Delete a communication
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `communication_log_id` - The uuid of the record to pull out of the database
///
///
/// # Return Value
///  The number of affected records
pub fn delete(
    conn: &PgConnection,
    uid: &Uuid,
    communication_log_id: &Uuid,
) -> Result<usize, IqiperDatabaseError> {
    Ok(diesel::delete(
        communication_log::table
            .filter(
                communication_log::columns::communication_id.eq(any(communication::table
                    .select(communication::columns::id)
                    .filter(communication::columns::uid.eq(uid)))),
            )
            .find(communication_log_id),
    )
    .execute(conn)?)
}

type CommunicationLogColumns = (
    communication_log::columns::id,
    communication_log::columns::communication_id,
    communication_log::columns::contact_id,
    communication_log::columns::status,
    communication_log::columns::created_at,
);

pub const COMMUNICATION_LOG_SELECT_COLUMNS: CommunicationLogColumns = (
    communication_log::columns::id,
    communication_log::columns::communication_id,
    communication_log::columns::contact_id,
    communication_log::columns::status,
    communication_log::columns::created_at,
);

#[derive(
    PartialEq,
    Debug,
    Clone,
    Queryable,
    QueryableByName,
    Identifiable,
    Serialize,
    Associations,
    Deserialize,
)]
#[belongs_to(
    crate::communication::SelectCommunication,
    foreign_key = "communication_id"
)]
#[belongs_to(crate::contact::SelectContact, foreign_key = "contact_id")]
#[table_name = "communication_log"]
pub struct SelectCommunicationLog {
    pub id: Uuid,
    #[serde(skip_serializing)]
    pub communication_id: Uuid,
    pub contact_id: Uuid,
    pub status: CommunicationStatus,
    pub created_at: DateTime<Utc>,
}

#[derive(PartialEq, Debug, Clone, Insertable, Serialize)]
#[table_name = "communication_log"]
pub struct NewCommunicationLog {
    #[serde(skip_serializing)]
    pub communication_id: Uuid,
    pub contact_id: Uuid,
    pub status: CommunicationStatus,
}

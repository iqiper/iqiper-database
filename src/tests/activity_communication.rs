use super::*;

use crate::activity::SelectActivity;
use crate::activity_communication::{self, NewActivityCommunication};
use crate::activity_constraint_communication::{self, NewActivityConstraintCommunication};
use crate::communication::{self, NewCommunication, SelectCommunication};

use crate::enums::{CommunicationReasonType, CommunicationType};
use chrono::{Duration, Utc};

use diesel::PgConnection;
use uuid::Uuid;

fn setup(conn: &PgConnection) -> (Uuid, Uuid, Uuid) {
    let user_uuid = create_test_user(&conn);
    let activity = create_activity(&conn, &user_uuid, true, None, None);
    let communication = communication::create(
        &conn,
        NewCommunication {
            uid: user_uuid,
            type_: CommunicationType::Email,
            reason: CommunicationReasonType::Check,
        },
    )
    .expect("to create the communication in the database");
    (user_uuid, activity, communication.id)
}

#[test]
fn create_activity_communication_ok() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, activity, communication) = setup(&conn);
        activity_communication::create(
            &conn,
            &uid,
            NewActivityCommunication {
                activity_id: activity,
                communication_id: communication,
            },
        )
        .expect("to create the activity constraint communication in the db");
        Ok(())
    });
}

#[test]
fn create_activity_not_found_1() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, _activity, communication) = setup(&conn);
        activity_communication::create(
            &conn,
            &uid,
            NewActivityCommunication {
                activity_id: Uuid::new_v4(),
                communication_id: communication,
            },
        )
        .expect_err("not to create the activity constraint communication in the db");
        Ok(())
    });
}

#[test]
fn create_activity_not_found_2() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, activity, _communication) = setup(&conn);
        activity_communication::create(
            &conn,
            &uid,
            NewActivityCommunication {
                activity_id: activity,
                communication_id: Uuid::new_v4(),
            },
        )
        .expect_err("not to create the activity constraint communication in the db");
        Ok(())
    });
}

#[test]
fn select_activity_by_communication() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, activity, communication) = setup(&conn);
        activity_communication::create(
            &conn,
            &uid,
            NewActivityCommunication {
                activity_id: activity,
                communication_id: communication,
            },
        )
        .expect("to create the activity constraint communication in the db");
        let res: SelectActivity =
            activity_communication::select_activity_by_communication(&conn, &uid, &communication)
                .expect("to select the activity constraint from the database");
        assert_eq!(res.id, activity, "activity id mismatch");
        Ok(())
    });
}

#[test]
fn select_activity_by_communication_not_found() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, activity, communication) = setup(&conn);
        activity_communication::create(
            &conn,
            &uid,
            NewActivityCommunication {
                activity_id: activity,
                communication_id: communication,
            },
        )
        .expect("to create the activity constraint communication in the db");
        activity_communication::select_activity_by_communication(&conn, &uid, &activity)
            .expect_err("not to have select the activity constraint from the database");
        Ok(())
    });
}

#[test]
fn select_all() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, activity, communication) = setup(&conn);
        activity_communication::create(
            &conn,
            &uid,
            NewActivityCommunication {
                activity_id: activity,
                communication_id: communication,
            },
        )
        .expect("to create the activity constraint communication in the db");
        let mut res: Vec<SelectCommunication> =
            activity_communication::select_all(&conn, &uid, &activity)
                .expect("to select the activity constraint from the database");
        assert_eq!(res.len(), 1, "It should be of length 1");
        let res = res.pop().unwrap();
        assert_eq!(res.id, communication, "communication id mismatch");
        Ok(())
    });
}

#[test]
fn select_all_not_found_1() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, activity, communication) = setup(&conn);
        activity_communication::create(
            &conn,
            &uid,
            NewActivityCommunication {
                activity_id: activity,
                communication_id: communication,
            },
        )
        .expect("to create the activity constraint communication in the db");
        let res: Vec<SelectCommunication> =
            activity_communication::select_all(&conn, &Uuid::new_v4(), &activity)
                .expect("to select the activity constraint from the database");
        assert_eq!(res.len(), 0, "It should be of length 0");
        Ok(())
    });
}

#[test]
fn select_all_not_found_2() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, activity, communication) = setup(&conn);
        activity_communication::create(
            &conn,
            &uid,
            NewActivityCommunication {
                activity_id: activity,
                communication_id: communication,
            },
        )
        .expect("to create the activity constraint communication in the db");
        let res: Vec<SelectCommunication> =
            activity_communication::select_all(&conn, &uid, &Uuid::new_v4())
                .expect("to select the activity constraint from the database");
        assert_eq!(res.len(), 0, "It should be of length 0");
        Ok(())
    });
}

#[test]
fn check_if_has_reason_found() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let uid = create_test_user(&conn);
        let activity = create_activity(&conn, &uid, true, None, None);
        let communication = communication::create(
            &conn,
            NewCommunication {
                uid,
                type_: CommunicationType::Email,
                reason: CommunicationReasonType::Alert,
            },
        )
        .expect("to create the communication in the database");
        activity_communication::create(
            &conn,
            &uid,
            NewActivityCommunication {
                activity_id: activity,
                communication_id: communication.id,
            },
        )
        .expect("to create the activity constraint communication in the db");
        let mut res = activity_communication::check_if_activity_has_reason(
            &conn,
            &uid,
            &activity,
            vec![CommunicationReasonType::Alert],
        )
        .expect("to fetch if a similar record exists in the db");
        assert_eq!(res.len(), 1, "There should be 1 object");
        assert_eq!(res.pop().unwrap(), communication.id, "The id should match");
        Ok(())
    });
}

#[test]
fn check_if_has_reason_search_multiple() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let uid = create_test_user(&conn);
        let activity = create_activity(&conn, &uid, true, None, None);
        let communication = communication::create(
            &conn,
            NewCommunication {
                uid,
                type_: CommunicationType::Email,
                reason: CommunicationReasonType::ForcedAlert,
            },
        )
        .expect("to create the communication in the database");
        activity_communication::create(
            &conn,
            &uid,
            NewActivityCommunication {
                activity_id: activity,
                communication_id: communication.id,
            },
        )
        .expect("to create the activity constraint communication in the db");
        let mut res = activity_communication::check_if_activity_has_reason(
            &conn,
            &uid,
            &activity,
            vec![
                CommunicationReasonType::Alert,
                CommunicationReasonType::ForcedAlert,
            ],
        )
        .expect("to fetch if a similar record exists in the db");
        assert_eq!(res.len(), 1, "There should be 1 object");
        assert_eq!(res.pop().unwrap(), communication.id, "The id should match");
        Ok(())
    });
}

#[test]
fn check_if_has_reason_search_not_found() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let uid = create_test_user(&conn);
        let activity = create_activity(&conn, &uid, true, None, None);
        let communication = communication::create(
            &conn,
            NewCommunication {
                uid,
                type_: CommunicationType::Email,
                reason: CommunicationReasonType::Check,
            },
        )
        .expect("to create the communication in the database");
        activity_communication::create(
            &conn,
            &uid,
            NewActivityCommunication {
                activity_id: activity,
                communication_id: communication.id,
            },
        )
        .expect("to create the activity constraint communication in the db");
        let res = activity_communication::check_if_activity_has_reason(
            &conn,
            &uid,
            &activity,
            vec![
                CommunicationReasonType::Alert,
                CommunicationReasonType::ForcedAlert,
            ],
        )
        .expect("to fetch if a similar record exists in the db");
        assert_eq!(res.len(), 0, "There should be 1 object");
        Ok(())
    });
}

#[test]
fn check_if_has_reason_in_constraints() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let uid = create_test_user(&conn);
        let activity = create_activity(&conn, &uid, true, None, None);
        let constraint = create_activity_constraint(
            &conn,
            &uid,
            &activity,
            false,
            Some(Utc::now() - Duration::minutes(1)),
            Some(Utc::now() - Duration::seconds(20)),
            Some(Utc::now() - Duration::seconds(5)),
            None,
        );
        let communication = communication::create(
            &conn,
            NewCommunication {
                uid,
                type_: CommunicationType::Email,
                reason: CommunicationReasonType::ForcedAlert,
            },
        )
        .expect("to create the communication in the database");
        activity_constraint_communication::create(
            &conn,
            &uid,
            NewActivityConstraintCommunication {
                constraint_id: constraint,
                communication_id: communication.id,
            },
        )
        .expect("to create the activity constraint communication in the db");
        let mut res = activity_communication::check_if_activity_has_reason(
            &conn,
            &uid,
            &activity,
            vec![
                CommunicationReasonType::Alert,
                CommunicationReasonType::ForcedAlert,
            ],
        )
        .expect("to fetch if a similar record exists in the db");
        assert_eq!(res.len(), 1, "There should be 1 object");
        assert_eq!(res.pop().unwrap(), communication.id, "The id should match");
        Ok(())
    });
}

use crate::activity_contact::{self};

use diesel::PgConnection;

use uuid::Uuid;

use super::*;

fn setup() -> (PgConnection, Uuid) {
    let connection = establish_connection();
    let user_uuid = create_test_user(&connection);
    (connection, user_uuid)
}

#[test]
fn create_activity_contact_ok() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        create_activity_contact(
            &conn,
            &uid,
            &create_activity(&conn, &uid, true, None, None),
            &create_contact(&conn, &uid),
        );
        Ok(())
    })
}

#[test]
fn create_activity_contact_forbidden_activity() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let uid2 = create_test_user(&conn);
        let result = crate::activity_contact::create(
            &conn,
            &uid,
            crate::activity_contact::NewActivityContact {
                activity_id: create_activity(&conn, &uid, true, None, None),
                contact_id: create_contact(&conn, &uid2),
            },
        );
        assert!(result.is_err());
        Ok(())
    })
}

#[test]
fn create_activity_contact_forbidden_contact() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let uid2 = create_test_user(&conn);
        let result = crate::activity_contact::create(
            &conn,
            &uid,
            crate::activity_contact::NewActivityContact {
                activity_id: create_activity(&conn, &uid2, true, None, None),
                contact_id: create_contact(&conn, &uid),
            },
        );
        assert!(result.is_err());
        Ok(())
    })
}

#[test]
fn create_activity_contact_forbidden_both() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let uid2 = create_test_user(&conn);
        let result = crate::activity_contact::create(
            &conn,
            &uid,
            crate::activity_contact::NewActivityContact {
                activity_id: create_activity(&conn, &uid2, true, None, None),
                contact_id: create_contact(&conn, &uid2),
            },
        );
        assert!(result.is_err());
        Ok(())
    })
}

#[test]
fn create_activity_contact_duplicate() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_id = create_activity(&conn, &uid, true, None, None);
        let contact_id = create_contact(&conn, &uid);
        create_activity_contact(&conn, &uid, &activity_id, &contact_id);
        let result = crate::activity_contact::create(
            &conn,
            &uid,
            crate::activity_contact::NewActivityContact {
                activity_id: activity_id,
                contact_id: contact_id,
            },
        );
        assert!(result.is_err());
        Ok(())
    })
}

#[test]
fn create_activity_contact_readonly() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_id = create_activity(&conn, &uid, true, None, None);
        let contact_id = create_contact(&conn, &uid);
        crate::activity::end(&conn, &uid, &activity_id).unwrap();
        let err = crate::activity_contact::create(
            &conn,
            &uid,
            crate::activity_contact::NewActivityContact {
                activity_id: activity_id,
                contact_id: contact_id,
            },
        )
        .expect_err("The create shouldn't work");
        assert_eq!(
            err,
            crate::error::IqiperDatabaseError::ReadOnly,
            "Should be read only"
        );
        Ok(())
    })
}

#[test]
fn select_all() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        create_activity_contact(
            &conn,
            &uid,
            &create_activity(&conn, &uid, true, None, None),
            &create_contact(&conn, &uid),
        );
        create_activity_contact(
            &conn,
            &uid,
            &create_activity(&conn, &uid, true, None, None),
            &create_contact(&conn, &uid),
        );
        create_activity_contact(
            &conn,
            &uid,
            &create_activity(&conn, &uid, true, None, None),
            &create_contact(&conn, &uid),
        );
        let res = activity_contact::select_all(&conn, &uid)
            .expect("To fetch every activity contact for that user");
        assert_eq!(res.len(), 3, "There should be 3 records");
        Ok(())
    })
}

#[test]
fn select_by_id() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_id = create_activity(&conn, &uid, true, None, None);
        let contact_id = create_contact(&conn, &uid);
        let _id = create_activity_contact(&conn, &uid, &activity_id, &contact_id);
        let res = activity_contact::select_by_id(&conn, &uid, &activity_id, &contact_id)
            .expect("To fetch every activity contact for that user");
        assert_eq!(res.activity_id, activity_id, "ids don't match");
        assert_eq!(res.contact_id, contact_id, "ids don't match");
        Ok(())
    })
}

#[test]
fn select_contacts_for_activity() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_id = create_activity(&conn, &uid, true, None, None);
        let contact_id = create_contact(&conn, &uid);
        let _id = create_activity_contact(&conn, &uid, &activity_id, &contact_id);
        let res = activity_contact::select_contacts_for_activity(&conn, &uid, &activity_id)
            .expect("To fetch every activity contacts for that activity");
        assert_eq!(res.len(), 1, "should be 1");
        assert_eq!(res[0].id, contact_id, "ids don't match");
        Ok(())
    })
}

#[test]
fn select_contacts_for_activity_unlogged() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_id = create_activity(&conn, &uid, true, None, None);
        let contact_id = create_contact(&conn, &uid);
        let _id = create_activity_contact(&conn, &uid, &activity_id, &contact_id);
        let res = activity_contact::select_contacts_for_activity_unlogged(&conn, &activity_id)
            .expect("To fetch every activity contacts for that activity");
        assert_eq!(res.len(), 1, "should be 1");
        assert_eq!(res[0].activity_id, activity_id, "ids don't match");
        assert_eq!(res[0].contact_id, contact_id, "ids don't match");
        Ok(())
    })
}

#[test]
fn delete_wrong_uid() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_id = create_activity(&conn, &uid, true, None, None);
        let contact_id = create_contact(&conn, &uid);
        create_activity_contact(&conn, &uid, &activity_id, &contact_id);
        activity_contact::delete(&conn, &Uuid::new_v4(), &activity_id, &contact_id)
            .expect_err("the activity contact shouldn't be deleted");
        Ok(())
    })
}

#[test]
fn delete_wrong_activity_activity_id() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_id = create_activity(&conn, &uid, true, None, None);
        let contact_id = create_contact(&conn, &uid);
        create_activity_contact(&conn, &uid, &activity_id, &contact_id);
        activity_contact::delete(&conn, &uid, &Uuid::new_v4(), &contact_id)
            .expect_err("the activity contact shouldn't be deleted");
        Ok(())
    })
}

#[test]
fn delete_wrong_contact_id() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_id = create_activity(&conn, &uid, true, None, None);
        let contact_id = create_contact(&conn, &uid);
        create_activity_contact(&conn, &uid, &activity_id, &contact_id);
        let _res = activity_contact::delete(&conn, &uid, &activity_id, &Uuid::new_v4())
            .expect_err("the activity contact shouldn't be deleted");
        Ok(())
    })
}

#[test]
fn delete() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_id = create_activity(&conn, &uid, true, None, None);
        let contact_id = create_contact(&conn, &uid);
        let _id = create_activity_contact(&conn, &uid, &activity_id, &contact_id);
        let res = activity_contact::select_by_id(&conn, &uid, &activity_id, &contact_id)
            .expect("To fetch every activity contact for that user");
        assert_eq!(res.activity_id, activity_id, "ids don't match");
        assert_eq!(res.contact_id, contact_id, "ids don't match");
        let n = activity_contact::delete(&conn, &uid, &activity_id, &contact_id)
            .expect("The delete not to fail");
        assert_eq!(n, 1, "There should be 1 modified row");
        let _res2 = activity_contact::select_by_id(&conn, &uid, &activity_id, &contact_id)
            .expect_err("The record should've been deleted");
        Ok(())
    })
}

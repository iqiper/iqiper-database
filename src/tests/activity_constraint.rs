use super::*;
use crate::activity_constraint::{self, NewActivityConstraint};
use crate::common::UpdateResponse;
use crate::enums::ActivityConstraintType;
use chrono::{Duration, Utc};
use diesel::PgConnection;
use uuid::Uuid;

fn setup() -> (PgConnection, Uuid) {
    let connection = establish_connection();
    let user_uuid = create_test_user(&connection);
    (connection, user_uuid)
}

#[test]
fn create_activity_constraint_ok() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        create_activity_constraint(
            &conn,
            &uid,
            &create_activity(&conn, &uid, true, None, None),
            true,
            None,
            None,
            None,
            None,
        );
        Ok(())
    });
}

#[test]
fn create_activity_constraint_read_only() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_id = create_activity(&conn, &uid, true, None, None);
        let expected = NewActivityConstraint {
            activity_id: activity_id,
            type_: ActivityConstraintType::Timer,
            alert_message: None,
            start_date: chrono::Utc::now(),
            notify_date: Some(chrono::Utc::now() + chrono::Duration::seconds(5)),
            end_date: chrono::Utc::now() + chrono::Duration::seconds(10),
        };
        crate::activity::end(&conn, &uid, &activity_id).unwrap();
        let err = activity_constraint::create(&conn, &uid, expected.clone())
            .expect_err("Should've failed");
        assert_eq!(
            err,
            crate::error::IqiperDatabaseError::ReadOnly,
            "Should be read only"
        );
        Ok(())
    });
}

#[test]
fn create_activity_constraint_bad_activity_id() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let expected = NewActivityConstraint {
            activity_id: Uuid::new_v4(),
            type_: ActivityConstraintType::Timer,
            alert_message: None,
            start_date: chrono::Utc::now(),
            notify_date: Some(chrono::Utc::now() + chrono::Duration::seconds(5)),
            end_date: chrono::Utc::now() + chrono::Duration::seconds(10),
        };
        activity_constraint::create(&conn, &uid, expected.clone())
            .expect_err("The foreign key constraint should have made this call fail");
        Ok(())
    });
}

#[test]
fn create_activity_constraint_forbidden() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let uid2 = create_test_user(&conn);
        let activity_id = create_activity(&conn, &uid2, true, None, None);
        let expected = NewActivityConstraint {
            activity_id: activity_id,
            type_: ActivityConstraintType::Timer,
            alert_message: None,
            start_date: chrono::Utc::now(),
            notify_date: Some(chrono::Utc::now() + chrono::Duration::seconds(5)),
            end_date: chrono::Utc::now() + chrono::Duration::seconds(10),
        };
        activity_constraint::create(&conn, &uid, expected.clone())
            .expect_err("The foreign key constraint should have made this call fail");
        Ok(())
    });
}

#[test]
fn create_activity_w_start_date_before_end_date() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_id = create_activity(&conn, &uid, true, None, None);
        let now = chrono::Utc::now();
        let expected = NewActivityConstraint {
            activity_id,
            type_: ActivityConstraintType::Timer,
            alert_message: None,
            start_date: now + chrono::Duration::seconds(15),
            notify_date: Some(now + chrono::Duration::seconds(5)),
            end_date: now + chrono::Duration::seconds(10),
        };
        activity_constraint::create(&conn, &uid, expected.clone())
            .expect_err("The logic checker should've make this fail");
        Ok(())
    });
}

#[test]
fn create_activity_w_timer_check_date_equal_start_date() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_id = create_activity(&conn, &uid, true, None, None);
        let now = chrono::Utc::now();
        let expected = NewActivityConstraint {
            activity_id,
            type_: ActivityConstraintType::Timer,
            alert_message: None,
            start_date: now,
            notify_date: Some(now),
            end_date: now + chrono::Duration::seconds(10),
        };
        activity_constraint::create(&conn, &uid, expected.clone())
            .expect_err("The logic checker should've make this fail");
        Ok(())
    });
}

#[test]
fn create_activity_w_timer_check_date_equal_end_date() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_id = create_activity(&conn, &uid, true, None, None);
        let now = chrono::Utc::now();
        let expected = NewActivityConstraint {
            activity_id,
            type_: ActivityConstraintType::Timer,
            alert_message: None,
            start_date: now,
            notify_date: Some(now + chrono::Duration::seconds(10)),
            end_date: now + chrono::Duration::seconds(10),
        };
        activity_constraint::create(&conn, &uid, expected.clone())
            .expect_err("The logic checker should've make this fail");
        Ok(())
    });
}

#[test]
fn create_activity_w_timer_check_date_lesser_start_date() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_id = create_activity(&conn, &uid, true, None, None);
        let now = chrono::Utc::now();
        let expected = NewActivityConstraint {
            activity_id,
            type_: ActivityConstraintType::Timer,
            alert_message: None,
            start_date: now,
            notify_date: Some(now - chrono::Duration::seconds(10)),
            end_date: now + chrono::Duration::seconds(10),
        };
        activity_constraint::create(&conn, &uid, expected.clone())
            .expect_err("The logic checker should've make this fail");
        Ok(())
    });
}

#[test]
fn create_activity_w_timer_check_date_greater_end_date() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_id = create_activity(&conn, &uid, true, None, None);
        let now = chrono::Utc::now();
        let expected = NewActivityConstraint {
            activity_id,
            type_: ActivityConstraintType::Timer,
            alert_message: None,
            start_date: now,
            notify_date: Some(now + chrono::Duration::seconds(10)),
            end_date: now + chrono::Duration::seconds(9),
        };
        activity_constraint::create(&conn, &uid, expected.clone())
            .expect_err("The logic checker should've make this fail");
        Ok(())
    });
}

#[test]
fn delete_ok() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_id = create_activity(&conn, &uid, true, None, None);
        let id =
            create_activity_constraint(&conn, &uid, &activity_id, true, None, None, None, None);
        let res = activity_constraint::delete(&conn, &uid, &activity_id, &id)
            .expect("the activity should be deleted");
        assert_eq!(res, 1, "There should be 1 deleted record");
        let _obj = activity_constraint::select_by_id(&conn, &uid, &activity_id, &id)
            .expect_err("the record should be deleted");
        Ok(())
    });
}

#[test]
fn delete_wrong_uid() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_id = create_activity(&conn, &uid, true, None, None);
        let id =
            create_activity_constraint(&conn, &uid, &activity_id, true, None, None, None, None);
        activity_constraint::delete(&conn, &Uuid::new_v4(), &activity_id, &id)
            .expect_err("the activity shouldn't be deleted");
        Ok(())
    });
}

#[test]
fn delete_forbidden() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_id = create_activity(&conn, &uid, true, None, None);
        let uid2 = create_test_user(&conn);
        let id =
            create_activity_constraint(&conn, &uid, &activity_id, true, None, None, None, None);
        activity_constraint::delete(&conn, &uid2, &activity_id, &id)
            .expect_err("the activity shouldn't be deleted");
        Ok(())
    });
}

#[test]
fn delete_wrong_activity_id() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_id = create_activity(&conn, &uid, true, None, None);
        let id =
            create_activity_constraint(&conn, &uid, &activity_id, true, None, None, None, None);
        activity_constraint::delete(&conn, &uid, &Uuid::new_v4(), &id)
            .expect_err("the activity shouldn't be deleted");
        Ok(())
    });
}

#[test]
fn delete_wrong_id() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_id = create_activity(&conn, &uid, true, None, None);
        create_activity_constraint(&conn, &uid, &activity_id, true, None, None, None, None);
        activity_constraint::delete(&conn, &uid, &activity_id, &Uuid::new_v4())
            .expect_err("the activity shouldn't be deleted");
        Ok(())
    });
}

#[test]
fn select_all() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_id = create_activity(&conn, &uid, true, None, None);
        create_activity_constraint(&conn, &uid, &activity_id, true, None, None, None, None);
        create_activity_constraint(&conn, &uid, &activity_id, true, None, None, None, None);
        create_activity_constraint(&conn, &uid, &activity_id, true, None, None, None, None);
        let res =
            activity_constraint::select_all(&conn, &uid).expect("the activities should be fetched");
        assert_eq!(res.len(), 3, "There should be 3 objects");
        Ok(())
    });
}

#[test]
fn select_bulk_id() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_id = create_activity(&conn, &uid, true, None, None);
        let mut ids = vec![
            create_activity_constraint(&conn, &uid, &activity_id, true, None, None, None, None),
            create_activity_constraint(&conn, &uid, &activity_id, true, None, None, None, None),
            create_activity_constraint(&conn, &uid, &activity_id, true, None, None, None, None),
        ];
        ids.sort();
        let res = activity_constraint::select_bulk_by_id_unlogged(&conn, ids.clone())
            .expect("the activities should be fetched");
        assert_eq!(res.len(), 3, "There should be 3 objects");
        let mut res = res.iter().map(|x| x.id).collect::<Vec<Uuid>>();
        res.sort();
        for (a, b) in ids.iter().zip(res) {
            assert_eq!(*a, b, "ids should match");
        }
        Ok(())
    });
}

#[test]
fn select_bulk_id_one_is_missing() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_id = create_activity(&conn, &uid, true, None, None);
        let mut ids = vec![
            Uuid::new_v4(),
            create_activity_constraint(&conn, &uid, &activity_id, true, None, None, None, None),
            create_activity_constraint(&conn, &uid, &activity_id, true, None, None, None, None),
            create_activity_constraint(&conn, &uid, &activity_id, true, None, None, None, None),
        ];
        ids.drain(0..1);
        ids.sort();
        let res = activity_constraint::select_bulk_by_id_unlogged(&conn, ids.clone())
            .expect("the activities should be fetched");
        assert_eq!(res.len(), 3, "There should be 3 objects");
        let mut res = res.iter().map(|x| x.id).collect::<Vec<Uuid>>();
        res.sort();
        for (a, b) in ids.iter().zip(res) {
            assert_eq!(*a, b, "ids should match");
        }
        Ok(())
    });
}

#[test]
fn select_all_paginated_offset_unlogged() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let uid2 = create_test_user(&conn);
        let activity_id = create_activity(&conn, &uid, true, None, None);
        let activity_id2 = create_activity(&conn, &uid2, true, None, None);
        create_activity_constraint(&conn, &uid, &activity_id, true, None, None, None, None);
        create_activity_constraint(&conn, &uid2, &activity_id2, true, None, None, None, None);
        create_activity_constraint(&conn, &uid, &activity_id, true, None, None, None, None);
        let res = activity_constraint::select_all_paginated_unlogged(&conn, None, 3)
            .expect("the activities should be fetched");
        assert_eq!(res.len(), 3, "There should be 3 objects");
        let res2 = activity_constraint::select_all_paginated_unlogged(
            &conn,
            Some((res[1].id, res[1].created_at)),
            1,
        )
        .expect("the activities should be fetched");
        assert_eq!(res2.len(), 1, "There should be 1 object");
        assert_eq!(res2[0], res[2], "The object mismatch");
        Ok(())
    });
}

#[test]
fn select_all_active_paginated_offset_unlogged() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let uid2 = create_test_user(&conn);
        let activity_id = create_activity(
            &conn,
            &uid,
            true,
            Some(Utc::now() + Duration::hours(1)),
            None,
        );
        let activity_id2 = create_activity(
            &conn,
            &uid2,
            false,
            Some(Utc::now() - Duration::hours(1)),
            None,
        );
        create_activity_constraint(
            &conn,
            &uid,
            &activity_id,
            false,
            Some(Utc::now() + Duration::hours(1)),
            None,
            Some(Utc::now() + Duration::weeks(2)),
            None,
        );
        let expected = create_activity_constraint(
            &conn,
            &uid2,
            &activity_id2,
            false,
            Some(Utc::now()),
            None,
            Some(Utc::now() + Duration::seconds(60)),
            None,
        );
        create_activity_constraint(
            &conn,
            &uid2,
            &activity_id2,
            false,
            Some(Utc::now() - Duration::minutes(1)),
            None,
            Some(Utc::now() - Duration::seconds(2)),
            None,
        );
        let res = activity_constraint::select_all_active_paginated_unlogged(&conn, None, 10)
            .expect("the activities should be fetched");
        assert_eq!(res.len(), 1, "There should be 1 object");
        assert_eq!(res[0].id, expected, "The objects mismatch");
        Ok(())
    });
}

#[test]
fn select_all_inactive_paginated_offset_unlogged() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let uid2 = create_test_user(&conn);
        let activity_id = create_activity(
            &conn,
            &uid,
            true,
            Some(Utc::now() + Duration::hours(1)),
            None,
        );
        let activity_id2 = create_activity(
            &conn,
            &uid2,
            false,
            Some(Utc::now() - Duration::hours(1)),
            None,
        );
        create_activity_constraint(
            &conn,
            &uid,
            &activity_id,
            false,
            Some(Utc::now() + Duration::hours(1)),
            None,
            Some(Utc::now() + Duration::weeks(2)),
            None,
        );
        create_activity_constraint(
            &conn,
            &uid2,
            &activity_id2,
            false,
            Some(Utc::now()),
            None,
            Some(Utc::now() + Duration::seconds(60)),
            None,
        );
        let expected = create_activity_constraint(
            &conn,
            &uid2,
            &activity_id2,
            false,
            Some(Utc::now() - Duration::minutes(1)),
            None,
            Some(Utc::now() - Duration::seconds(2)),
            None,
        );
        let res = activity_constraint::select_all_inactive_since_paginated_unlogged(
            &conn,
            Utc::now() - Duration::seconds(30),
            None,
            10,
        )
        .expect("the activities should be fetched");
        assert_eq!(res.len(), 1, "There should be 1 object");
        assert_eq!(res[0].id, expected, "The objects mismatch");
        Ok(())
    });
}

#[test]
fn select_all_active_since_paginated_offset_unlogged() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let uid2 = create_test_user(&conn);
        let activity_id = create_activity(
            &conn,
            &uid,
            true,
            Some(Utc::now() + Duration::hours(1)),
            None,
        );
        let activity_id2 = create_activity(
            &conn,
            &uid2,
            false,
            Some(Utc::now() - Duration::hours(1)),
            None,
        );
        create_activity_constraint(
            &conn,
            &uid,
            &activity_id,
            false,
            Some(Utc::now() + Duration::hours(1)),
            None,
            Some(Utc::now() + Duration::weeks(2)),
            None,
        );
        let expected = create_activity_constraint(
            &conn,
            &uid2,
            &activity_id2,
            false,
            Some(Utc::now()),
            None,
            Some(Utc::now() + Duration::seconds(60)),
            None,
        );
        create_activity_constraint(
            &conn,
            &uid2,
            &activity_id2,
            false,
            Some(Utc::now() - Duration::minutes(1)),
            None,
            Some(Utc::now() - Duration::seconds(2)),
            None,
        );
        let res = activity_constraint::select_all_active_since_paginated_unlogged(
            &conn,
            Utc::now() - Duration::seconds(30),
            None,
            10,
        )
        .expect("the activities should be fetched");
        assert_eq!(res.len(), 1, "There should be 1 object");
        assert_eq!(res[0].id, expected, "The objects mismatch");
        Ok(())
    });
}

#[test]
fn select_all_by_activity() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_id = create_activity(&conn, &uid, true, None, None);
        create_activity_constraint(&conn, &uid, &activity_id, true, None, None, None, None);
        create_activity_constraint(&conn, &uid, &activity_id, true, None, None, None, None);
        create_activity_constraint(&conn, &uid, &activity_id, true, None, None, None, None);
        let res = activity_constraint::select_all_by_activity(&conn, &uid, &activity_id)
            .expect("the activities should be fetched");
        assert_eq!(res.len(), 3, "There should be 3 objects");
        Ok(())
    });
}

#[test]
fn select_id() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_id = create_activity(&conn, &uid, true, None, None);
        let id =
            create_activity_constraint(&conn, &uid, &activity_id, true, None, None, None, None);
        let _res = activity_constraint::select_by_id(&conn, &uid, &activity_id, &id)
            .expect("the activities should be fetched");
        Ok(())
    });
}

#[test]
fn select_unique_id() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_id = create_activity(&conn, &uid, true, None, None);
        let id =
            create_activity_constraint(&conn, &uid, &activity_id, true, None, None, None, None);
        let _res = activity_constraint::select_by_unique_id(&conn, &id)
            .expect("the activities should be fetched");
        Ok(())
    });
}

#[test]
fn select_uuid() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_id = create_activity(&conn, &uid, true, None, None);
        let id =
            create_activity_constraint(&conn, &uid, &activity_id, true, None, None, None, None);
        let res = activity_constraint::select_uid(&conn, &id).expect("the uid should be fetched");
        assert_eq!(res, uid, "User id mismatch");
        Ok(())
    });
}

#[test]
fn modifiy_activity_constraint_ok() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_id = create_activity(&conn, &uid, true, None, None);
        let constraint_id =
            create_activity_constraint(&conn, &uid, &activity_id, true, None, None, None, None);
        let res = activity_constraint::select_by_id(&conn, &uid, &activity_id, &constraint_id)
            .expect("the activities should be fetched");
        let expected = NewActivityConstraint {
            activity_id: res.activity_id,
            type_: res.type_,
            alert_message: None,
            start_date: chrono::Utc::now() + chrono::Duration::days(5),
            notify_date: Some(
                chrono::Utc::now() + chrono::Duration::days(5) + chrono::Duration::seconds(5),
            ),
            end_date: chrono::Utc::now()
                + chrono::Duration::days(5)
                + chrono::Duration::seconds(10),
        };
        let received: UpdateResponse = activity_constraint::modify(
            &conn,
            &uid,
            &activity_id,
            &constraint_id,
            expected.clone(),
        )
        .expect("The update object");
        assert_eq!(received.id, constraint_id, "id's don't match");
        Ok(())
    });
}

#[test]
fn modifiy_activity_constraint_readonly() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_id = create_activity(&conn, &uid, true, None, None);
        let constraint_id =
            create_activity_constraint(&conn, &uid, &activity_id, true, None, None, None, None);
        let res = activity_constraint::select_by_id(&conn, &uid, &activity_id, &constraint_id)
            .expect("the activities should be fetched");
        crate::activity::end(&conn, &uid, &activity_id).unwrap();
        let expected = NewActivityConstraint {
            activity_id: res.activity_id,
            type_: res.type_,
            alert_message: None,
            start_date: chrono::Utc::now() + chrono::Duration::days(5),
            notify_date: Some(
                chrono::Utc::now() + chrono::Duration::days(5) + chrono::Duration::seconds(5),
            ),
            end_date: chrono::Utc::now()
                + chrono::Duration::days(5)
                + chrono::Duration::seconds(10),
        };
        let err = activity_constraint::modify(
            &conn,
            &uid,
            &activity_id,
            &constraint_id,
            expected.clone(),
        )
        .expect_err("Should'nt be update");
        assert_eq!(
            err,
            crate::error::IqiperDatabaseError::ReadOnly,
            "Should be readonly"
        );
        Ok(())
    });
}

#[test]
fn modifiy_activity_constraint_forbidden() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let uid2 = create_test_user(&conn);
        let activity_id = create_activity(&conn, &uid, true, None, None);
        let constraint_id =
            create_activity_constraint(&conn, &uid, &activity_id, true, None, None, None, None);
        let res = activity_constraint::select_by_id(&conn, &uid, &activity_id, &constraint_id)
            .expect("the activities should be fetched");
        let expected = NewActivityConstraint {
            activity_id: res.activity_id,
            type_: res.type_,
            alert_message: None,
            start_date: chrono::Utc::now() + chrono::Duration::days(5),
            notify_date: Some(
                chrono::Utc::now() + chrono::Duration::days(5) + chrono::Duration::seconds(5),
            ),
            end_date: chrono::Utc::now()
                + chrono::Duration::days(5)
                + chrono::Duration::seconds(10),
        };
        activity_constraint::modify(&conn, &uid2, &activity_id, &constraint_id, expected.clone())
            .expect_err("Shouldn't be able to modify constraint not belonging to the user");
        Ok(())
    });
}

#[test]
fn modifiy_activity_w_start_date_before_end_date() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let _uid2 = create_test_user(&conn);
        let activity_id = create_activity(&conn, &uid, true, None, None);
        let constraint_id =
            create_activity_constraint(&conn, &uid, &activity_id, true, None, None, None, None);
        let _res = activity_constraint::select_by_id(&conn, &uid, &activity_id, &constraint_id)
            .expect("the activities should be fetched");
        let now = Utc::now();
        let expected = NewActivityConstraint {
            activity_id,
            type_: ActivityConstraintType::Timer,
            alert_message: None,
            start_date: now + chrono::Duration::seconds(15),
            notify_date: Some(now + chrono::Duration::seconds(5)),
            end_date: now + chrono::Duration::seconds(10),
        };
        activity_constraint::modify(&conn, &uid, &activity_id, &constraint_id, expected.clone())
            .expect_err("The logic checker should've make this fail");
        Ok(())
    });
}

#[test]
fn modifiy_activity_w_timer_check_date_equal_start_date() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let _uid2 = create_test_user(&conn);
        let activity_id = create_activity(&conn, &uid, true, None, None);
        let constraint_id =
            create_activity_constraint(&conn, &uid, &activity_id, true, None, None, None, None);
        let _res = activity_constraint::select_by_id(&conn, &uid, &activity_id, &constraint_id)
            .expect("the activities should be fetched");
        let now = Utc::now();
        let expected = NewActivityConstraint {
            activity_id,
            type_: ActivityConstraintType::Timer,
            alert_message: None,
            start_date: now,
            notify_date: Some(now),
            end_date: now + chrono::Duration::seconds(10),
        };
        activity_constraint::modify(&conn, &uid, &activity_id, &constraint_id, expected.clone())
            .expect_err("The logic checker should've make this fail");
        Ok(())
    });
}

#[test]
fn modifiy_activity_w_timer_check_date_equal_end_date() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let _uid2 = create_test_user(&conn);
        let activity_id = create_activity(&conn, &uid, true, None, None);
        let constraint_id =
            create_activity_constraint(&conn, &uid, &activity_id, true, None, None, None, None);
        let _res = activity_constraint::select_by_id(&conn, &uid, &activity_id, &constraint_id)
            .expect("the activities should be fetched");
        let now = Utc::now();
        let expected = NewActivityConstraint {
            activity_id,
            type_: ActivityConstraintType::Timer,
            alert_message: None,
            start_date: now,
            notify_date: Some(now + chrono::Duration::seconds(10)),
            end_date: now + chrono::Duration::seconds(10),
        };
        activity_constraint::modify(&conn, &uid, &activity_id, &constraint_id, expected.clone())
            .expect_err("The logic checker should've make this fail");
        Ok(())
    });
}

#[test]
fn modifiy_activity_w_timer_check_date_lesser_start_date() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let _uid2 = create_test_user(&conn);
        let activity_id = create_activity(&conn, &uid, true, None, None);
        let constraint_id =
            create_activity_constraint(&conn, &uid, &activity_id, true, None, None, None, None);
        let _res = activity_constraint::select_by_id(&conn, &uid, &activity_id, &constraint_id)
            .expect("the activities should be fetched");
        let now = Utc::now();
        let expected = NewActivityConstraint {
            activity_id,
            type_: ActivityConstraintType::Timer,
            alert_message: None,
            start_date: now,
            notify_date: Some(now - chrono::Duration::seconds(10)),
            end_date: now + chrono::Duration::seconds(10),
        };
        activity_constraint::modify(&conn, &uid, &activity_id, &constraint_id, expected.clone())
            .expect_err("The logic checker should've make this fail");
        Ok(())
    });
}

#[test]
fn modifiy_activity_w_timer_check_date_greater_end_date() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let _uid2 = create_test_user(&conn);
        let activity_id = create_activity(&conn, &uid, true, None, None);
        let constraint_id =
            create_activity_constraint(&conn, &uid, &activity_id, true, None, None, None, None);
        let _res = activity_constraint::select_by_id(&conn, &uid, &activity_id, &constraint_id)
            .expect("the activities should be fetched");
        let now = Utc::now();
        let expected = NewActivityConstraint {
            activity_id,
            type_: ActivityConstraintType::Timer,
            alert_message: None,
            start_date: now,
            notify_date: Some(now + chrono::Duration::seconds(10)),
            end_date: now + chrono::Duration::seconds(9),
        };
        activity_constraint::modify(&conn, &uid, &activity_id, &constraint_id, expected.clone())
            .expect_err("The logic checker should've make this fail");
        Ok(())
    });
}

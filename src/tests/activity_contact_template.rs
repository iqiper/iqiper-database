use super::*;
use crate::activity_contact_template::{self, NewActivityContactTemplate};
use crate::activity_template::{self, NewActivityTemplate};
use crate::contact::{self, NewContact};
use crate::enums::ContactType;
use diesel::PgConnection;
use std::convert::From;
use uuid::Uuid;

use super::{create_test_user, establish_connection};

fn setup() -> (PgConnection, Uuid) {
    let connection = establish_connection();
    let user_uuid = create_test_user(&connection);
    (connection, user_uuid)
}

fn create_activity_template(conn: &PgConnection, uid: Uuid) -> Uuid {
    let expected_activity = NewActivityTemplate {
        name: format!("Name {}", Uuid::new_v4()),
        uid: uid,
        icon: None,
        alert_message: None,
    };
    let created_activity = activity_template::create(conn, expected_activity.clone())
        .expect("activity should've been created");
    created_activity.id
}

fn create_contact_template(conn: &PgConnection, uid: Uuid) -> Uuid {
    let expected_contact = NewContact {
        nickname: String::from("Hello world"),
        favorite: false,
        uid: uid,
        type_: ContactType::Email,
        email: Some(format!("{}@world", Uuid::new_v4())),
        target_uid: None,
    };
    let created_contact =
        contact::create(conn, expected_contact.clone()).expect("contact should've been created");
    assert_eq!(
        expected_contact.nickname, created_contact.nickname,
        "names do not match"
    );
    assert_eq!(
        expected_contact.uid, created_contact.uid,
        "uids do not match"
    );
    assert_eq!(
        expected_contact.favorite, created_contact.favorite,
        "favorite do not match"
    );
    assert_eq!(
        expected_contact.type_, created_contact.type_,
        "type do not match"
    );
    assert_eq!(
        expected_contact.email, created_contact.email,
        "emails do not match"
    );
    assert_eq!(
        expected_contact.target_uid, created_contact.target_uid,
        "target_uids do not match"
    );
    created_contact.id
}

fn create_activity_contact_template(
    conn: &PgConnection,
    uid: &Uuid,
    activity_template_id: Uuid,
    contact_id: Uuid,
) -> (Uuid, Uuid) {
    let expected = NewActivityContactTemplate {
        activity_template_id: activity_template_id,
        contact_id: contact_id,
    };
    let created_contact = activity_contact_template::create(conn, uid, expected.clone())
        .expect("activity_contact_template should've been created");
    assert_eq!(
        expected.activity_template_id, created_contact.activity_template_id,
        "activity_template_ids do not match"
    );
    assert_eq!(
        expected.contact_id, created_contact.contact_id,
        "contact_ids do not match"
    );
    (
        created_contact.activity_template_id,
        created_contact.contact_id,
    )
}

#[test]
fn create_activity_contact_template_ok() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        create_activity_contact_template(
            &conn,
            &uid,
            create_activity_template(&conn, uid),
            create_contact_template(&conn, uid),
        );
        Ok(())
    })
}

#[test]
fn create_activity_contact_template_duplicate() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_template_id = create_activity_template(&conn, uid);
        let contact_id = create_contact_template(&conn, uid);
        create_activity_contact_template(&conn, &uid, activity_template_id, contact_id);
        let result = activity_contact_template::create(
            &conn,
            &uid,
            crate::activity_contact_template::NewActivityContactTemplate {
                activity_template_id,
                contact_id,
            },
        );
        assert!(result.is_err());
        Ok(())
    })
}

#[test]
fn select_all() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        create_activity_contact_template(
            &conn,
            &uid,
            create_activity_template(&conn, uid),
            create_contact_template(&conn, uid),
        );
        create_activity_contact_template(
            &conn,
            &uid,
            create_activity_template(&conn, uid),
            create_contact_template(&conn, uid),
        );
        create_activity_contact_template(
            &conn,
            &uid,
            create_activity_template(&conn, uid),
            create_contact_template(&conn, uid),
        );
        let res = activity_contact_template::select_all(&conn, &uid)
            .expect("To fetch every activity contact for that user");
        assert_eq!(res.len(), 3, "There should be 3 records");
        Ok(())
    })
}

#[test]
fn select_contacts_for_activity_template() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_id = create_activity_template(&conn, uid);
        let contact_id = create_contact_template(&conn, uid);
        let _id = create_activity_contact_template(&conn, &uid, activity_id, contact_id);
        let res = activity_contact_template::select_contacts_for_activity_template(
            &conn,
            &uid,
            &activity_id,
        )
        .expect("To fetch every activity contacts for that activity");
        assert_eq!(res.len(), 1, "should be 1");
        assert_eq!(res[0].id, contact_id, "ids don't match");
        Ok(())
    })
}

#[test]
fn select_by_id() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_template_id = create_activity_template(&conn, uid);
        let contact_id = create_contact_template(&conn, uid);
        let _id = create_activity_contact_template(&conn, &uid, activity_template_id, contact_id);
        let res = activity_contact_template::select_by_id(
            &conn,
            &uid,
            &activity_template_id,
            &contact_id,
        )
        .expect("To fetch every activity contact for that user");
        assert_eq!(
            res.activity_template_id, activity_template_id,
            "ids don't match"
        );
        assert_eq!(res.contact_id, contact_id, "ids don't match");
        Ok(())
    })
}
#[test]
fn delete_wrong_uid() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_template_id = create_activity_template(&conn, uid);
        let contact_id = create_contact_template(&conn, uid);
        create_activity_contact_template(&conn, &uid, activity_template_id, contact_id);
        activity_contact_template::delete(
            &conn,
            &Uuid::new_v4(),
            &activity_template_id,
            &contact_id,
        )
        .expect_err("the activity contact shouldn't be deleted");
        Ok(())
    })
}

#[test]
fn delete_wrong_activity_activity_template_id() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_template_id = create_activity_template(&conn, uid);
        let contact_id = create_contact_template(&conn, uid);
        create_activity_contact_template(&conn, &uid, activity_template_id, contact_id);
        activity_contact_template::delete(&conn, &uid, &Uuid::new_v4(), &contact_id)
            .expect_err("the activity contact shouldn't be deleted");
        Ok(())
    })
}

#[test]
fn delete_wrong_contact_id() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_template_id = create_activity_template(&conn, uid);
        let contact_id = create_contact_template(&conn, uid);
        create_activity_contact_template(&conn, &uid, activity_template_id, contact_id);
        let _res =
            activity_contact_template::delete(&conn, &uid, &activity_template_id, &Uuid::new_v4())
                .expect_err("the activity contact shouldn't be deleted");
        Ok(())
    })
}

#[test]
fn delete() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_template_id = create_activity_template(&conn, uid);
        let contact_id = create_contact_template(&conn, uid);
        let _id = create_activity_contact_template(&conn, &uid, activity_template_id, contact_id);
        let res = activity_contact_template::select_by_id(
            &conn,
            &uid,
            &activity_template_id,
            &contact_id,
        )
        .expect("To fetch every activity contact for that user");
        assert_eq!(
            res.activity_template_id, activity_template_id,
            "ids don't match"
        );
        assert_eq!(res.contact_id, contact_id, "ids don't match");
        let n = activity_contact_template::delete(&conn, &uid, &activity_template_id, &contact_id)
            .expect("The delete not to fail");
        assert_eq!(n, 1, "There should be 1 modified row");
        let _res2 = activity_contact_template::select_by_id(
            &conn,
            &uid,
            &activity_template_id,
            &contact_id,
        )
        .expect_err("The record should've been deleted");
        Ok(())
    })
}

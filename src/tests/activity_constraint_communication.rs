use super::*;

use crate::activity_constraint_communication::{self, NewActivityConstraintCommunication};

use crate::communication::{self, NewCommunication, SelectCommunication};

use crate::enums::{CommunicationReasonType, CommunicationType};

use diesel::PgConnection;
use uuid::Uuid;

fn setup(conn: &PgConnection) -> (Uuid, Uuid, Uuid, Uuid) {
    let user_uuid = create_test_user(&conn);
    let activity = create_activity(&conn, &user_uuid, true, None, None);
    let constraint =
        create_activity_constraint(&conn, &user_uuid, &activity, true, None, None, None, None);
    let communication = communication::create(
        &conn,
        NewCommunication {
            uid: user_uuid,
            type_: CommunicationType::Email,
            reason: CommunicationReasonType::Check,
        },
    )
    .expect("to create the communication in the database");
    (user_uuid, activity, constraint, communication.id)
}

#[test]
fn create_activity_constraint_communication_ok() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, _activity, constraint, communication) = setup(&conn);
        activity_constraint_communication::create(
            &conn,
            &uid,
            NewActivityConstraintCommunication {
                constraint_id: constraint,
                communication_id: communication,
            },
        )
        .expect("to create the activity constraint communication in the db");
        Ok(())
    });
}

#[test]
fn create_activity_constraint_not_found_1() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, _activity, _constraint, communication) = setup(&conn);
        activity_constraint_communication::create(
            &conn,
            &uid,
            NewActivityConstraintCommunication {
                constraint_id: Uuid::new_v4(),
                communication_id: communication,
            },
        )
        .expect_err("not to create the activity constraint communication in the db");
        Ok(())
    });
}

#[test]
fn create_activity_constraint_not_found_2() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, _activity, constraint, _communication) = setup(&conn);
        activity_constraint_communication::create(
            &conn,
            &uid,
            NewActivityConstraintCommunication {
                constraint_id: constraint,
                communication_id: Uuid::new_v4(),
            },
        )
        .expect_err("not to create the activity constraint communication in the db");
        Ok(())
    });
}

#[test]
fn select_all() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, activity, constraint, communication) = setup(&conn);
        activity_constraint_communication::create(
            &conn,
            &uid,
            NewActivityConstraintCommunication {
                constraint_id: constraint,
                communication_id: communication,
            },
        )
        .expect("to create the activity constraint communication in the db");
        let mut res: Vec<SelectCommunication> =
            activity_constraint_communication::select_all(&conn, &uid, &activity, &constraint)
                .expect("to select the activity constraint from the database");
        assert_eq!(res.len(), 1, "It should be of length 1");
        let res = res.pop().unwrap();
        assert_eq!(res.id, communication, "communication id mismatch");
        Ok(())
    });
}

#[test]
fn select_all_not_found_1() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, activity, constraint, communication) = setup(&conn);
        activity_constraint_communication::create(
            &conn,
            &uid,
            NewActivityConstraintCommunication {
                constraint_id: constraint,
                communication_id: communication,
            },
        )
        .expect("to create the activity constraint communication in the db");
        let res: Vec<SelectCommunication> = activity_constraint_communication::select_all(
            &conn,
            &Uuid::new_v4(),
            &activity,
            &constraint,
        )
        .expect("to select the activity constraint from the database");
        assert_eq!(res.len(), 0, "It should be of length 0");
        Ok(())
    });
}

#[test]
fn select_all_not_found_2() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, _activity, constraint, communication) = setup(&conn);
        activity_constraint_communication::create(
            &conn,
            &uid,
            NewActivityConstraintCommunication {
                constraint_id: constraint,
                communication_id: communication,
            },
        )
        .expect("to create the activity constraint communication in the db");
        let res: Vec<SelectCommunication> = activity_constraint_communication::select_all(
            &conn,
            &uid,
            &Uuid::new_v4(),
            &constraint,
        )
        .expect("to select the activity constraint from the database");
        assert_eq!(res.len(), 0, "It should be of length 0");
        Ok(())
    });
}

#[test]
fn select_all_not_found_3() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, activity, constraint, communication) = setup(&conn);
        activity_constraint_communication::create(
            &conn,
            &uid,
            NewActivityConstraintCommunication {
                constraint_id: constraint,
                communication_id: communication,
            },
        )
        .expect("to create the activity constraint communication in the db");
        let res: Vec<SelectCommunication> =
            activity_constraint_communication::select_all(&conn, &uid, &activity, &Uuid::new_v4())
                .expect("to select the activity constraint from the database");
        assert_eq!(res.len(), 0, "It should be of length 0");
        Ok(())
    });
}

#[test]
fn select_activity_constraint_by_communication() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, _activity, constraint, communication) = setup(&conn);
        activity_constraint_communication::create(
            &conn,
            &uid,
            NewActivityConstraintCommunication {
                constraint_id: constraint,
                communication_id: communication,
            },
        )
        .expect("to create the activity constraint communication in the db");
        let res: crate::activity_constraint::SelectActivityConstraint =
            activity_constraint_communication::select_constraint_by_communication(
                &conn,
                &uid,
                &communication,
            )
            .expect("to select the activity constraint from the database");
        assert_eq!(res.id, constraint, "constraint id mismatch");
        Ok(())
    });
}

#[test]
fn select_activity_constraint_by_communication_not_found() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, activity, constraint, communication) = setup(&conn);
        activity_constraint_communication::create(
            &conn,
            &uid,
            NewActivityConstraintCommunication {
                constraint_id: constraint,
                communication_id: communication,
            },
        )
        .expect("to create the activity constraint communication in the db");
        activity_constraint_communication::select_constraint_by_communication(
            &conn, &uid, &activity,
        )
        .expect_err("not to have select the activity constraint from the database");
        Ok(())
    });
}

use super::*;
use crate::activity_communication;
use crate::activity_constraint_communication;
use crate::communication::{self, NewCommunication};
use crate::communication_recipient;
use crate::enums::{CommunicationReasonType, CommunicationType};
use uuid::Uuid;

#[test]
fn create_communication_ok() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let uid = create_test_user(&conn);
        communication::create(
            &conn,
            NewCommunication {
                uid,
                type_: CommunicationType::Email,
                reason: CommunicationReasonType::Check,
            },
        )
        .expect("to create the communication in the database");
        Ok(())
    })
}

#[test]
fn create_communication_dup() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let uid = create_test_user(&conn);
        communication::create(
            &conn,
            NewCommunication {
                uid,
                type_: CommunicationType::Email,
                reason: CommunicationReasonType::Check,
            },
        )
        .expect("to create the communication in the database");
        communication::create(
            &conn,
            NewCommunication {
                uid,
                type_: CommunicationType::Email,
                reason: CommunicationReasonType::Check,
            },
        )
        .expect("to create the communication in the database");
        Ok(())
    })
}

#[test]
fn create_communication_from_constraint() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let uid = create_test_user(&conn);
        let activity = create_activity(&conn, &uid, true, None, None);
        let contact = create_contact(&conn, &uid);
        let activity_constraint =
            create_activity_constraint(&conn, &uid, &activity, true, None, None, None, None);
        let _activity_contact = create_activity_contact(&conn, &uid, &activity, &contact);
        let communication = communication::create_from_constraint(
            &conn,
            &activity_constraint,
            NewCommunication {
                uid,
                type_: CommunicationType::Email,
                reason: CommunicationReasonType::Check,
            },
        )
        .expect("to create the communication in the database");
        let mut recipients =
            communication_recipient::select_by_communication_id(&conn, &uid, &communication.id)
                .unwrap();
        assert_eq!(recipients.len(), 1, "Should have 1 recipients");
        let recipient = recipients.pop().unwrap();
        assert_eq!(
            recipient.communication_id, communication.id,
            "Communication id mismatch"
        );
        assert_eq!(recipient.contact_id, contact, "Contact id mismatch");
        let _activity_constraint_communication = activity_constraint_communication::select_by_id(
            &conn,
            &uid,
            &activity_constraint,
            &communication.id,
        )
        .expect("There should be an activity constraint communication record");
        Ok(())
    })
}

#[test]
fn create_communication_from_activity() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let uid = create_test_user(&conn);
        let activity = create_activity(&conn, &uid, true, None, None);
        let contact = create_contact(&conn, &uid);
        let _activity_contact = create_activity_contact(&conn, &uid, &activity, &contact);
        let communication = communication::create_from_activity(
            &conn,
            &activity,
            NewCommunication {
                uid,
                type_: CommunicationType::Email,
                reason: CommunicationReasonType::Check,
            },
        )
        .expect("to create the communication in the database");
        let mut recipients =
            communication_recipient::select_by_communication_id(&conn, &uid, &communication.id)
                .unwrap();
        assert_eq!(recipients.len(), 1, "Should have 1 recipients");
        let recipient = recipients.pop().unwrap();
        assert_eq!(
            recipient.communication_id, communication.id,
            "Communication id mismatch"
        );
        assert_eq!(recipient.contact_id, contact, "Contact id mismatch");
        let _activity_communication =
            activity_communication::select_by_id(&conn, &uid, &activity, &communication.id)
                .expect("There should be an activity constraint communication record");
        Ok(())
    })
}

#[test]
fn select_by_id_unlogged() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let uid = create_test_user(&conn);
        let exp = communication::create(
            &conn,
            NewCommunication {
                uid,
                type_: CommunicationType::Email,
                reason: CommunicationReasonType::Check,
            },
        )
        .expect("to create the communication in the database");
        let _comm = communication::select_by_id_unlogged(&conn, &exp.id)
            .expect("should have fetched communcation from db");
        Ok(())
    })
}

#[test]
fn select_by_id() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let uid = create_test_user(&conn);
        let exp = communication::create(
            &conn,
            NewCommunication {
                uid,
                type_: CommunicationType::Email,
                reason: CommunicationReasonType::Check,
            },
        )
        .expect("to create the communication in the database");
        let _comm = communication::select_by_id(&conn, &uid, &exp.id)
            .expect("should have fetched communcation from db");
        Ok(())
    })
}

#[test]
fn select_by_id_not_found_1() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let uid = create_test_user(&conn);
        let _exp = communication::create(
            &conn,
            NewCommunication {
                uid,
                type_: CommunicationType::Email,
                reason: CommunicationReasonType::Check,
            },
        )
        .expect("to create the communication in the database");
        let _comm = communication::select_by_id(&conn, &uid, &Uuid::new_v4())
            .expect_err("shouldn't be found");
        Ok(())
    })
}

#[test]
fn select_by_id_not_found_2() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let uid = create_test_user(&conn);
        let exp = communication::create(
            &conn,
            NewCommunication {
                uid,
                type_: CommunicationType::Email,
                reason: CommunicationReasonType::Check,
            },
        )
        .expect("to create the communication in the database");
        let _comm = communication::select_by_id(&conn, &Uuid::new_v4(), &exp.id)
            .expect_err("shouldn't be found");
        Ok(())
    })
}

#[test]
fn delete_ok() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let uid = create_test_user(&conn);
        let exp = communication::create(
            &conn,
            NewCommunication {
                uid,
                type_: CommunicationType::Email,
                reason: CommunicationReasonType::Check,
            },
        )
        .expect("to create the communication in the database");
        let comm =
            communication::delete(&conn, &uid, &exp.id).expect("should have been deleted from db");
        assert_eq!(comm, 1, "1 Should've been deleted");
        Ok(())
    })
}

#[test]
fn delete_not_found_1() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let uid = create_test_user(&conn);
        let exp = communication::create(
            &conn,
            NewCommunication {
                uid,
                type_: CommunicationType::Email,
                reason: CommunicationReasonType::Check,
            },
        )
        .expect("to create the communication in the database");
        let comm = communication::delete(&conn, &Uuid::new_v4(), &exp.id)
            .expect("should have been deleted from db");
        assert_eq!(comm, 0, "0 Should've been deleted");
        Ok(())
    })
}

#[test]
fn delete_not_found_2() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let uid = create_test_user(&conn);
        let _exp = communication::create(
            &conn,
            NewCommunication {
                uid,
                type_: CommunicationType::Email,
                reason: CommunicationReasonType::Check,
            },
        )
        .expect("to create the communication in the database");
        let comm = communication::delete(&conn, &uid, &Uuid::new_v4())
            .expect("should have been deleted from db");
        assert_eq!(comm, 0, "0 Should've been deleted");
        Ok(())
    })
}

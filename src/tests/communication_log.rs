use super::*;
use crate::communication::{self, NewCommunication};
use crate::communication_log::{self, NewCommunicationLog};
use crate::communication_recipient::{self, NewCommunicationRecipient};

use crate::enums::{CommunicationReasonType, CommunicationStatus, CommunicationType};
use diesel::PgConnection;

use uuid::Uuid;

fn setup_communication(conn: &PgConnection) -> (Uuid, Uuid, Uuid) {
    let uid = create_test_user(&conn);
    let contact_id = create_contact(&conn, &uid);
    let communication_id = communication::create(
        &conn,
        NewCommunication {
            uid,
            type_: CommunicationType::Email,
            reason: CommunicationReasonType::Check,
        },
    )
    .expect("to create the communication in the database");
    let _communication_recipient_id = communication_recipient::create(
        &conn,
        &uid,
        NewCommunicationRecipient {
            communication_id: communication_id.id,
            contact_id,
        },
    )
    .expect("to create the communication recipient in the database");
    (uid, communication_id.id, contact_id)
}

#[test]
fn create_ok() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (_uid, communication_id, contact_id) = setup_communication(&conn);
        let res = communication_log::create(
            &conn,
            NewCommunicationLog {
                communication_id,
                contact_id,
                status: CommunicationStatus::Sent,
            },
        )
        .expect("to have created the communication log in the database");
        assert_eq!(res.communication_id, communication_id, "id mismatch");
        assert_eq!(res.contact_id, contact_id, "id mismatch");
        assert_eq!(res.status, CommunicationStatus::Sent, "status mismatch");
        Ok(())
    })
}

#[test]
fn create_multi_ok() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (_uid, communication_id, contact_id) = setup_communication(&conn);
        let res = communication_log::create_multi(
            &conn,
            vec![NewCommunicationLog {
                communication_id,
                contact_id,
                status: CommunicationStatus::Sent,
            }],
        )
        .expect("to have created the communication log in the database");
        assert_eq!(res[0].communication_id, communication_id, "id mismatch");
        assert_eq!(res[0].contact_id, contact_id, "id mismatch");
        assert_eq!(res[0].status, CommunicationStatus::Sent, "status mismatch");
        Ok(())
    })
}

#[test]
fn create_not_found_1() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (_uid, _communication_id, contact_id) = setup_communication(&conn);
        let _res = communication_log::create(
            &conn,
            NewCommunicationLog {
                communication_id: Uuid::new_v4(),
                contact_id,
                status: CommunicationStatus::Sent,
            },
        )
        .expect_err("shouldn't have found the communication id");
        Ok(())
    })
}

#[test]
fn create_not_found_2() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (_uid, communication_id, _contact_id) = setup_communication(&conn);
        let _res = communication_log::create(
            &conn,
            NewCommunicationLog {
                communication_id,
                contact_id: Uuid::new_v4(),
                status: CommunicationStatus::Sent,
            },
        )
        .expect_err("shouldn't have found the communication id");
        Ok(())
    })
}

#[test]
fn select_by_id() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, communication_id, contact_id) = setup_communication(&conn);
        let exp = communication_log::create(
            &conn,
            NewCommunicationLog {
                communication_id,
                contact_id,
                status: CommunicationStatus::Retrying,
            },
        )
        .expect("should have found the communication log");
        let res = communication_log::select_by_id(&conn, &uid, &exp.id)
            .expect("to have found the communication log");
        assert_eq!(res, exp, "objects mismatch");
        Ok(())
    })
}

#[test]
fn select_by_id_not_found_1() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (_uid, communication_id, contact_id) = setup_communication(&conn);
        let exp = communication_log::create(
            &conn,
            NewCommunicationLog {
                communication_id,
                contact_id,
                status: CommunicationStatus::Retrying,
            },
        )
        .expect("should have found the communication log");
        let _res = communication_log::select_by_id(&conn, &Uuid::new_v4(), &exp.id)
            .expect_err("shouldn't have found object");
        Ok(())
    })
}

#[test]
fn select_by_id_not_found_2() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, communication_id, contact_id) = setup_communication(&conn);
        let _exp = communication_log::create(
            &conn,
            NewCommunicationLog {
                communication_id,
                contact_id,
                status: CommunicationStatus::Retrying,
            },
        )
        .expect("should have found the communication log");
        let _res = communication_log::select_by_id(&conn, &uid, &Uuid::new_v4())
            .expect_err("shouldn't have found object");
        Ok(())
    })
}

#[test]
fn select_by_contacts_unlogged_ok() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, communication_id, contact_id) = setup_communication(&conn);
        let contact = crate::contact::select_by_id(&conn, &uid, &contact_id).unwrap();
        let exp = communication_log::create(
            &conn,
            NewCommunicationLog {
                communication_id,
                contact_id,
                status: CommunicationStatus::Retrying,
            },
        )
        .expect("should have found the communication log");
        let mut res = communication_log::select_by_contacts_unlogged(
            &conn,
            &communication_id,
            vec![contact.clone()],
        )
        .expect("to have found the communication log");
        assert_eq!(res.len(), 1, "Should be 1");
        let mut res = res.pop().unwrap();
        assert_eq!(res.0, contact, "objects mismatch");
        assert_eq!(res.1.len(), 1, "Should be 1");
        let res = res.1.pop().unwrap();
        assert_eq!(exp, res, "objects mismatch");
        Ok(())
    })
}

#[test]
fn select_by_contacts_unlogged_no_logs() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, communication_id, contact_id) = setup_communication(&conn);
        let contact = crate::contact::select_by_id(&conn, &uid, &contact_id).unwrap();
        let mut res = communication_log::select_by_contacts_unlogged(
            &conn,
            &communication_id,
            vec![contact.clone()],
        )
        .expect("to have found the communication log");
        assert_eq!(res.len(), 1, "Should be 1");
        let res = res.pop().unwrap();
        assert_eq!(res.0, contact, "objects mismatch");
        assert_eq!(res.1.len(), 0, "Should be 0");
        Ok(())
    })
}

#[test]
fn select_by_contacts_unlogged_not_found() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, communication_id, _contact_id) = setup_communication(&conn);
        let contact_2 = create_contact(&conn, &uid);
        let contact = crate::contact::select_by_id(&conn, &uid, &contact_2).unwrap();
        let mut res = communication_log::select_by_contacts_unlogged(
            &conn,
            &communication_id,
            vec![contact.clone()],
        )
        .expect("to have found the communication log");
        assert_eq!(res.len(), 1, "Should be 1");
        let res = res.pop().unwrap();
        assert_eq!(res.0, contact, "objects mismatch");
        assert_eq!(res.1.len(), 0, "Should be 0");
        Ok(())
    })
}

#[test]
fn select_by_contacts_unlogged_empty() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (_uid, communication_id, _contact_id) = setup_communication(&conn);
        let res = communication_log::select_by_contacts_unlogged(&conn, &communication_id, vec![])
            .expect("to have found the communication log");
        assert_eq!(res.len(), 0, "Should be 1");
        Ok(())
    })
}

#[test]
fn select_by_recipient() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, communication_id, contact_id) = setup_communication(&conn);
        let exp = communication_log::create(
            &conn,
            NewCommunicationLog {
                communication_id,
                contact_id,
                status: CommunicationStatus::Retrying,
            },
        )
        .expect("should have found the communication log");
        let mut res =
            communication_log::select_by_recipient(&conn, &uid, &communication_id, &contact_id)
                .expect("to have found the communication log");
        assert_eq!(res.len(), 1, "Should be 1");
        let res = res.pop().unwrap();
        assert_eq!(res, exp, "objects mismatch");
        Ok(())
    })
}

#[test]
fn select_by_recipient_not_found_1() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (_uid, communication_id, contact_id) = setup_communication(&conn);
        let _exp = communication_log::create(
            &conn,
            NewCommunicationLog {
                communication_id,
                contact_id,
                status: CommunicationStatus::Retrying,
            },
        )
        .expect("should have found the communication log");
        let res = communication_log::select_by_recipient(
            &conn,
            &Uuid::new_v4(),
            &communication_id,
            &contact_id,
        )
        .expect("to have found the communication log");
        assert_eq!(res.len(), 0, "Should be 0");
        Ok(())
    })
}

#[test]
fn select_by_recipient_not_found_2() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, communication_id, contact_id) = setup_communication(&conn);
        let _exp = communication_log::create(
            &conn,
            NewCommunicationLog {
                communication_id,
                contact_id,
                status: CommunicationStatus::Retrying,
            },
        )
        .expect("should have found the communication log");
        let res = communication_log::select_by_recipient(&conn, &uid, &Uuid::new_v4(), &contact_id)
            .expect("to have found the communication log");
        assert_eq!(res.len(), 0, "Should be 0");
        Ok(())
    })
}

#[test]
fn select_by_recipient_not_found_3() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, communication_id, contact_id) = setup_communication(&conn);
        let _exp = communication_log::create(
            &conn,
            NewCommunicationLog {
                communication_id,
                contact_id,
                status: CommunicationStatus::Retrying,
            },
        )
        .expect("should have found the communication log");
        let res =
            communication_log::select_by_recipient(&conn, &uid, &communication_id, &Uuid::new_v4())
                .expect("to have found the communication log");
        assert_eq!(res.len(), 0, "Should be 0");
        Ok(())
    })
}

#[test]
fn delete() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, communication_id, contact_id) = setup_communication(&conn);
        let exp = communication_log::create(
            &conn,
            NewCommunicationLog {
                communication_id,
                contact_id,
                status: CommunicationStatus::Retrying,
            },
        )
        .expect("should have found the communication log");
        let res = communication_log::delete(&conn, &uid, &exp.id)
            .expect("to have found the communication log");
        assert_eq!(res, 1, "should have deleted 1");
        Ok(())
    })
}

#[test]
fn delete_not_found_1() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (_uid, communication_id, contact_id) = setup_communication(&conn);
        let exp = communication_log::create(
            &conn,
            NewCommunicationLog {
                communication_id,
                contact_id,
                status: CommunicationStatus::Retrying,
            },
        )
        .expect("should have found the communication log");
        let res = communication_log::delete(&conn, &Uuid::new_v4(), &exp.id)
            .expect("to have found the communication log");
        assert_eq!(res, 0, "should have deleted 0");
        Ok(())
    })
}

#[test]
fn delete_not_found_2() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, communication_id, contact_id) = setup_communication(&conn);
        let _exp = communication_log::create(
            &conn,
            NewCommunicationLog {
                communication_id,
                contact_id,
                status: CommunicationStatus::Retrying,
            },
        )
        .expect("should have found the communication log");
        let res = communication_log::delete(&conn, &uid, &Uuid::new_v4())
            .expect("to have found the communication log");
        assert_eq!(res, 0, "should have deleted 0");
        Ok(())
    })
}

use super::*;
use crate::communication::{self, NewCommunication};
use crate::communication_recipient::{self, NewCommunicationRecipient};
use crate::enums::{CommunicationReasonType, CommunicationType};
use diesel::PgConnection;

use uuid::Uuid;

fn setup_communication(conn: &PgConnection) -> (Uuid, Uuid, Uuid) {
    let uid = create_test_user(&conn);
    let contact = create_contact(&conn, &uid);
    let communication_id = communication::create(
        &conn,
        NewCommunication {
            uid,
            type_: CommunicationType::Email,
            reason: CommunicationReasonType::Check,
        },
    )
    .expect("to create the communication in the database");

    (uid, contact, communication_id.id)
}

#[test]
fn create_ok() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, contact_id, communication_id) = setup_communication(&conn);
        let res = communication_recipient::create(
            &conn,
            &uid,
            NewCommunicationRecipient {
                communication_id,
                contact_id,
            },
        )
        .expect("to have created the communication recipient in the database");
        assert_eq!(
            res.communication_id, communication_id,
            "communication id mismatch"
        );
        assert_eq!(res.contact_id, contact_id, "contacty id mismatch");
        Ok(())
    })
}

#[test]
fn create_not_found_1() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, contact_id, _communication_id) = setup_communication(&conn);
        let _res = communication_recipient::create(
            &conn,
            &uid,
            NewCommunicationRecipient {
                communication_id: Uuid::new_v4(),
                contact_id,
            },
        )
        .expect_err("fail to create communication recipient");
        Ok(())
    })
}

#[test]
fn create_not_found_2() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, _contact_id, communication_id) = setup_communication(&conn);
        let _res = communication_recipient::create(
            &conn,
            &uid,
            NewCommunicationRecipient {
                communication_id,
                contact_id: Uuid::new_v4(),
            },
        )
        .expect_err("fail to create communication recipient");
        Ok(())
    })
}

#[test]
fn create_from_constraint_ok() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, contact_id, communication_id) = setup_communication(&conn);
        let activity = create_activity(&conn, &uid, true, None, None);
        let activity_constraint =
            create_activity_constraint(&conn, &uid, &activity, true, None, None, None, None);
        let _activity_contact = create_activity_contact(&conn, &uid, &activity, &contact_id);
        let mut res = communication_recipient::create_from_activity_constraint(
            &conn,
            &uid,
            &communication_id,
            &activity_constraint,
        )
        .expect("to have created the communication recipient in the database");
        assert_eq!(res.len(), 1, "Should have created 1 object");
        let res = res.pop().unwrap();
        assert_eq!(
            res.communication_id, communication_id,
            "communication id mismatch"
        );
        assert_eq!(res.contact_id, contact_id, "contact id mismatch");
        Ok(())
    })
}

#[test]
fn create_from_activity_ok() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, contact_id, communication_id) = setup_communication(&conn);
        let activity = create_activity(&conn, &uid, true, None, None);
        let _activity_contact = create_activity_contact(&conn, &uid, &activity, &contact_id);
        let mut res = communication_recipient::create_from_activity(
            &conn,
            &uid,
            &communication_id,
            &activity,
        )
        .expect("to have created the communication recipient in the database");
        assert_eq!(res.len(), 1, "Should have created 1 object");
        let res = res.pop().unwrap();
        assert_eq!(
            res.communication_id, communication_id,
            "communication id mismatch"
        );
        assert_eq!(res.contact_id, contact_id, "contact id mismatch");
        Ok(())
    })
}

#[test]
fn select_by_communication_id() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, contact_id, communication_id) = setup_communication(&conn);
        let recipient = communication_recipient::create(
            &conn,
            &uid,
            NewCommunicationRecipient {
                communication_id,
                contact_id,
            },
        )
        .expect("should create the communication recipient");
        let mut res =
            communication_recipient::select_by_communication_id(&conn, &uid, &communication_id)
                .expect("to have fetch the communication recipient");
        assert_eq!(res.len(), 1, "Should have fetched a recipient");
        assert_eq!(recipient, res.pop().unwrap(), "obj mismatch");
        Ok(())
    })
}

#[test]
fn select_by_communication_id_with_contacts() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, contact_id, communication_id) = setup_communication(&conn);
        let recipient = communication_recipient::create(
            &conn,
            &uid,
            NewCommunicationRecipient {
                communication_id,
                contact_id,
            },
        )
        .expect("should create the communication recipient");
        let mut res = communication_recipient::select_by_communication_id_with_contacts(
            &conn,
            &uid,
            &communication_id,
        )
        .expect("to have fetch the communication recipient");
        assert_eq!(res.len(), 1, "Should have fetched a recipient");
        assert_eq!(recipient.contact_id, res.pop().unwrap().id, "obj mismatch");
        Ok(())
    })
}

#[test]
fn select_by_communication_id_not_found_1() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, contact_id, communication_id) = setup_communication(&conn);
        let _recipient = communication_recipient::create(
            &conn,
            &uid,
            NewCommunicationRecipient {
                communication_id,
                contact_id,
            },
        )
        .expect("should create the communication recipient");
        let res = communication_recipient::select_by_communication_id(
            &conn,
            &Uuid::new_v4(),
            &communication_id,
        )
        .expect("to have fetch the communication recipient");
        assert_eq!(res.len(), 0, "Shouldn't have fetched a recipient");
        Ok(())
    })
}

#[test]
fn select_by_communication_id_not_found_2() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, contact_id, communication_id) = setup_communication(&conn);
        let _recipient = communication_recipient::create(
            &conn,
            &uid,
            NewCommunicationRecipient {
                communication_id,
                contact_id,
            },
        )
        .expect("should create the communication recipient");
        let res = communication_recipient::select_by_communication_id(&conn, &uid, &Uuid::new_v4())
            .expect("to have fetch the communication recipient");
        assert_eq!(res.len(), 0, "Shouldn't have fetched a recipient");
        Ok(())
    })
}

#[test]
fn delete_ok() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, contact_id, communication_id) = setup_communication(&conn);
        let recipient = communication_recipient::create(
            &conn,
            &uid,
            NewCommunicationRecipient {
                communication_id,
                contact_id,
            },
        )
        .expect("should create the communication recipient");
        let res = communication_recipient::delete(
            &conn,
            &uid,
            &recipient.communication_id,
            &recipient.contact_id,
        )
        .expect("should've been deleted");
        assert_eq!(res, 1, "should have been deleted");
        Ok(())
    })
}

#[test]
fn delete_not_found_1() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, contact_id, communication_id) = setup_communication(&conn);
        let recipient = communication_recipient::create(
            &conn,
            &uid,
            NewCommunicationRecipient {
                communication_id,
                contact_id,
            },
        )
        .expect("should create the communication recipient");
        let res = communication_recipient::delete(
            &conn,
            &Uuid::new_v4(),
            &recipient.communication_id,
            &recipient.contact_id,
        )
        .expect("can't delete because not found");
        assert_eq!(res, 0, "shouldn't have been deleted");
        Ok(())
    })
}

#[test]
fn delete_not_found_2() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, contact_id, communication_id) = setup_communication(&conn);
        let recipient = communication_recipient::create(
            &conn,
            &uid,
            NewCommunicationRecipient {
                communication_id,
                contact_id,
            },
        )
        .expect("should create the communication recipient");
        let res =
            communication_recipient::delete(&conn, &uid, &Uuid::new_v4(), &recipient.contact_id)
                .expect("can't delete because not found");
        assert_eq!(res, 0, "shouldn't have been deleted");
        Ok(())
    })
}

#[test]
fn delete_not_found_3() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, contact_id, communication_id) = setup_communication(&conn);
        let recipient = communication_recipient::create(
            &conn,
            &uid,
            NewCommunicationRecipient {
                communication_id,
                contact_id,
            },
        )
        .expect("should create the communication recipient");
        let res = communication_recipient::delete(
            &conn,
            &uid,
            &recipient.communication_id,
            &Uuid::new_v4(),
        )
        .expect("can't delete because not found");
        assert_eq!(res, 0, "shouldn't have been deleted");
        Ok(())
    })
}

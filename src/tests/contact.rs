use super::*;
use crate::contact::{self, NewContact, SelectContact};
use crate::enums::ContactType;
use crate::error::IqiperDatabaseError;
use diesel::PgConnection;
use std::convert::From;
use uuid::Uuid;

use super::{create_test_user, delete_test_user, establish_connection};

pub fn create_some_contact(
    conn: &PgConnection,
    user_1: Uuid,
    user_2: Uuid,
    user_3: Uuid,
) -> Vec<Result<SelectContact, IqiperDatabaseError>> {
    let contact_1 = NewContact {
        nickname: String::from("flenouille"),
        favorite: true,
        uid: user_1,
        type_: ContactType::Email,
        email: Some(String::from("flenouille@notmail.ch")),
        target_uid: None,
    };
    let contact_2 = NewContact {
        nickname: String::from("flenouille"),
        favorite: true,
        uid: user_1,
        type_: ContactType::Friend,
        email: None,
        target_uid: Some(user_2),
    };
    let contact_3 = NewContact {
        nickname: String::from("flenouille"),
        favorite: true,
        uid: user_3,
        type_: ContactType::Email,
        email: Some(String::from("flenouille@notmail.ch")),
        target_uid: None,
    };
    let mut created = Vec::new();
    created.push(contact::create(conn, contact_1));
    created.push(contact::create(conn, contact_2));
    created.push(contact::create(conn, contact_3));
    return created;
}

#[test]
fn create_email() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let user_uuid = create_test_user(&conn);
        let expected = NewContact {
            nickname: String::from("flenouille"),
            favorite: true,
            uid: user_uuid,
            type_: ContactType::Email,
            email: Some(String::from("flenouille@notmail.ch")),
            target_uid: None,
        };
        let created_contact = contact::create(&conn, expected.clone());
        delete_test_user(&conn, &user_uuid);
        let created =
            created_contact.expect("contact mail creation should succeed with a valid uid");
        assert_eq!(
            expected.nickname, created.nickname,
            "nicknames do not match"
        );
        assert_eq!(
            expected.favorite, created.favorite,
            "favorites do not match"
        );
        assert_eq!(expected.uid, created.uid, "uids do not match");
        assert_eq!(expected.email, created.email, "emails do not match");
        assert_eq!(
            expected.target_uid, created.target_uid,
            "target_uids do not match"
        );
        Ok(())
    })
}

#[test]
fn create_email_missing_user() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let user_uuid = Uuid::new_v4();
        let expected = NewContact {
            nickname: String::from("flenouille"),
            favorite: true,
            uid: user_uuid,
            type_: ContactType::Email,
            email: Some(String::from("flenouille@notmail.ch")),
            target_uid: None,
        };
        let created_contact = contact::create(&conn, expected.clone());
        assert!(
            created_contact.is_err(),
            "contact mail creation should fail with an invalid uid"
        );
        Ok(())
    })
}

#[test]
fn create_friend() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let user_uuid = create_test_user(&conn);
        let target_uuid = create_test_user(&conn);
        let expected = NewContact {
            nickname: String::from("flenouille"),
            favorite: true,
            uid: user_uuid,
            type_: ContactType::Friend,
            email: None,
            target_uid: Some(target_uuid),
        };
        let created_contact = contact::create(&conn, expected.clone());
        delete_test_user(&conn, &user_uuid);
        delete_test_user(&conn, &target_uuid);
        let created =
            created_contact.expect("contact friend creation should succeed with a valid uid");
        assert_eq!(
            expected.nickname, created.nickname,
            "nicknames do not match"
        );
        assert_eq!(
            expected.favorite, created.favorite,
            "favorites do not match"
        );
        assert_eq!(expected.uid, created.uid, "uids do not match");
        assert_eq!(expected.email, created.email, "emails do not match");
        assert_eq!(
            expected.target_uid, created.target_uid,
            "target_uids do not match"
        );
        Ok(())
    })
}

#[test]
fn create_friend_missing_target() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let user_uuid = create_test_user(&conn);
        let target_uuid = Uuid::new_v4();
        let expected = NewContact {
            nickname: String::from("flenouille"),
            favorite: true,
            uid: user_uuid,
            type_: ContactType::Friend,
            email: None,
            target_uid: Some(target_uuid),
        };
        let created_contact = contact::create(&conn, expected.clone());
        created_contact.expect_err("contact friend creation should fail with an invali target_id");
        Ok(())
    })
}

#[test]
fn get_all() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let user_1 = create_test_user(&conn);
        let user_2 = create_test_user(&conn);
        let user_3 = create_test_user(&conn);
        let created = create_some_contact(&conn, user_1, user_2, user_3);
        let selected = contact::select_all(&conn, &user_1);
        delete_test_user(&conn, &user_1);
        delete_test_user(&conn, &user_2);
        delete_test_user(&conn, &user_3);
        let created: Vec<SelectContact> = created
            .into_iter()
            .map(|res| res.expect("Failed to create a contact"))
            .collect();
        let selected = selected.expect("failed to select contact");
        assert!(
            selected.len() == 2,
            "Selection did not returned the correct amount of contact"
        );
        for cont in &created[0..1] {
            assert!(selected.contains(cont), "Selection missed a contact");
        }
        Ok(())
    })
}

#[test]
fn get_all_empty() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let user_1 = create_test_user(&conn);
        let user_2 = create_test_user(&conn);
        let user_3 = create_test_user(&conn);
        let created = create_some_contact(&conn, user_1, user_2, user_3);
        let selected = contact::select_all(&conn, &user_2);
        delete_test_user(&conn, &user_1);
        delete_test_user(&conn, &user_2);
        delete_test_user(&conn, &user_3);
        for res in created {
            assert!(res.is_ok(), "Failed to create a contact");
        }
        let selected = selected.expect("failed to select contact");
        assert!(
            selected.len() == 0,
            "Selection did not returned the correct amount of contact"
        );
        Ok(())
    })
}

#[test]
fn get_all_id() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let user_1 = create_test_user(&conn);
        let user_2 = create_test_user(&conn);
        let user_3 = create_test_user(&conn);
        let created = create_some_contact(&conn, user_1, user_2, user_3);
        let selected = contact::select_all_id(&conn, &user_1);
        delete_test_user(&conn, &user_1);
        delete_test_user(&conn, &user_2);
        delete_test_user(&conn, &user_3);
        let created: Vec<SelectContact> = created
            .into_iter()
            .map(|res| res.expect("Failed to create a contact"))
            .collect();
        let selected = selected.expect("failed to select contact");
        assert!(
            selected.len() == 2,
            "Selection did not returned the correct amount of contact"
        );
        eprintln!("{:#?}", created);
        eprintln!("{:#?}", selected);
        for cont in &created[0..1] {
            assert!(selected.contains(&cont.id), "Selection missed a contact");
        }
        Ok(())
    })
}

#[test]
fn get_all_id_empty() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let user_1 = create_test_user(&conn);
        let user_2 = create_test_user(&conn);
        let user_3 = create_test_user(&conn);
        let created = create_some_contact(&conn, user_1, user_2, user_3);
        let selected = contact::select_all_id(&conn, &user_2);
        delete_test_user(&conn, &user_1);
        delete_test_user(&conn, &user_2);
        delete_test_user(&conn, &user_3);
        for res in created {
            assert!(res.is_ok(), "Failed to create a contact");
        }
        let selected = selected.expect("failed to select contact");
        assert!(
            selected.len() == 0,
            "Selection did not returned the correct amount of contact"
        );
        Ok(())
    })
}

#[test]
fn get_by_id() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let user_1 = create_test_user(&conn);
        let user_2 = create_test_user(&conn);
        let user_3 = create_test_user(&conn);
        let created = create_some_contact(&conn, user_1, user_2, user_3);
        let selected = if let Ok(created_0) = &created[0] {
            Some(contact::select_by_id(&conn, &user_1, &created_0.id))
        } else {
            None
        };
        delete_test_user(&conn, &user_1);
        delete_test_user(&conn, &user_2);
        delete_test_user(&conn, &user_3);
        let created: Vec<SelectContact> = created
            .into_iter()
            .map(|res| res.expect("Failed to create a contact"))
            .collect();
        let selected = selected.unwrap().expect("failjed to select contact");
        assert_eq!(
            created[0].id, selected.id,
            "Selection did not returned the correct contact"
        );
        Ok(())
    })
}

#[test]
fn get_by_id_empty() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let user_1 = create_test_user(&conn);
        let contact_id = Uuid::new_v4();
        let selected = contact::select_by_id(&conn, &user_1, &contact_id);
        delete_test_user(&conn, &user_1);
        let _selected = selected.expect_err("Shouldn't have selected this contact");
        Ok(())
    })
}

#[test]
fn get_by_id_not_owned() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let user_1 = create_test_user(&conn);
        let user_2 = create_test_user(&conn);
        let user_3 = create_test_user(&conn);
        let created = create_some_contact(&conn, user_1, user_2, user_3);
        let selected = if let Ok(created_0) = &created[0] {
            Some(contact::select_by_id(&conn, &user_2, &created_0.id))
        } else {
            None
        };
        delete_test_user(&conn, &user_1);
        delete_test_user(&conn, &user_2);
        delete_test_user(&conn, &user_3);
        for res in created {
            assert!(res.is_ok(), "Failed to create a contact");
        }
        let _selected = selected
            .unwrap()
            .expect_err("Should'nt have selected this contact");
        Ok(())
    })
}

#[test]
fn update_nickname_existing() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let user_1 = create_test_user(&conn);
        let user_2 = create_test_user(&conn);
        let user_3 = create_test_user(&conn);
        let created = create_some_contact(&conn, user_1, user_2, user_3);
        let update_val = String::from("petit-chaman");
        let mut res = None;
        let selected = if let Ok(created_0) = &created[0] {
            res = Some(contact::update_nickname(
                &conn,
                &user_1,
                &created_0.id,
                update_val.clone(),
            ));
            Some(contact::select_by_id(&conn, &user_1, &created_0.id))
        } else {
            None
        };
        delete_test_user(&conn, &user_1);
        delete_test_user(&conn, &user_2);
        delete_test_user(&conn, &user_3);
        for res in &created {
            assert!(res.is_ok(), "Failed to create a contact");
        }
        let created = created[0].as_ref().unwrap();
        let selected = selected.unwrap().expect("failed to select contact");
        assert_eq!(update_val, selected.nickname, "nicknames do not match");
        assert_eq!(created.uid, selected.uid, "uids do not match");
        assert_eq!(created.email, selected.email, "emails do not match");
        assert_eq!(
            created.target_uid, selected.target_uid,
            "target_uids do not match"
        );
        assert!(
            res.unwrap().expect("Deletion returned an error").0 == created.id,
            "Update did not returned the correct id"
        );
        Ok(())
    })
}

#[test]
fn update_nickname_existing_not_owned() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let user_1 = create_test_user(&conn);
        let user_2 = create_test_user(&conn);
        let user_3 = create_test_user(&conn);
        let created = create_some_contact(&conn, user_1, user_2, user_3);
        let update_val = String::from("petit-chaman");
        let mut res = None;
        let selected = if let Ok(created_0) = &created[0] {
            res = Some(contact::update_nickname(
                &conn,
                &user_2,
                &created_0.id,
                update_val.clone(),
            ));
            Some(contact::select_by_id(&conn, &user_1, &created_0.id))
        } else {
            None
        };
        delete_test_user(&conn, &user_1);
        delete_test_user(&conn, &user_2);
        delete_test_user(&conn, &user_3);
        for res in &created {
            assert!(res.is_ok(), "Failed to create a contact");
        }
        let created = created[0].as_ref().unwrap();
        let selected = selected.unwrap().expect("failed to select contact");
        assert_eq!(
            created.nickname, selected.nickname,
            "nicknames do not match"
        );
        assert_eq!(
            created.favorite, selected.favorite,
            "favorites do not match"
        );
        assert_eq!(created.uid, selected.uid, "uids do not match");
        assert_eq!(created.email, selected.email, "emails do not match");
        assert_eq!(
            created.target_uid, selected.target_uid,
            "target_uids do not match"
        );
        assert!(res.unwrap().is_err(), "Update should return an error");
        Ok(())
    })
}

#[test]
fn update_nickname_not_existing() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let user_1 = create_test_user(&conn);
        let contact_id = Uuid::new_v4();
        let update_val = String::from("petit-chaman");
        let res = contact::update_nickname(&conn, &user_1, &contact_id, update_val.clone());
        delete_test_user(&conn, &user_1);
        assert!(res.is_err(), "Update should return an error");
        Ok(())
    })
}

#[test]
fn update_favorite_existing() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let user_1 = create_test_user(&conn);
        let user_2 = create_test_user(&conn);
        let user_3 = create_test_user(&conn);
        let created = create_some_contact(&conn, user_1, user_2, user_3);
        let update_val = true;
        let mut res = None;
        let selected = if let Ok(created_0) = &created[0] {
            res = Some(contact::update_favorite(
                &conn,
                &user_1,
                &created_0.id,
                update_val.clone(),
            ));
            Some(contact::select_by_id(&conn, &user_1, &created_0.id))
        } else {
            None
        };
        delete_test_user(&conn, &user_1);
        delete_test_user(&conn, &user_2);
        delete_test_user(&conn, &user_3);
        for res in &created {
            assert!(res.is_ok(), "Failed to create a contact");
        }
        let created = created[0].as_ref().unwrap();
        let selected = selected.unwrap().expect("failed to select contact");
        assert_eq!(update_val, selected.favorite, "favorites do not match");
        assert_eq!(created.uid, selected.uid, "uids do not match");
        assert_eq!(created.email, selected.email, "emails do not match");
        assert_eq!(
            created.target_uid, selected.target_uid,
            "target_uids do not match"
        );
        assert!(
            res.unwrap().expect("Deletion returned an error").0 == created.id,
            "Update did not returned the correct id"
        );
        Ok(())
    })
}

#[test]
fn update_favorite_existing_not_owned() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let user_1 = create_test_user(&conn);
        let user_2 = create_test_user(&conn);
        let user_3 = create_test_user(&conn);
        let created = create_some_contact(&conn, user_1, user_2, user_3);
        let update_val = true;
        let mut res = None;
        let selected = if let Ok(created_0) = &created[0] {
            res = Some(contact::update_favorite(
                &conn,
                &user_2,
                &created_0.id,
                update_val.clone(),
            ));
            Some(contact::select_by_id(&conn, &user_1, &created_0.id))
        } else {
            None
        };
        delete_test_user(&conn, &user_1);
        delete_test_user(&conn, &user_2);
        delete_test_user(&conn, &user_3);
        for res in &created {
            assert!(res.is_ok(), "Failed to create a contact");
        }
        let created = created[0].as_ref().unwrap();
        let selected = selected.unwrap().expect("failed to select contact");
        assert_eq!(
            created.nickname, selected.nickname,
            "nicknames do not match"
        );
        assert_eq!(
            created.favorite, selected.favorite,
            "favorites do not match"
        );
        assert_eq!(created.uid, selected.uid, "uids do not match");
        assert_eq!(created.email, selected.email, "emails do not match");
        assert_eq!(
            created.target_uid, selected.target_uid,
            "target_uids do not match"
        );
        assert!(res.unwrap().is_err(), "Update should return an error");
        Ok(())
    })
}

#[test]
fn update_favorite_not_existing() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let user_1 = create_test_user(&conn);
        let contact_id = Uuid::new_v4();
        let update_val = true;
        let res = contact::update_favorite(&conn, &user_1, &contact_id, update_val.clone());
        delete_test_user(&conn, &user_1);
        assert!(res.is_err(), "Update should return an error");
        Ok(())
    })
}

#[test]
fn delete_existing() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let user_1 = create_test_user(&conn);
        let user_2 = create_test_user(&conn);
        let user_3 = create_test_user(&conn);
        let created = create_some_contact(&conn, user_1, user_2, user_3);
        let mut res = None;
        let selected = if let Ok(created_0) = &created[0] {
            res = Some(contact::delete(&conn, &user_1, &created_0.id));
            Some(contact::select_by_id(&conn, &user_1, &created_0.id))
        } else {
            None
        };
        delete_test_user(&conn, &user_1);
        delete_test_user(&conn, &user_2);
        delete_test_user(&conn, &user_3);
        for res in created {
            assert!(res.is_ok(), "Failed to create a contact");
        }
        let _selected = selected
            .unwrap()
            .expect_err("Deletion failed to erase contact");
        assert!(
            res.unwrap().expect("Deletion returned an error") == 1,
            "Deletion did not modify only 1 row"
        );
        Ok(())
    })
}

#[test]
fn delete_existing_not_owned() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let user_1 = create_test_user(&conn);
        let user_2 = create_test_user(&conn);
        let user_3 = create_test_user(&conn);
        let created = create_some_contact(&conn, user_1, user_2, user_3);
        let mut res = None;
        let selected = if let Ok(created_0) = &created[0] {
            res = Some(contact::delete(&conn, &user_2, &created_0.id));
            Some(contact::select_by_id(&conn, &user_1, &created_0.id))
        } else {
            None
        };
        delete_test_user(&conn, &user_1);
        delete_test_user(&conn, &user_2);
        delete_test_user(&conn, &user_3);
        for res in created {
            assert!(res.is_ok(), "Failed to create a contact");
        }
        let _selected = selected.unwrap().expect("failed to select contact");
        assert!(
            res.unwrap().expect("Deletion returned an error") == 0,
            "Deletion modified more row than expected"
        );
        Ok(())
    })
}

#[test]
fn delete_not_existing() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let user_1 = create_test_user(&conn);
        let contact_id = Uuid::new_v4();
        let res = contact::delete(&conn, &user_1, &contact_id);
        delete_test_user(&conn, &user_1);
        assert!(
            res.expect("Deletion returned an error") == 0,
            "Deletion modified more row than expected"
        );
        Ok(())
    })
}

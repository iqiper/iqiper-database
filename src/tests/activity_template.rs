use super::*;
use crate::activity_template::{self, NewActivityTemplate};
use diesel::PgConnection;
use std::convert::From;
use uuid::Uuid;

use super::{create_test_user, establish_connection};

fn create_user_and_activity(conn: &PgConnection) -> (Uuid, Uuid) {
    let user_uuid = create_test_user(&conn);
    let expected = NewActivityTemplate {
        name: String::from("going to work"),
        icon: None,
        uid: user_uuid,
        alert_message: None,
    };
    let created_contact = activity_template::create(&conn, expected.clone());
    let created = created_contact.expect("activity should've been created");
    (user_uuid, created.id)
}

fn create_activity(conn: &PgConnection, uid: Uuid) -> Uuid {
    let expected = NewActivityTemplate {
        name: format!("going to work {}", Uuid::new_v4()),
        icon: None,
        uid: uid,
        alert_message: None,
    };
    let created_contact = activity_template::create(conn, expected.clone());
    let created = created_contact.expect("activity should've been created");
    created.id
}

#[test]
fn create_activity_ok() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (_uid, _id) = create_user_and_activity(&conn);
        Ok(())
    })
}

#[test]
fn create_activity_bad_uid() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let expected = NewActivityTemplate {
            name: String::from("going to work"),
            icon: None,
            uid: Uuid::new_v4(),
            alert_message: None,
        };
        activity_template::create(&conn, expected.clone())
            .expect_err("The foreign key constraint should have made this call fail");
        Ok(())
    })
}

#[test]
fn create_activity_empty_name() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let user_uuid = create_test_user(&conn);
        let expected = NewActivityTemplate {
            name: String::from(""),
            icon: None,
            uid: user_uuid,
            alert_message: None,
        };
        activity_template::create(&conn, expected.clone())
            .expect_err("The empty string check should have made this call fail");
        Ok(())
    })
}

#[test]
fn create_activity_name_too_long() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let user_uuid = create_test_user(&conn);
        let expected = NewActivityTemplate {
            name: (0..101).map(|_| "X").collect::<String>(),
            icon: None,
            uid: user_uuid,
            alert_message: None,
        };
        activity_template::create(&conn, expected.clone())
            .expect_err("The string length check should have made this call fail");
        Ok(())
    })
}

#[test]
fn update_name_ok() {
    let conn = establish_connection();
    let (uid, id) = create_user_and_activity(&conn);
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        activity_template::update_name(&conn, &uid, &id, String::from("blaaaaa"))
            .expect("Name should've been modified");
        let obj =
            activity_template::select_by_id(&conn, &uid, &id).expect("the select should work");
        assert_eq!(obj.id, id, "The id should match");
        assert_eq!(obj.name, "blaaaaa", "The name should match");
        Ok(())
    })
}

#[test]
fn update_name_empty() {
    let conn = establish_connection();
    let (uid, id) = create_user_and_activity(&conn);
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        activity_template::update_name(&conn, &uid, &id, String::from(""))
            .expect_err("Empty name should've been refused");
        Ok(())
    })
}

#[test]
fn update_name_too_long() {
    let conn = establish_connection();
    let (uid, id) = create_user_and_activity(&conn);
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        activity_template::update_name(&conn, &uid, &id, (0..101).map(|_| "X").collect::<String>())
            .expect_err("Empty name should've been refused");
        Ok(())
    })
}

#[test]
fn update_alert_message_ok() {
    let conn = establish_connection();
    let (uid, id) = create_user_and_activity(&conn);
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        activity_template::update_alert_message(&conn, &uid, &id, Some(String::from("blaaaaa")))
            .expect("Alert message should've been modified");
        let obj =
            activity_template::select_by_id(&conn, &uid, &id).expect("the select should work");
        assert_eq!(obj.id, id, "The id should match");
        assert_eq!(
            obj.alert_message,
            Some(String::from("blaaaaa")),
            "The alert message should match"
        );
        Ok(())
    })
}

#[test]
fn update_alert_message_empty() {
    let conn = establish_connection();
    let (uid, id) = create_user_and_activity(&conn);
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        activity_template::update_alert_message(&conn, &uid, &id, None)
            .expect("Should've unset the alert message from db");
        Ok(())
    })
}

#[test]
fn update_alert_message_too_long() {
    let conn = establish_connection();
    let (uid, id) = create_user_and_activity(&conn);
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        activity_template::update_alert_message(
            &conn,
            &uid,
            &id,
            Some((0..1025).map(|_| "X").collect::<String>()),
        )
        .expect_err("Alert message over 1024 char should've been refused");
        Ok(())
    })
}

#[test]
fn delete_ok() {
    let conn = establish_connection();
    let (uid, id) = create_user_and_activity(&conn);
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let res =
            activity_template::delete(&conn, &uid, &id).expect("the activity should be deleted");
        assert_eq!(res, 1, "There should be 1 deleted record");
        let _obj = activity_template::select_by_id(&conn, &uid, &id)
            .expect_err("The object should be deleted");
        Ok(())
    })
}

#[test]
fn delete_wrong_uid() {
    let conn = establish_connection();
    let (_uid, id) = create_user_and_activity(&conn);
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let res = activity_template::delete(&conn, &Uuid::new_v4(), &id)
            .expect("the activity shouldn't be deleted");
        assert_eq!(res, 0, "There should be 0 deleted record");
        Ok(())
    })
}

#[test]
fn delete_wrong_id() {
    let conn = establish_connection();
    let (uid, _id) = create_user_and_activity(&conn);
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let res = activity_template::delete(&conn, &uid, &Uuid::new_v4())
            .expect("the activity shouldn't be deleted");
        assert_eq!(res, 0, "There should be 0 deleted record");
        Ok(())
    })
}

#[test]
fn select_all() {
    let conn = establish_connection();
    let (uid, _id) = create_user_and_activity(&conn);
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let _id2 = create_activity(&conn, uid);
        let res = activity_template::select_all(&conn, &uid).expect("a list of activities");
        assert_eq!(res.len(), 2, "There should be 2 records");
        Ok(())
    })
}

#[test]
fn select_id() {
    let conn = establish_connection();
    let (uid, _id) = create_user_and_activity(&conn);
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let id2 = create_activity(&conn, uid);
        let res =
            activity_template::select_by_id(&conn, &uid, &id2).expect("to fetch a single activity");
        assert_eq!(res.id, id2, "The record id should match");
        Ok(())
    })
}

#[test]
fn update_icon() {
    let conn = establish_connection();
    let (uid, id) = create_user_and_activity(&conn);
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        activity_template::update_icon(&conn, &uid, &id, Some(42))
            .expect("the icon the be updated");
        let obj =
            activity_template::select_by_id(&conn, &uid, &id).expect("the select should work");
        assert_eq!(obj.id, id, "The id should match");
        assert_eq!(obj.icon, Some(42), "The icon should match");
        activity_template::update_icon(&conn, &uid, &id, None).expect("the icon the be updated");
        let obj =
            activity_template::select_by_id(&conn, &uid, &id).expect("the select should work");
        assert_eq!(obj.id, id, "The id should match");
        assert_eq!(obj.icon, None, "The icon should match");
        Ok(())
    })
}

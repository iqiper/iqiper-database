use super::*;
use super::{create_test_user, delete_test_user, establish_connection};
use crate::fcm_token::{self, NewToken};
use crate::schema::iqiper::fcm_token as fcm_token_sch;
use chrono::{DateTime, Utc};
use diesel::{QueryDsl, RunQueryDsl};
use uuid::Uuid;

#[derive(Queryable)]
struct SelectToken {
    pub uid: Uuid,
    pub device_id: Uuid,
    pub token: String,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
}

#[test]
fn create_ok() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let user_uuid = create_test_user(&conn);
        let token = String::from("I am a token");
        let new_token = NewToken {
            uid: user_uuid,
            device_id: Uuid::new_v4(),
            token: token.clone(),
        };
        let res = fcm_token::upsert_fcm_token(&conn, new_token)
            .expect("Token creation returned an error");
        let select = fcm_token_sch::table
            .find((res.uid, res.device_id))
            .load::<SelectToken>(&conn);
        delete_test_user(&conn, &user_uuid);
        let select = select.expect("Token Selection returned an error");
        assert_eq!(select.len(), 1, "Wrong amount of token were created");
        assert_eq!(select[0].uid, user_uuid, "uids do not match");
        assert_eq!(select[0].token, token, "tokens do not match");
        Ok(())
    })
}

#[test]
fn create_without_user() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let user_uuid = Uuid::new_v4();
        let token = String::from("I am a token");
        let new_token = NewToken {
            uid: user_uuid,
            device_id: Uuid::new_v4(),
            token: token.clone(),
        };
        fcm_token::upsert_fcm_token(&conn, new_token)
            .expect_err("Token creation should return an error");
        Ok(())
    })
}

#[test]
fn delete_ok() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let user_uuid = create_test_user(&conn);
        let token = String::from("I am a token");
        let new_token = NewToken {
            uid: user_uuid,
            device_id: Uuid::new_v4(),
            token: token.clone(),
        };
        let res = fcm_token::upsert_fcm_token(&conn, new_token)
            .expect("Token creation returned an error");
        let _res2 = fcm_token::delete_fcm_token(&conn, &res.uid, &res.device_id)
            .expect("Token deletion returned an error");
        let select = fcm_token_sch::table
            .find((res.uid, res.device_id))
            .load::<SelectToken>(&conn);
        delete_test_user(&conn, &user_uuid);
        let select = select.expect("Token Selection returned an error");
        assert_eq!(select.len(), 0, "Wrong amount of token were deleted");
        Ok(())
    })
}

#[test]
fn delete_none() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let user_uuid = Uuid::new_v4();
        let token_uuid = Uuid::new_v4();
        fcm_token::delete_fcm_token(&conn, &user_uuid, &token_uuid)
            .expect_err("Token deletion should return an error");
        Ok(())
    })
}

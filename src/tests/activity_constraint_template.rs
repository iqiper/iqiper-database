use super::*;
use crate::activity_constraint_template::{self, NewActivityConstraintTemplate};
use crate::activity_template::{self, NewActivityTemplate};
use crate::common::UpdateResponse;
use crate::enums::ActivityConstraintTemplateType;
use diesel::PgConnection;
use uuid::Uuid;

use super::{create_test_user, establish_connection};

fn setup() -> (PgConnection, Uuid) {
    let connection = establish_connection();
    let user_uuid = create_test_user(&connection);
    (connection, user_uuid)
}

fn create_activity_template(conn: &PgConnection, uid: &Uuid) -> Uuid {
    let expected_activity = NewActivityTemplate {
        name: format!("Name {}", Uuid::new_v4()),
        uid: uid.clone(),
        icon: None,
        alert_message: None,
    };
    let created_activity = activity_template::create(conn, expected_activity.clone())
        .expect("activity should've been created");
    created_activity.id
}

fn create_activity_constraint_template(
    conn: &PgConnection,
    uid: &Uuid,
    activity_template_id: &Uuid,
) -> Uuid {
    let expected_contact = NewActivityConstraintTemplate {
        activity_template_id: activity_template_id.clone(),
        type_: ActivityConstraintTemplateType::Timer,
        start_offset: 0,
        notify_offset: Some(15),
        end_offset: 30,
        alert_message: None,
    };
    let created_activity_constraint =
        activity_constraint_template::create(conn, uid, expected_contact.clone())
            .expect("activity_constraint should've been created");
    assert_eq!(
        expected_contact.activity_template_id, created_activity_constraint.activity_template_id,
        "activity_template_id do not match"
    );
    assert_eq!(
        expected_contact.type_, created_activity_constraint.type_,
        "type_ do not match"
    );
    assert_eq!(
        expected_contact.notify_offset.is_some(),
        created_activity_constraint.notify_offset.is_some(),
        "notify_offset should be Some(x)"
    );
    assert_eq!(
        expected_contact.notify_offset.unwrap(),
        created_activity_constraint.notify_offset.unwrap(),
        "notify_offset do not match"
    );
    assert_eq!(
        expected_contact.start_offset, created_activity_constraint.start_offset,
        "start_offset do not match"
    );
    assert_eq!(
        expected_contact.end_offset, created_activity_constraint.end_offset,
        "end_offset do not match"
    );
    created_activity_constraint.id
}

#[test]
fn create_activity_constraint_ok() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        create_activity_constraint_template(&conn, &uid, &create_activity_template(&conn, &uid));
        Ok(())
    })
}

#[test]
fn create_activity_report_bad_activity_template_id() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let expected = NewActivityConstraintTemplate {
            activity_template_id: Uuid::new_v4(),
            type_: ActivityConstraintTemplateType::Timer,
            start_offset: 0,
            notify_offset: Some(15),
            end_offset: 30,
            alert_message: None,
        };
        activity_constraint_template::create(&conn, &uid, expected.clone())
            .expect_err("The foreign key constraint should have made this call fail");
        Ok(())
    })
}

#[test]
fn create_activity_constraint_template_forbidden() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let uid2 = create_test_user(&conn);
        let activity_template_id = create_activity_template(&conn, &uid2);
        let expected = NewActivityConstraintTemplate {
            activity_template_id: activity_template_id,
            type_: ActivityConstraintTemplateType::Timer,
            start_offset: 0,
            notify_offset: Some(55),
            end_offset: 120,
            alert_message: None,
        };
        activity_constraint_template::create(&conn, &uid, expected.clone())
            .expect_err("The foreign key constraint should have made this call fail");
        Ok(())
    })
}

#[test]
fn create_activity_w_start_offset_after_end_offset() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_template_id = create_activity_template(&conn, &uid);
        let expected = NewActivityConstraintTemplate {
            activity_template_id,
            type_: ActivityConstraintTemplateType::Timer,
            start_offset: 15,
            notify_offset: Some(5),
            end_offset: 10,
            alert_message: None,
        };
        activity_constraint_template::create(&conn, &uid, expected.clone())
            .expect_err("The logic checker should've make this fail");
        Ok(())
    })
}

#[test]
fn create_activity_w_timer_check_offset_equal_start_offset() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_template_id = create_activity_template(&conn, &uid);
        let expected = NewActivityConstraintTemplate {
            activity_template_id,
            type_: ActivityConstraintTemplateType::Timer,
            start_offset: 0,
            notify_offset: Some(0),
            end_offset: 10,
            alert_message: None,
        };
        activity_constraint_template::create(&conn, &uid, expected.clone())
            .expect_err("The logic checker should've make this fail");
        Ok(())
    })
}

#[test]
fn create_activity_w_timer_check_offset_equal_end_offset() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_template_id = create_activity_template(&conn, &uid);
        let expected = NewActivityConstraintTemplate {
            activity_template_id,
            type_: ActivityConstraintTemplateType::Timer,
            start_offset: 0,
            notify_offset: Some(10),
            end_offset: 10,
            alert_message: None,
        };
        activity_constraint_template::create(&conn, &uid, expected.clone())
            .expect_err("The logic checker should've make this fail");
        Ok(())
    })
}

#[test]
fn create_activity_w_timer_check_offset_lesser_start_offset() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_template_id = create_activity_template(&conn, &uid);
        let expected = NewActivityConstraintTemplate {
            activity_template_id,
            type_: ActivityConstraintTemplateType::Timer,
            start_offset: 10,
            notify_offset: Some(0),
            end_offset: 20,
            alert_message: None,
        };
        activity_constraint_template::create(&conn, &uid, expected.clone())
            .expect_err("The logic checker should've make this fail");
        Ok(())
    })
}

#[test]
fn create_activity_w_timer_check_offset_greater_end_offset() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_template_id = create_activity_template(&conn, &uid);
        let expected = NewActivityConstraintTemplate {
            activity_template_id,
            type_: ActivityConstraintTemplateType::Timer,
            start_offset: 0,
            notify_offset: Some(10),
            end_offset: 9,
            alert_message: None,
        };
        activity_constraint_template::create(&conn, &uid, expected.clone())
            .expect_err("The logic checker should've make this fail");
        Ok(())
    })
}

#[test]
fn delete_ok() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_template_id = create_activity_template(&conn, &uid);
        let id = create_activity_constraint_template(&conn, &uid, &activity_template_id);
        let res = activity_constraint_template::delete(&conn, &uid, &activity_template_id, &id)
            .expect("the activity should be deleted");
        assert_eq!(res, 1, "There should be 1 deleted record");
        let _obj =
            activity_constraint_template::select_by_id(&conn, &uid, &activity_template_id, &id)
                .expect_err("The object should be deleted");
        Ok(())
    })
}

#[test]
fn delete_wrong_uid() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_template_id = create_activity_template(&conn, &uid);
        let id = create_activity_constraint_template(&conn, &uid, &activity_template_id);
        activity_constraint_template::delete(&conn, &Uuid::new_v4(), &activity_template_id, &id)
            .expect_err("the activity shouldn't be deleted");
        Ok(())
    })
}

#[test]
fn delete_forbidden() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_template_id = create_activity_template(&conn, &uid);
        let uid2 = create_test_user(&conn);
        let id = create_activity_constraint_template(&conn, &uid, &activity_template_id);
        activity_constraint_template::delete(&conn, &uid2, &activity_template_id, &id)
            .expect_err("the activity shouldn't be deleted");
        Ok(())
    })
}

#[test]
fn delete_wrong_activity_template_id() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_template_id = create_activity_template(&conn, &uid);
        let id = create_activity_constraint_template(&conn, &uid, &activity_template_id);
        activity_constraint_template::delete(&conn, &uid, &Uuid::new_v4(), &id)
            .expect_err("the activity shouldn't be deleted");
        Ok(())
    })
}

#[test]
fn delete_wrong_id() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_template_id = create_activity_template(&conn, &uid);
        create_activity_constraint_template(&conn, &uid, &activity_template_id);
        activity_constraint_template::delete(&conn, &uid, &activity_template_id, &Uuid::new_v4())
            .expect_err("the activity shouldn't be deleted");
        Ok(())
    })
}

#[test]
fn select_all() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_template_id = create_activity_template(&conn, &uid);
        create_activity_constraint_template(&conn, &uid, &activity_template_id);
        create_activity_constraint_template(&conn, &uid, &activity_template_id);
        create_activity_constraint_template(&conn, &uid, &activity_template_id);
        let res = activity_constraint_template::select_all(&conn, &uid)
            .expect("the activities should be fetched");
        assert_eq!(res.len(), 3, "There should be 3 objects");
        Ok(())
    })
}

#[test]
fn select_all_id() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_template_id = create_activity_template(&conn, &uid);
        create_activity_constraint_template(&conn, &uid, &activity_template_id);
        create_activity_constraint_template(&conn, &uid, &activity_template_id);
        create_activity_constraint_template(&conn, &uid, &activity_template_id);
        let res = activity_constraint_template::select_all_id(&conn, &uid)
            .expect("the activities should be fetched");
        assert_eq!(res.len(), 3, "There should be 3 objects");
        Ok(())
    })
}

#[test]
fn select_all_by_activity() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_template_id = create_activity_template(&conn, &uid);
        create_activity_constraint_template(&conn, &uid, &activity_template_id);
        create_activity_constraint_template(&conn, &uid, &activity_template_id);
        create_activity_constraint_template(&conn, &uid, &activity_template_id);
        let res = activity_constraint_template::select_all_by_activity(
            &conn,
            &uid,
            &activity_template_id,
        )
        .expect("the activities should be fetched");
        assert_eq!(res.len(), 3, "There should be 3 objects");
        Ok(())
    })
}

#[test]
fn select_all_id_by_activity() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_template_id = create_activity_template(&conn, &uid);
        create_activity_constraint_template(&conn, &uid, &activity_template_id);
        create_activity_constraint_template(&conn, &uid, &activity_template_id);
        create_activity_constraint_template(&conn, &uid, &activity_template_id);
        let res = activity_constraint_template::select_all_id_by_activity(
            &conn,
            &uid,
            &activity_template_id,
        )
        .expect("the activities should be fetched");
        assert_eq!(res.len(), 3, "There should be 3 objects");
        Ok(())
    })
}

#[test]
fn select_id() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_template_id = create_activity_template(&conn, &uid);
        let id = create_activity_constraint_template(&conn, &uid, &activity_template_id);
        let _res =
            activity_constraint_template::select_by_id(&conn, &uid, &activity_template_id, &id)
                .expect("the activities should be fetched");
        Ok(())
    })
}

#[test]
fn modifiy_activity_constraint_template_ok() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let activity_template_id = create_activity_template(&conn, &uid);
        let constraint_id = create_activity_constraint_template(&conn, &uid, &activity_template_id);
        let res = activity_constraint_template::select_by_id(
            &conn,
            &uid,
            &activity_template_id,
            &constraint_id,
        )
        .expect("the activities should be fetched");
        let expected = NewActivityConstraintTemplate {
            activity_template_id: activity_template_id,
            type_: res.type_,
            start_offset: 0,
            notify_offset: Some(5),
            end_offset: 10,
            alert_message: None,
        };
        let received: UpdateResponse = activity_constraint_template::modify(
            &conn,
            &uid,
            &activity_template_id,
            &constraint_id,
            expected.clone(),
        )
        .expect("The update object");
        assert_eq!(received.id, constraint_id, "id's don't match");
        Ok(())
    })
}

#[test]
fn modifiy_activity_constraint_template_forbidden() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let uid2 = create_test_user(&conn);
        let activity_template_id = create_activity_template(&conn, &uid);
        let constraint_id = create_activity_constraint_template(&conn, &uid, &activity_template_id);
        let res = activity_constraint_template::select_by_id(
            &conn,
            &uid,
            &activity_template_id,
            &constraint_id,
        )
        .expect("the activities should be fetched");
        let expected = NewActivityConstraintTemplate {
            activity_template_id: activity_template_id,
            type_: res.type_,
            start_offset: 0,
            notify_offset: Some(5),
            end_offset: 10,
            alert_message: None,
        };
        activity_constraint_template::modify(
            &conn,
            &uid2,
            &activity_template_id,
            &constraint_id,
            expected.clone(),
        )
        .expect_err("Shouldn't be able to modify constraint not belonging to the user");
        Ok(())
    })
}

#[test]
fn modifiy_activity_w_start_date_before_end_offset() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let _uid2 = create_test_user(&conn);
        let activity_template_id = create_activity_template(&conn, &uid);
        let constraint_id = create_activity_constraint_template(&conn, &uid, &activity_template_id);
        let _res = activity_constraint_template::select_by_id(
            &conn,
            &uid,
            &activity_template_id,
            &constraint_id,
        )
        .expect("the activities should be fetched");
        let expected = NewActivityConstraintTemplate {
            activity_template_id: activity_template_id,
            type_: ActivityConstraintTemplateType::Timer,
            start_offset: 15,
            notify_offset: Some(5),
            end_offset: 10,
            alert_message: None,
        };
        activity_constraint_template::modify(
            &conn,
            &uid,
            &activity_template_id,
            &constraint_id,
            expected.clone(),
        )
        .expect_err("The logic checker should've make this fail");
        Ok(())
    })
}

#[test]
fn modifiy_activity_w_timer_check_date_equal_start_date() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let _uid2 = create_test_user(&conn);
        let activity_template_id = create_activity_template(&conn, &uid);
        let constraint_id = create_activity_constraint_template(&conn, &uid, &activity_template_id);
        let _res = activity_constraint_template::select_by_id(
            &conn,
            &uid,
            &activity_template_id,
            &constraint_id,
        )
        .expect("the activities should be fetched");
        let expected = NewActivityConstraintTemplate {
            activity_template_id: activity_template_id,
            type_: ActivityConstraintTemplateType::Timer,
            start_offset: 0,
            notify_offset: Some(0),
            end_offset: 10,
            alert_message: None,
        };
        activity_constraint_template::modify(
            &conn,
            &uid,
            &activity_template_id,
            &constraint_id,
            expected.clone(),
        )
        .expect_err("The logic checker should've make this fail");
        Ok(())
    })
}

#[test]
fn modifiy_activity_w_timer_check_date_equal_end_offset() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let _uid2 = create_test_user(&conn);
        let activity_template_id = create_activity_template(&conn, &uid);
        let constraint_id = create_activity_constraint_template(&conn, &uid, &activity_template_id);
        let _res = activity_constraint_template::select_by_id(
            &conn,
            &uid,
            &activity_template_id,
            &constraint_id,
        )
        .expect("the activities should be fetched");
        let expected = NewActivityConstraintTemplate {
            activity_template_id: activity_template_id,
            type_: ActivityConstraintTemplateType::Timer,
            start_offset: 0,
            notify_offset: Some(10),
            end_offset: 10,
            alert_message: None,
        };
        activity_constraint_template::modify(
            &conn,
            &uid,
            &activity_template_id,
            &constraint_id,
            expected.clone(),
        )
        .expect_err("The logic checker should've make this fail");
        Ok(())
    })
}

#[test]
fn modifiy_activity_w_timer_check_date_lesser_start_date() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let _uid2 = create_test_user(&conn);
        let activity_template_id = create_activity_template(&conn, &uid);
        let constraint_id = create_activity_constraint_template(&conn, &uid, &activity_template_id);
        let _res = activity_constraint_template::select_by_id(
            &conn,
            &uid,
            &activity_template_id,
            &constraint_id,
        )
        .expect("the activities should be fetched");
        let expected = NewActivityConstraintTemplate {
            activity_template_id: activity_template_id,
            type_: ActivityConstraintTemplateType::Timer,
            start_offset: 10,
            notify_offset: Some(0),
            end_offset: 10,
            alert_message: None,
        };
        activity_constraint_template::modify(
            &conn,
            &uid,
            &activity_template_id,
            &constraint_id,
            expected.clone(),
        )
        .expect_err("The logic checker should've make this fail");
        Ok(())
    })
}

#[test]
fn modifiy_activity_w_timer_check_date_greater_end_offset() {
    let (conn, uid) = setup();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let _uid2 = create_test_user(&conn);
        let activity_template_id = create_activity_template(&conn, &uid);
        let constraint_id = create_activity_constraint_template(&conn, &uid, &activity_template_id);
        let _res = activity_constraint_template::select_by_id(
            &conn,
            &uid,
            &activity_template_id,
            &constraint_id,
        )
        .expect("the activities should be fetched");
        let expected = NewActivityConstraintTemplate {
            activity_template_id: activity_template_id,
            type_: ActivityConstraintTemplateType::Timer,
            start_offset: 0,
            notify_offset: Some(10),
            end_offset: 9,
            alert_message: None,
        };
        activity_constraint_template::modify(
            &conn,
            &uid,
            &activity_template_id,
            &constraint_id,
            expected.clone(),
        )
        .expect_err("The logic checker should've make this fail");
        Ok(())
    })
}

use super::*;
use crate::activity::{self, NewActivity};
use chrono::{Duration, Utc};
use diesel::PgConnection;
use std::convert::From;
use uuid::Uuid;

fn create_user_and_activity(conn: &PgConnection) -> (Uuid, Uuid) {
    let user_uuid = create_test_user(&conn);
    let expected = NewActivity {
        name: String::from("going to work"),
        icon: None,
        alert_message: None,
        uid: user_uuid,
        start_date: None,
    };
    let created_contact = activity::create(&conn, expected.clone());
    let created = created_contact.expect("activity should've been created");
    (user_uuid, created.id)
}

#[test]
fn create_activity_ok() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (_uid, _id) = create_user_and_activity(&conn);
        Ok(())
    })
}

#[test]
fn create_activity_bad_uid() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let expected = NewActivity {
            name: String::from("going to work"),
            icon: None,
            alert_message: None,
            uid: Uuid::new_v4(),
            start_date: None,
        };
        activity::create(&conn, expected.clone())
            .expect_err("The foreign key constraint should have made this call fail");
        Ok(())
    })
}

#[test]
fn create_activity_empty_name() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let user_uuid = create_test_user(&conn);
        let expected = NewActivity {
            name: String::from(""),
            icon: None,
            alert_message: None,
            uid: user_uuid,
            start_date: None,
        };
        activity::create(&conn, expected.clone())
            .expect_err("The empty string check should have made this call fail");
        Ok(())
    })
}

#[test]
fn create_activity_name_too_long() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let user_uuid = create_test_user(&conn);
        let expected = NewActivity {
            name: (0..101).map(|_| "X").collect::<String>(),
            icon: None,
            alert_message: None,
            uid: user_uuid,
            start_date: None,
        };
        activity::create(&conn, expected.clone())
            .expect_err("The string length check should have made this call fail");
        Ok(())
    })
}

#[test]
fn update_name_ok() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, id) = create_user_and_activity(&conn);
        activity::update_name(&conn, &uid, &id, String::from("blaaaaa"))
            .expect("Name should've been modified");
        let obj = activity::select_by_id(&conn, &uid, &id).expect("the select should work");
        assert_eq!(obj.id, id, "The id should match");
        assert_eq!(obj.name, "blaaaaa", "The name should match");
        Ok(())
    })
}

#[test]
fn update_name_empty() {
    let conn = establish_connection();
    let (uid, id) = create_user_and_activity(&conn);
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        activity::update_name(&conn, &uid, &id, String::from(""))
            .expect_err("Empty name should've been refused");
        Ok(())
    })
}

#[test]
fn update_name_too_long() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, id) = create_user_and_activity(&conn);
        activity::update_name(&conn, &uid, &id, (0..101).map(|_| "X").collect::<String>())
            .expect_err("Empty name should've been refused");
        Ok(())
    })
}

#[test]
fn update_name_read_only() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, id) = create_user_and_activity(&conn);
        activity::end(&conn, &uid, &id).unwrap();
        let err =
            activity::update_name(&conn, &uid, &id, (0..101).map(|_| "X").collect::<String>())
                .expect_err("Empty name should've been refused");
        assert_eq!(
            err,
            crate::error::IqiperDatabaseError::ReadOnly,
            "Should be readonly"
        );
        Ok(())
    })
}

#[test]
fn update_alert_message_ok() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, id) = create_user_and_activity(&conn);
        activity::update_alert_message(&conn, &uid, &id, Some(String::from("blaaaaa")))
            .expect("Alert message should've been modified");
        let obj = activity::select_by_id(&conn, &uid, &id).expect("the select should work");
        assert_eq!(obj.id, id, "The id should match");
        assert_eq!(
            obj.alert_message,
            Some(String::from("blaaaaa")),
            "The alert message should match"
        );
        Ok(())
    })
}

#[test]
fn update_alert_message_empty() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, id) = create_user_and_activity(&conn);
        activity::update_alert_message(&conn, &uid, &id, None)
            .expect("Should've unset the message from db");
        Ok(())
    })
}

#[test]
fn update_alert_message_too_long() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, id) = create_user_and_activity(&conn);
        activity::update_alert_message(
            &conn,
            &uid,
            &id,
            Some((0..1025).map(|_| "X").collect::<String>()),
        )
        .expect_err("Alert message over 1024 char should've been refused");
        Ok(())
    })
}

#[test]
fn update_alert_message_read_only() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, id) = create_user_and_activity(&conn);
        activity::end(&conn, &uid, &id).unwrap();
        let err = activity::update_alert_message(
            &conn,
            &uid,
            &id,
            Some((0..1025).map(|_| "X").collect::<String>()),
        )
        .expect_err("Alert message over 1024 char should've been refused");
        assert_eq!(
            err,
            crate::error::IqiperDatabaseError::ReadOnly,
            "Should be read only"
        );
        Ok(())
    })
}

#[test]
fn delete_ok() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, id) = create_user_and_activity(&conn);
        let res = activity::delete(&conn, &uid, id).expect("the activity should be deleted");
        assert_eq!(res, 1, "There should be 1 deleted record");
        let _obj =
            activity::select_by_id(&conn, &uid, &id).expect_err("The object should be deleted");
        Ok(())
    })
}

#[test]
fn delete_wrong_uid() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (_uid, id) = create_user_and_activity(&conn);
        let res = activity::delete(&conn, &Uuid::new_v4(), id)
            .expect("the activity shouldn't be deleted");
        assert_eq!(res, 0, "There should be 0 deleted record");
        Ok(())
    })
}

#[test]
fn delete_wrong_id() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, _id) = create_user_and_activity(&conn);
        let res = activity::delete(&conn, &uid, Uuid::new_v4())
            .expect("the activity shouldn't be deleted");
        assert_eq!(res, 0, "There should be 0 deleted record");
        Ok(())
    })
}

#[test]
fn select_all() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, _id) = create_user_and_activity(&conn);
        let _id2 = create_activity(&conn, &uid, true, None, None);
        let res = activity::select_all(&conn, &uid).expect("a list of activities");
        assert_eq!(res.len(), 2, "There should be 2 records");
        Ok(())
    })
}

#[test]
fn select_all_paginated_offset_unlogged() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, _id) = create_user_and_activity(&conn);
        let uid2 = create_test_user(&conn);
        create_activity(&conn, &uid, true, None, None);
        create_activity(&conn, &uid2, true, None, None);
        create_activity(&conn, &uid, true, None, None);
        let res = activity::select_all_paginated_unlogged(&conn, None, 3)
            .expect("the activities should be fetched");
        assert_eq!(res.len(), 3, "There should be 3 objects");
        let res2 =
            activity::select_all_paginated_unlogged(&conn, Some((res[1].id, res[1].created_at)), 1)
                .expect("the activities should be fetched");
        assert_eq!(res2.len(), 1, "There should be 1 object");
        assert_eq!(res2[0], res[2], "The object mismatch");
        Ok(())
    })
}

#[test]
fn select_id_unlogged() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, _id) = create_user_and_activity(&conn);
        let id2 = create_activity(&conn, &uid, true, None, None);
        let res = activity::select_by_id_unlogged(&conn, &id2).expect("to fetch a single activity");
        assert_eq!(res.id, id2, "The record id should match");
        Ok(())
    })
}

#[test]
fn select_id() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, _id) = create_user_and_activity(&conn);
        let id2 = create_activity(&conn, &uid, true, None, None);
        let res = activity::select_by_id(&conn, &uid, &id2).expect("to fetch a single activity");
        assert_eq!(res.id, id2, "The record id should match");
        Ok(())
    })
}

#[test]
fn select_by_activity_constraint() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, _id) = create_user_and_activity(&conn);
        let activity_id = create_activity(&conn, &uid, true, None, None);
        let constraint_id =
            create_activity_constraint(&conn, &uid, &activity_id, true, None, None, None, None);
        let res = activity::select_by_activity_constraint_unlogged(&conn, &constraint_id)
            .expect("to fetch a single activity");
        assert_eq!(res.id, activity_id, "The record id should match");
        Ok(())
    })
}

#[test]
fn update_icon() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, id) = create_user_and_activity(&conn);
        activity::update_icon(&conn, &uid, &id, Some(42)).expect("the icon the be updated");
        let obj = activity::select_by_id(&conn, &uid, &id).expect("the select should work");
        assert_eq!(obj.id, id, "The id should match");
        assert_eq!(obj.icon, Some(42), "The icon should match");
        activity::update_icon(&conn, &uid, &id, None).expect("the icon the be updated");
        let obj = activity::select_by_id(&conn, &uid, &id).expect("the select should work");
        assert_eq!(obj.id, id, "The id should match");
        assert_eq!(obj.icon, None, "The icon should match");
        Ok(())
    })
}

#[test]
fn update_icon_readonly() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, id) = create_user_and_activity(&conn);
        activity::update_icon(&conn, &uid, &id, Some(42)).expect("the icon the be updated");
        let obj = activity::select_by_id(&conn, &uid, &id).expect("the select should work");
        assert_eq!(obj.id, id, "The id should match");
        assert_eq!(obj.icon, Some(42), "The icon should match");
        activity::end(&conn, &uid, &id).expect("should end the activity");
        let err = activity::update_icon(&conn, &uid, &id, None)
            .expect_err("the icon shouldn't be updated");
        assert_eq!(
            err,
            crate::error::IqiperDatabaseError::ReadOnly,
            "Should be read only"
        );
        Ok(())
    })
}

#[test]
fn end_activity() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, id) = create_user_and_activity(&conn);
        let res = activity::end(&conn, &uid, &id).expect("The record to be updated");
        assert_eq!(res.id, id, "The id should match");
        let select = activity::select_by_id(&conn, &uid, &id).expect("The record to be updated");
        assert_eq!(
            select.end_date.is_some(),
            true,
            "The end_date should be set"
        );
        Ok(())
    })
}

#[test]
fn end_activity_duplicate() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let (uid, id) = create_user_and_activity(&conn);
        let res = activity::end(&conn, &uid, &id).expect("The record to be updated");
        assert_eq!(res.id, id, "The id should match");
        let select = activity::select_by_id(&conn, &uid, &id).expect("The record to be updated");
        assert_eq!(
            select.end_date.is_some(),
            true,
            "The end_date should be set"
        );
        let err = activity::end(&conn, &uid, &id).expect_err("The record shoudn't be updated");
        assert_eq!(
            err,
            crate::error::IqiperDatabaseError::DatabaseError(diesel::result::Error::NotFound),
            "Should be read only"
        );
        Ok(())
    })
}

#[test]
fn end_activity_if_done_ok_no_constraint() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let uid = create_test_user(&conn);
        let activity = create_activity(
            &conn,
            &uid,
            false,
            Some(Utc::now() - Duration::minutes(10)),
            None,
        );
        let res = activity::end_if_done(&conn, &uid, &activity)
            .expect("The activity should be marked as done");
        assert_eq!(res.is_some(), true, "The activity should've been updated");
        let select =
            activity::select_by_id(&conn, &uid, &activity).expect("The record to be updated");
        assert_eq!(
            select.end_date.is_some(),
            true,
            "The end_date should be set"
        );
        Ok(())
    })
}

#[test]
fn end_activity_if_done_ok_ended_constraints() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let uid = create_test_user(&conn);
        let activity = create_activity(
            &conn,
            &uid,
            false,
            Some(Utc::now() - Duration::minutes(10)),
            None,
        );
        create_activity_constraint(
            &conn,
            &uid,
            &activity,
            false,
            Some(Utc::now() - Duration::minutes(10)),
            Some(Utc::now() - Duration::minutes(1)),
            Some(Utc::now() - Duration::seconds(30)),
            None,
        );
        create_activity_constraint(
            &conn,
            &uid,
            &activity,
            false,
            Some(Utc::now() - Duration::minutes(10)),
            Some(Utc::now() - Duration::minutes(2)),
            Some(Utc::now() - Duration::minutes(1)),
            None,
        );
        let res = activity::end_if_done(&conn, &uid, &activity)
            .expect("The activity should be marked as done");
        assert_eq!(res.is_some(), true, "The activity should've been updated");
        let select =
            activity::select_by_id(&conn, &uid, &activity).expect("The record to be updated");
        assert_eq!(
            select.end_date.is_some(),
            true,
            "The end_date should be set"
        );
        Ok(())
    })
}

#[test]
fn end_activity_if_done_ok_alerted_constraint() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let uid = create_test_user(&conn);
        let activity = create_activity(
            &conn,
            &uid,
            false,
            Some(Utc::now() - Duration::minutes(10)),
            None,
        );
        let constraint = create_activity_constraint(
            &conn,
            &uid,
            &activity,
            false,
            Some(Utc::now() - Duration::minutes(10)),
            Some(Utc::now() + Duration::seconds(30)),
            Some(Utc::now() + Duration::minutes(1)),
            None,
        );
        create_activity_constraint(
            &conn,
            &uid,
            &activity,
            false,
            Some(Utc::now() - Duration::minutes(10)),
            Some(Utc::now() - Duration::minutes(2)),
            Some(Utc::now() - Duration::minutes(1)),
            None,
        );
        crate::communication::create_from_constraint(
            &conn,
            &constraint,
            crate::communication::NewCommunication {
                uid,
                type_: crate::enums::CommunicationType::Email,
                reason: crate::enums::CommunicationReasonType::Alert,
            },
        )
        .unwrap();
        let res = activity::end_if_done(&conn, &uid, &activity)
            .expect("The activity should be marked as done");
        assert_eq!(res.is_some(), true, "The activity should've been updated");
        let select =
            activity::select_by_id(&conn, &uid, &activity).expect("The record to be updated");
        assert_eq!(
            select.end_date.is_some(),
            true,
            "The end_date should be set"
        );
        Ok(())
    })
}

#[test]
fn end_activity_if_done_ok_alerted_constraint_bad_communication_type() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let uid = create_test_user(&conn);
        let activity = create_activity(
            &conn,
            &uid,
            false,
            Some(Utc::now() - Duration::minutes(10)),
            None,
        );
        let constraint = create_activity_constraint(
            &conn,
            &uid,
            &activity,
            false,
            Some(Utc::now() - Duration::minutes(10)),
            Some(Utc::now() + Duration::seconds(30)),
            Some(Utc::now() + Duration::minutes(1)),
            None,
        );
        create_activity_constraint(
            &conn,
            &uid,
            &activity,
            false,
            Some(Utc::now() - Duration::minutes(10)),
            Some(Utc::now() - Duration::minutes(2)),
            Some(Utc::now() - Duration::minutes(1)),
            None,
        );
        crate::communication::create_from_constraint(
            &conn,
            &constraint,
            crate::communication::NewCommunication {
                uid,
                type_: crate::enums::CommunicationType::Email,
                reason: crate::enums::CommunicationReasonType::Check,
            },
        )
        .unwrap();
        let res = activity::end_if_done(&conn, &uid, &activity)
            .expect("The activity should be marked as done");
        assert_eq!(
            res.is_none(),
            true,
            "The activity shouldn't've been updated"
        );
        let select =
            activity::select_by_id(&conn, &uid, &activity).expect("The record to be updated");
        assert_eq!(
            select.end_date.is_some(),
            false,
            "The end_date shouldn't be set"
        );
        Ok(())
    })
}

#[test]
fn end_activity_if_done_ok_alerted_constraint_one_not_ended() {
    let conn = establish_connection();
    conn.test_transaction::<_, diesel::result::Error, _>(|| {
        let uid = create_test_user(&conn);
        let activity = create_activity(
            &conn,
            &uid,
            false,
            Some(Utc::now() - Duration::minutes(10)),
            None,
        );
        let constraint = create_activity_constraint(
            &conn,
            &uid,
            &activity,
            false,
            Some(Utc::now() - Duration::minutes(10)),
            Some(Utc::now() + Duration::seconds(30)),
            Some(Utc::now() + Duration::minutes(1)),
            None,
        );
        create_activity_constraint(
            &conn,
            &uid,
            &activity,
            false,
            Some(Utc::now() - Duration::minutes(10)),
            Some(Utc::now() + Duration::minutes(1)),
            Some(Utc::now() + Duration::minutes(2)),
            None,
        );
        crate::communication::create_from_constraint(
            &conn,
            &constraint,
            crate::communication::NewCommunication {
                uid,
                type_: crate::enums::CommunicationType::Email,
                reason: crate::enums::CommunicationReasonType::Alert,
            },
        )
        .unwrap();
        let res = activity::end_if_done(&conn, &uid, &activity)
            .expect("The activity should be marked as done");
        assert_eq!(
            res.is_none(),
            true,
            "The activity shouldn't've been updated"
        );
        let select =
            activity::select_by_id(&conn, &uid, &activity).expect("The record to be updated");
        assert_eq!(
            select.end_date.is_some(),
            false,
            "The end_date shouldn't be set"
        );
        Ok(())
    })
}

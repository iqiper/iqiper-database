use serde::{Deserialize, Serialize};

use strum_macros::{Display, EnumString, EnumVariantNames};

#[derive(
    EnumString,
    EnumVariantNames,
    Display,
    Clone,
    Debug,
    PartialEq,
    diesel_derive_enum::DbEnum,
    Serialize,
    Deserialize,
)]
#[strum(serialize_all = "snake_case")]
#[serde(rename_all = "snake_case")]
#[DieselType = "Activity_constraint_type"]
pub enum ActivityConstraintType {
    #[db_rename = "TIMER"]
    Timer,
}

#[derive(
    EnumString,
    EnumVariantNames,
    Display,
    Clone,
    Debug,
    PartialEq,
    diesel_derive_enum::DbEnum,
    Serialize,
    Deserialize,
)]
#[strum(serialize_all = "snake_case")]
#[serde(rename_all = "snake_case")]
#[DieselType = "Activity_constraint_template_type"]
pub enum ActivityConstraintTemplateType {
    #[db_rename = "TIMER"]
    Timer,
}

#[derive(
    EnumString,
    EnumVariantNames,
    Display,
    Clone,
    Debug,
    PartialEq,
    Eq,
    Hash,
    diesel_derive_enum::DbEnum,
    Serialize,
    Deserialize,
)]
#[strum(serialize_all = "snake_case")]
#[serde(rename_all = "snake_case")]
#[DieselType = "Contact_type"]
pub enum ContactType {
    #[db_rename = "EMAIL"]
    Email,
    #[db_rename = "FRIEND"]
    Friend,
}

#[derive(
    EnumString,
    EnumVariantNames,
    Display,
    Clone,
    Debug,
    PartialEq,
    Eq,
    Hash,
    diesel_derive_enum::DbEnum,
    Serialize,
    Deserialize,
)]
#[strum(serialize_all = "snake_case")]
#[serde(rename_all = "snake_case")]
#[DieselType = "Communication_type"]
pub enum CommunicationType {
    #[db_rename = "EMAIL"]
    Email,
}

#[derive(
    EnumString,
    EnumVariantNames,
    Display,
    Clone,
    Debug,
    PartialEq,
    Eq,
    Hash,
    diesel_derive_enum::DbEnum,
    Serialize,
    Deserialize,
)]
#[strum(serialize_all = "snake_case")]
#[serde(rename_all = "snake_case")]
#[DieselType = "Communication_reason_type"]
pub enum CommunicationReasonType {
    #[db_rename = "CHECK"]
    Check,
    #[db_rename = "ALERT"]
    Alert,
    #[db_rename = "FORCED_ALERT"]
    ForcedAlert,
    #[db_rename = "CORRECTION"]
    Correction,
}

#[derive(
    EnumString,
    EnumVariantNames,
    Display,
    Clone,
    Debug,
    PartialEq,
    Eq,
    Hash,
    diesel_derive_enum::DbEnum,
    Serialize,
    Deserialize,
)]
#[strum(serialize_all = "snake_case")]
#[serde(rename_all = "snake_case")]
#[DieselType = "Communication_status"]
pub enum CommunicationStatus {
    #[db_rename = "FAILED"]
    Failed,
    #[db_rename = "RETRYING"]
    Retrying,
    #[db_rename = "SENT"]
    Sent,
}

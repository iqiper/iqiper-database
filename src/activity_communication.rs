use crate::activity::SelectActivity;
use crate::communication::{SelectCommunication, COMMUNICATION_SELECT_COLUMNS};
use crate::enums::CommunicationReasonType;
use crate::schema::iqiper::{
    activity, activity_communication, activity_constraint, activity_constraint_communication,
    communication,
};
use chrono::{DateTime, Utc};
use diesel::pg::expression::dsl::*;
use diesel::{Connection, ExpressionMethods, Insertable, PgConnection, QueryDsl, RunQueryDsl};

use crate::error::IqiperDatabaseError;

use serde::{Deserialize, Serialize};

use uuid::Uuid;

/// Select all activity communication
///
/// # Return Value
/// # Arguments
/// - `conn` - The connection object
/// - `uid` - The user id
/// - `activity_id` - The activity id to select
///  
///  A list of communication
pub fn select_all(
    conn: &PgConnection,
    uid: &Uuid,
    activity_id: &Uuid,
) -> Result<Vec<SelectCommunication>, IqiperDatabaseError> {
    Ok(activity_communication::table
        .inner_join(communication::table)
        .inner_join(activity::table)
        .filter(communication::columns::uid.eq(uid))
        .filter(activity::columns::id.eq(activity_id))
        .order_by((
            communication::columns::created_at,
            communication::columns::id,
        ))
        .select(COMMUNICATION_SELECT_COLUMNS)
        .load::<SelectCommunication>(conn)?)
}

/// Select a single activity communication
///
/// # Arguments
/// - `conn` - The connection object
/// - `uid` - The user id
/// - `activity_id` - The activity id to select
/// - `communication_id` - The communication id to select
///  
/// # Return Value
///  A list of communication
pub fn select_by_id(
    conn: &PgConnection,
    uid: &Uuid,
    activity_id: &Uuid,
    communication_id: &Uuid,
) -> Result<SelectCommunication, IqiperDatabaseError> {
    Ok(activity_communication::table
        .inner_join(communication::table)
        .filter(communication::columns::uid.eq(uid))
        .filter(communication::columns::id.eq(communication_id))
        .filter(activity_communication::columns::activity_id.eq(activity_id))
        .order_by((
            communication::columns::created_at,
            communication::columns::id,
        ))
        .select(COMMUNICATION_SELECT_COLUMNS)
        .first::<SelectCommunication>(conn)?)
}

/// Select activity by communication
///
/// # Arguments
/// - `conn` - The connection object
/// - `uid` - The user id
/// - `activity_id` - The activity id to select
/// - `communication_id` - The communication id to select
///  
/// # Return Value
///  A list of communication
pub fn select_activity_by_communication(
    conn: &PgConnection,
    uid: &Uuid,
    communication_id: &Uuid,
) -> Result<SelectActivity, IqiperDatabaseError> {
    Ok(activity_communication::table
        .inner_join(activity::table)
        .inner_join(communication::table)
        .filter(communication::columns::uid.eq(uid))
        .filter(communication::columns::id.eq(communication_id))
        .order_by((activity::columns::created_at, activity::columns::id))
        .select(crate::activity::SELECT_COLUMNS)
        .first::<SelectActivity>(conn)?)
}

/// Return a list of activity communication id corresponding to the search reasons
///
/// # Arguments
/// - `conn` - The connection object
/// - `uid` - The user id
/// - `activity_id` - The activity id to select
/// - `reasons` - The list of reasons to match
///
/// # Return Value
///  A list of communications ids
pub fn check_if_activity_has_reason(
    conn: &PgConnection,
    uid: &Uuid,
    activity_id: &Uuid,
    reasons: Vec<CommunicationReasonType>,
) -> Result<Vec<Uuid>, IqiperDatabaseError> {
    Ok(
        conn.transaction(|| -> Result<Vec<Uuid>, IqiperDatabaseError> {
            let res_activity: Vec<Uuid> = activity_communication::table
                .inner_join(communication::table)
                .filter(communication::columns::uid.eq(uid))
                .filter(activity_communication::columns::activity_id.eq(activity_id))
                .filter(communication::columns::reason.eq(any(reasons.clone())))
                .order_by((
                    communication::columns::created_at,
                    communication::columns::id,
                ))
                .select(communication::columns::id)
                .load::<Uuid>(conn)?;

            let res_constraint: Vec<Uuid> = activity_constraint_communication::table
                .inner_join(communication::table)
                .inner_join(activity_constraint::table)
                .filter(communication::columns::uid.eq(uid))
                .filter(activity_constraint::columns::activity_id.eq(activity_id))
                .filter(communication::columns::reason.eq(any(reasons)))
                .order_by((
                    communication::columns::created_at,
                    communication::columns::id,
                ))
                .select(communication::columns::id)
                .load::<Uuid>(conn)?;
            Ok(res_activity
                .into_iter()
                .chain(res_constraint.into_iter())
                .collect::<Vec<Uuid>>())
        })?,
    )
}

/// Create a new activity constraint communication
///
/// # Arguments
/// - `conn` - The connection object
/// - `uid` - The user id
/// - `new_obj` - The object to create
///
/// # Return Value
///  The created record
pub fn create(
    conn: &PgConnection,
    uid: &Uuid,
    new_obj: NewActivityCommunication,
) -> Result<ActivityCommunication, IqiperDatabaseError> {
    Ok(
        conn.transaction(|| -> Result<ActivityCommunication, IqiperDatabaseError> {
            let _activity_id: Uuid = activity::table
                .filter(activity::columns::uid.eq(uid))
                .select(activity::columns::id)
                .for_update()
                .filter(activity::columns::id.eq(new_obj.activity_id))
                .first::<Uuid>(conn)?;
            let _communication_id: Uuid = communication::table
                .filter(communication::columns::uid.eq(uid))
                .select(communication::columns::id)
                .for_update()
                .find(new_obj.communication_id)
                .first::<Uuid>(conn)?;
            let res = diesel::insert_into(activity_communication::table)
                .values(new_obj)
                .returning(SELECT_COLUMNS)
                .get_result::<ActivityCommunication>(conn)?;
            Ok(res)
        })?,
    )
}

type ActivityConstraintCommmunicationColumns = (
    activity_communication::columns::activity_id,
    activity_communication::columns::communication_id,
    activity_communication::columns::created_at,
);

pub const SELECT_COLUMNS: ActivityConstraintCommmunicationColumns = (
    activity_communication::columns::activity_id,
    activity_communication::columns::communication_id,
    activity_communication::columns::created_at,
);

#[derive(
    PartialEq,
    Debug,
    Clone,
    Queryable,
    Identifiable,
    Associations,
    Insertable,
    Serialize,
    Deserialize,
)]
#[belongs_to(
    crate::activity_constraint::SelectActivityConstraint,
    foreign_key = "activity_id"
)]
#[belongs_to(
    crate::communication::SelectCommunication,
    foreign_key = "communication_id"
)]
#[primary_key(activity_id, communication_id)]
#[table_name = "activity_communication"]
pub struct ActivityCommunication {
    pub activity_id: Uuid,
    pub communication_id: Uuid,
    pub created_at: DateTime<Utc>,
}

#[derive(PartialEq, Debug, Clone, Queryable, Insertable, Serialize, Deserialize)]
#[table_name = "activity_communication"]
pub struct NewActivityCommunication {
    pub activity_id: Uuid,
    pub communication_id: Uuid,
}

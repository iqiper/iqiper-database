use crate::activity::select_activity_for_update;
use crate::contact::{SelectContact, CONTACT_SELECT_COLUMNS};
use crate::enums::ContactType;
use crate::schema::iqiper::{activity, activity_contact, contact};
use chrono::{DateTime, Utc};
use diesel::{
    Connection, ExpressionMethods, Insertable, JoinOnDsl, PgConnection, QueryDsl, RunQueryDsl,
};

use crate::error::IqiperDatabaseError;

use serde::{Deserialize, Serialize};

use uuid::Uuid;

/// Select every activity contacts for an user.
///
/// # Arguments
/// *`conn` - The postgres connection
/// *`uid` - The uuid of the `distant_user` record
///
/// # Return Value
///  A vector of activity contacts
pub fn select_all(
    conn: &PgConnection,
    uid: &Uuid,
) -> Result<Vec<SelectActivityContactWithContact>, IqiperDatabaseError> {
    Ok(activity_contact::table
        .inner_join(activity::table)
        .inner_join(contact::table)
        .filter(activity::columns::uid.eq(uid))
        .filter(contact::columns::uid.eq(uid))
        .order((
            activity_contact::columns::created_at.asc(),
            contact::columns::created_at.asc(),
            contact::columns::updated_at.asc(),
        ))
        .select(ACTIVITY_CONTACT_WITH_CONTACT_SELECT_COLUMNS)
        .load::<SelectActivityContactWithContact>(conn)?)
}

/// Select an activity id by its id joining the linked contact with it
///
/// # Arguments
/// *`conn` - The postgres connection
/// *`uid` - The uuid of the `distant_user` record
/// *`activity_id` - The uuid of the `activity` record
/// *`contact_id` - The uuid of the `contact` record
///
/// # Return Value
///  The selected activity contact + contact
pub fn select_by_id(
    conn: &PgConnection,
    uid: &Uuid,
    activity_id: &Uuid,
    contact_id: &Uuid,
) -> Result<SelectActivityContactWithContact, IqiperDatabaseError> {
    Ok(activity_contact::table
        .inner_join(contact::table)
        .order((
            activity_contact::columns::created_at.asc(),
            contact::columns::created_at.asc(),
            contact::columns::updated_at.asc(),
        ))
        .select(ACTIVITY_CONTACT_WITH_CONTACT_SELECT_COLUMNS)
        .inner_join(activity::table)
        .filter(activity::columns::uid.eq(uid))
        .filter(activity_contact::columns::contact_id.eq(contact_id))
        .filter(activity_contact::columns::activity_id.eq(activity_id))
        .first::<SelectActivityContactWithContact>(conn)?)
}

/// Select every contact linked to an activity (unlogged)
///
/// # Arguments
/// *`conn` - The postgres connection
/// *`uid` - The user id
/// *`activity_id` - The uuid of the `activity` record
///
/// # Return Value
///  The selected activity contact + contact
pub fn select_contacts_for_activity(
    conn: &PgConnection,
    uid: &Uuid,
    activity_id: &Uuid,
) -> Result<Vec<SelectContact>, IqiperDatabaseError> {
    Ok(activity_contact::table
        .inner_join(contact::table)
        .order((
            activity_contact::columns::created_at.asc(),
            contact::columns::created_at.asc(),
            contact::columns::updated_at.asc(),
        ))
        .filter(contact::columns::uid.eq(uid))
        .select(CONTACT_SELECT_COLUMNS)
        .filter(activity_contact::columns::activity_id.eq(activity_id))
        .load::<SelectContact>(conn)?)
}

/// Select every contact linked to an activity (unlogged)
///
/// # Arguments
/// *`conn` - The postgres connection
/// *`activity_id` - The uuid of the `activity` record
///
/// # Return Value
///  The selected activity contact + contact
pub fn select_contacts_for_activity_unlogged(
    conn: &PgConnection,
    activity_id: &Uuid,
) -> Result<Vec<SelectActivityContactWithContact>, IqiperDatabaseError> {
    Ok(activity_contact::table
        .inner_join(contact::table)
        .order((
            activity_contact::columns::created_at.asc(),
            contact::columns::created_at.asc(),
            contact::columns::updated_at.asc(),
        ))
        .select(ACTIVITY_CONTACT_WITH_CONTACT_SELECT_COLUMNS)
        .filter(activity_contact::columns::activity_id.eq(activity_id))
        .load::<SelectActivityContactWithContact>(conn)?)
}

/// Create a new activity contact in the database
///
/// # Arguments
/// *`conn` - The postgres connection
/// *`uid` - The uuid of the `distant_user` record
/// *`new_activity_contact` - The struct containing the new activity_contact informations
///
/// # Return Value
///  The created record
pub fn create(
    conn: &PgConnection,
    uid: &Uuid,
    new_activity_contact: NewActivityContact,
) -> Result<ActivityContact, IqiperDatabaseError> {
    conn.transaction::<ActivityContact, IqiperDatabaseError, _>(|| {
        select_activity_for_update(&conn, &new_activity_contact.activity_id)?;
        Ok(activity::table
            .inner_join(contact::table.on(activity::uid.eq(contact::uid)))
            .select((activity::columns::id, contact::columns::id))
            .filter(activity::columns::id.eq(new_activity_contact.activity_id))
            .filter(contact::columns::id.eq(new_activity_contact.contact_id))
            .filter(activity::columns::uid.eq(uid))
            .filter(contact::columns::uid.eq(uid))
            .insert_into(activity_contact::table)
            .returning(SELECT_COLUMNS)
            .into_columns((
                activity_contact::columns::activity_id,
                activity_contact::columns::contact_id,
            ))
            .get_result::<ActivityContact>(conn)?)
    })
}

/// Delete an activity contact
///
/// # Arguments
/// *`conn` - The postgres connection
/// *`uid` - The uuid of the `distant_user` record
/// *`activity_id` - The uuid of the record to pull out of the database
/// *`contact_id` - The uuid of the record to pull out of the database
///
///
/// # Return Value
///  The number of affected records
pub fn delete(
    conn: &PgConnection,
    uid: &Uuid,
    activity_id: &Uuid,
    contact_id: &Uuid,
) -> Result<usize, IqiperDatabaseError> {
    Ok(conn.transaction::<usize, IqiperDatabaseError, _>(move || {
        select_activity_for_update(&conn, &activity_id)?;
        activity_contact::table
            .inner_join(activity::table)
            .filter(activity::columns::uid.eq(uid))
            .filter(activity_contact::columns::contact_id.eq(contact_id))
            .filter(activity_contact::columns::activity_id.eq(activity_id))
            .select((
                activity_contact::columns::activity_id,
                activity_contact::columns::contact_id,
            ))
            .first::<(Uuid, Uuid)>(conn)?;
        Ok(
            diesel::delete(activity_contact::table.find((activity_id, contact_id)))
                .execute(conn)?,
        )
    })?)
}

#[derive(PartialEq, Debug, Clone, Queryable, Identifiable, Associations, Serialize, Deserialize)]
#[belongs_to(crate::contact::SelectContact, foreign_key = "contact_id")]
#[belongs_to(crate::activity::SelectActivity, foreign_key = "activity_id")]
#[primary_key(activity_id, contact_id)]
#[table_name = "activity_contact"]
pub struct ActivityContactId {
    pub activity_id: Uuid,
    pub contact_id: Uuid,
}

type ActivityContactColumns = (
    activity_contact::columns::activity_id,
    activity_contact::columns::contact_id,
    activity_contact::columns::created_at,
);

pub const SELECT_COLUMNS: ActivityContactColumns = (
    activity_contact::columns::activity_id,
    activity_contact::columns::contact_id,
    activity_contact::columns::created_at,
);

type ActivityContactWithContactColumns = (
    activity_contact::columns::activity_id,
    activity_contact::columns::contact_id,
    contact::columns::nickname,
    contact::columns::favorite,
    contact::columns::uid,
    contact::columns::type_,
    contact::columns::email,
    contact::columns::target_uid,
    contact::columns::created_at,
    contact::columns::updated_at,
);

pub const ACTIVITY_CONTACT_WITH_CONTACT_SELECT_COLUMNS: ActivityContactWithContactColumns = (
    activity_contact::columns::activity_id,
    activity_contact::columns::contact_id,
    contact::columns::nickname,
    contact::columns::favorite,
    contact::columns::uid,
    contact::columns::type_,
    contact::columns::email,
    contact::columns::target_uid,
    contact::columns::created_at,
    contact::columns::updated_at,
);

#[derive(
    PartialEq, Eq, Hash, Debug, Clone, Queryable, Identifiable, Associations, Serialize, Deserialize,
)]
#[belongs_to(crate::activity::SelectActivity, foreign_key = "activity_id")]
#[belongs_to(crate::contact::SelectContact, foreign_key = "contact_id")]
#[primary_key(activity_id, contact_id)]
#[table_name = "activity_contact"]
pub struct SelectActivityContactWithContact {
    #[serde(skip_serializing)]
    pub activity_id: Uuid,
    #[serde(rename(serialize = "id"))]
    pub contact_id: Uuid,
    pub nickname: String,
    pub favorite: bool,
    #[serde(skip_serializing)]
    pub uid: Uuid,
    #[serde(skip_serializing)]
    pub type_: ContactType,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub email: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub target_uid: Option<Uuid>,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
}

#[derive(
    PartialEq,
    Debug,
    Clone,
    Queryable,
    Identifiable,
    Associations,
    Insertable,
    Serialize,
    Deserialize,
)]
#[belongs_to(crate::activity::SelectActivity, foreign_key = "activity_id")]
#[belongs_to(crate::contact::SelectContact, foreign_key = "contact_id")]
#[primary_key(activity_id, contact_id)]
#[table_name = "activity_contact"]
pub struct ActivityContact {
    pub activity_id: Uuid,
    pub contact_id: Uuid,
    pub created_at: DateTime<Utc>,
}

#[derive(PartialEq, Debug, Clone, Queryable, Insertable, Serialize, Deserialize)]
#[table_name = "activity_contact"]
pub struct NewActivityContact {
    pub activity_id: Uuid,
    pub contact_id: Uuid,
}

use crate::activity_constraint as db_activity_constraint;
use crate::activity_constraint_communication;
use crate::db::schema::iqiper::activity_constraint;
use crate::enums::CommunicationReasonType;

use crate::common::{IdTsResponse, UpdateResponse};
use crate::diesel::Connection;
use crate::error::IqiperDatabaseError;
use crate::schema::iqiper::activity;
use chrono::{DateTime, Utc};
use diesel::sql_types::{BigInt, Timestamptz, Uuid as PgUuid};
use diesel::{ExpressionMethods, Insertable, PgConnection, QueryDsl, QueryableByName, RunQueryDsl};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

/// Check that an activity respect some basic logic before inserting it into
/// the database.
///
/// # Arguments
/// - `activity` - The activity that will be inserted
///
/// # Return Value
/// A result containing nothing
pub fn check_logic(activity: &NewActivity) -> Result<(), IqiperDatabaseError> {
    if activity.start_date.is_some() && activity.start_date.unwrap() < Utc::now() {
        return Err(IqiperDatabaseError::LogicError(String::from(
            "The start date can't be in the past",
        )));
    }
    Ok(())
}

/// Select an activity an mark it for update. Return an error if it's read-only
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `id` - The uuid of the activity
///
/// # Return Value
///  Nothing
pub fn select_activity_for_update(
    conn: &PgConnection,
    id: &Uuid,
) -> Result<(), IqiperDatabaseError> {
    let end_date = activity::table
        .find(id)
        .for_update()
        .select(activity::columns::end_date)
        .first::<Option<DateTime<Utc>>>(conn)?;
    match end_date {
        Some(_x) => Err(IqiperDatabaseError::ReadOnly),
        None => Ok(()),
    }
}

/// Select every activities for an user.
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
///
/// # Return Value
///  A vector of Activities
pub fn select_all(
    conn: &PgConnection,
    uid: &Uuid,
) -> Result<Vec<SelectActivity>, IqiperDatabaseError> {
    Ok(activity::table
        .filter(activity::columns::uid.eq(uid))
        .order((
            activity::columns::created_at.asc(),
            activity::columns::updated_at.asc(),
        ))
        .select(SELECT_COLUMNS)
        .load::<SelectActivity>(conn)?)
}

/// Select every activity. (paginated)
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `offset` - The offset at which to begin returning records
/// - `limit` - The number of record to return
///
/// # Return Value
///  A vector of activity
pub fn select_all_paginated_unlogged(
    conn: &PgConnection,
    offset: Option<(Uuid, DateTime<Utc>)>,
    limit: i64,
) -> Result<Vec<ActivityPaginated>, IqiperDatabaseError> {
    Ok(match offset {
        Some(x) => diesel::sql_query(include_str!(
            "../sql_queries/activity/select_all_paginated.sql"
        ))
        .bind::<Timestamptz, _>(x.1)
        .bind::<PgUuid, _>(x.0)
        .bind::<BigInt, _>(limit)
        .get_results(conn)?,
        None => diesel::sql_query(include_str!(
            "../sql_queries/activity/select_all_paginated_first.sql"
        ))
        .bind::<BigInt, _>(limit)
        .get_results(conn)?,
    })
}

/// Select a single activity
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `select_id` - The uuid of the record to pull out of the database
///
/// # Return Value
///  The selected activity
pub fn select_by_id(
    conn: &PgConnection,
    uid: &Uuid,
    select_id: &Uuid,
) -> Result<SelectActivity, IqiperDatabaseError> {
    Ok(activity::table
        .filter(activity::columns::uid.eq(uid))
        .filter(activity::columns::id.eq(select_id))
        .select(SELECT_COLUMNS)
        .first::<SelectActivity>(conn)?)
}

/// Select a single activity by activity constraint
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `select_id` - The uuid of constraint
///
/// # Return Value
///  The selected activity
pub fn select_by_activity_constraint_unlogged(
    conn: &PgConnection,
    select_id: &Uuid,
) -> Result<SelectActivity, IqiperDatabaseError> {
    Ok(activity::table
        .filter(
            activity::columns::id.eq_any(
                activity_constraint::table
                    .select(activity_constraint::columns::activity_id)
                    .find(select_id),
            ),
        )
        .select(SELECT_COLUMNS)
        .first::<SelectActivity>(conn)?)
}

/// Select a single activity (unlogged)
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `select_id` - The uuid of the record to pull out of the database
///
/// # Return Value
///  The selected activity
pub fn select_by_id_unlogged(
    conn: &PgConnection,
    select_id: &Uuid,
) -> Result<SelectActivity, IqiperDatabaseError> {
    Ok(activity::table
        .filter(activity::columns::id.eq(select_id))
        .select(SELECT_COLUMNS)
        .first::<SelectActivity>(conn)?)
}

/// Create a new activity in the database
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `new_activity` - The struct containing the new activity informations
///
/// # Return Value
///  The created record
pub fn create(
    conn: &PgConnection,
    new_activity: NewActivity,
) -> Result<IdTsResponse, IqiperDatabaseError> {
    check_logic(&new_activity)?;
    create_no_logic_check(&conn, new_activity)
}

/// Create a new activity in the database (no logic check)
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `new_activity` - The struct containing the new activity informations
///
/// # Return Value
///  The created record
pub fn create_no_logic_check(
    conn: &PgConnection,
    new_activity: NewActivity,
) -> Result<IdTsResponse, IqiperDatabaseError> {
    Ok(diesel::insert_into(activity::table)
        .values(NewActivityInternal {
            uid: new_activity.uid,
            name: new_activity.name,
            icon: new_activity.icon,
            alert_message: new_activity.alert_message,
            start_date: new_activity.start_date.unwrap_or_else(|| Utc::now()),
        })
        .returning((activity::columns::id, activity::columns::created_at))
        .get_result::<IdTsResponse>(conn)?)
}

/// Update the name of an activity
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `activity_id` - The uuid of the record to pull out of the database
/// - `name` - The new activity name
///
///
/// # Return Value
///  The uuid and update timestamp of the activity
pub fn update_name(
    conn: &PgConnection,
    uid: &Uuid,
    activity_id: &Uuid,
    name: String,
) -> Result<UpdateResponse, IqiperDatabaseError> {
    conn.transaction::<UpdateResponse, IqiperDatabaseError, _>(|| {
        select_activity_for_update(&conn, &activity_id)?;
        Ok(diesel::update(
            activity::table
                .filter(activity::columns::uid.eq(uid))
                .find(activity_id),
        )
        .set(activity::columns::name.eq(name))
        .returning(activity::columns::updated_at)
        .get_result(conn)
        .map(|x| UpdateResponse {
            id: *activity_id,
            updated_at: x,
        })?)
    })
}

/// Update the message of an activity
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `activity_id` - The uuid of the record to pull out of the database
/// - `alert_message` - The new activity message, if any
///
///
/// # Return Value
///  The uuid and update timestamp of the activity
pub fn update_alert_message(
    conn: &PgConnection,
    uid: &Uuid,
    activity_id: &Uuid,
    alert_message: Option<String>,
) -> Result<UpdateResponse, IqiperDatabaseError> {
    conn.transaction::<UpdateResponse, IqiperDatabaseError, _>(|| {
        select_activity_for_update(&conn, &activity_id)?;
        Ok(diesel::update(
            activity::table
                .filter(activity::columns::uid.eq(uid))
                .find(activity_id),
        )
        .set(activity::columns::alert_message.eq(alert_message))
        .returning(activity::columns::updated_at)
        .get_result(conn)
        .map(|x| UpdateResponse {
            id: *activity_id,
            updated_at: x,
        })?)
    })
}

/// Update the icon of an activity
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `activity_id` - The uuid of the record to pull out of the database
/// - `icon` - The new activity icon, if any
///
///
/// # Return Value
///  The uuid and update timestamp of the activity
pub fn update_icon(
    conn: &PgConnection,
    uid: &Uuid,
    activity_id: &Uuid,
    icon: Option<i32>,
) -> Result<UpdateResponse, IqiperDatabaseError> {
    conn.transaction::<UpdateResponse, IqiperDatabaseError, _>(|| {
        select_activity_for_update(&conn, &activity_id)?;
        Ok(diesel::update(
            activity::table
                .filter(activity::columns::uid.eq(uid))
                .find(activity_id),
        )
        .set(activity::columns::icon.eq(icon))
        .returning(activity::columns::updated_at)
        .get_result(conn)
        .map(|x| UpdateResponse {
            id: *activity_id,
            updated_at: x,
        })?)
    })
}

/// Set the end date for an activity
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `select_id` - The struct containing the activity to update
///
/// # Return Value
///  The uuid and update timestamp of the activity
pub fn end(
    conn: &PgConnection,
    uid: &Uuid,
    select_id: &Uuid,
) -> Result<UpdateResponse, IqiperDatabaseError> {
    Ok(diesel::update(
        activity::table
            .filter(activity::columns::uid.eq(uid))
            .filter(activity::columns::end_date.is_null())
            .find(select_id),
    )
    .set(activity::columns::end_date.eq(chrono::Utc::now()))
    .returning((activity::columns::id, activity::columns::updated_at))
    .get_result(conn)?)
}

/// End activity if every constraint has been alerted once or has expired
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `select_id` - The struct containing the activity to update
///
/// # Return Value
///  The uuid and update timestamp of the activity
pub fn end_if_done(
    conn: &PgConnection,
    uid: &Uuid,
    activity_id: &Uuid,
) -> Result<Option<UpdateResponse>, IqiperDatabaseError> {
    let res = conn.transaction::<UpdateResponse, IqiperDatabaseError, _>(|| {
        let now = Utc::now();
        select_activity_for_update(&conn, &activity_id)?;
        let constraints =
            db_activity_constraint::select_all_by_activity(&conn, &uid, &activity_id)?;
        for constraint in constraints.iter() {
            let constraints_communications = activity_constraint_communication::select_all(
                &conn,
                &uid,
                &activity_id,
                &constraint.id,
            )?;
            let mut has_alert = false;
            for comm in constraints_communications.iter() {
                match comm.reason {
                    CommunicationReasonType::Alert | CommunicationReasonType::ForcedAlert => {
                        has_alert = true;
                        break;
                    }
                    _ => (),
                };
            }
            if !has_alert {
                if constraint.end_date > now {
                    // The constraint hasn't ended
                    return Err(IqiperDatabaseError::from(
                        diesel::result::Error::RollbackTransaction,
                    ));
                } else {
                    continue;
                }
            }
        }
        Ok(end(&conn, &uid, &activity_id)?)
    });
    match res {
        Ok(x) => Ok(Some(x)),
        Err(IqiperDatabaseError::DatabaseError(diesel::result::Error::RollbackTransaction)) => {
            Ok(None)
        }
        Err(x) => Err(x),
    }
}

/// Delete an activity
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `activity_id` - The uuid of the record to pull out of the database
///
///
/// # Return Value
///  The number of affected records
pub fn delete(
    conn: &PgConnection,
    uid: &Uuid,
    activity_id: Uuid,
) -> Result<usize, IqiperDatabaseError> {
    Ok(diesel::delete(
        activity::table
            .filter(activity::columns::uid.eq(uid))
            .find(activity_id),
    )
    .execute(conn)?)
}

type ActivityColumns = (
    activity::columns::id,
    activity::columns::uid,
    activity::columns::name,
    activity::columns::icon,
    activity::columns::alert_message,
    activity::columns::start_date,
    activity::columns::end_date,
    activity::columns::created_at,
    activity::columns::updated_at,
);

pub const SELECT_COLUMNS: ActivityColumns = (
    activity::columns::id,
    activity::columns::uid,
    activity::columns::name,
    activity::columns::icon,
    activity::columns::alert_message,
    activity::columns::start_date,
    activity::columns::end_date,
    activity::columns::created_at,
    activity::columns::updated_at,
);

#[derive(
    PartialEq,
    Debug,
    Clone,
    Queryable,
    QueryableByName,
    Identifiable,
    Serialize,
    Deserialize,
    Associations,
)]
#[belongs_to(crate::distant_user::SelectUser, foreign_key = "uid")]
#[table_name = "activity"]
pub struct SelectActivity {
    pub id: Uuid,
    #[serde(skip_serializing)]
    pub uid: Uuid,
    pub name: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub icon: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub alert_message: Option<String>,
    pub start_date: DateTime<Utc>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub end_date: Option<DateTime<Utc>>,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
}

#[derive(PartialEq, Debug, Clone, Serialize, Deserialize)]
pub struct NewActivity {
    pub name: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub icon: Option<i32>,
    pub uid: Uuid,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub alert_message: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub start_date: Option<DateTime<Utc>>,
}

#[derive(PartialEq, Debug, Clone, Serialize, Deserialize)]
pub struct NewActivityWithoutUid {
    pub name: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub icon: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub alert_message: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub start_date: Option<DateTime<Utc>>,
}

#[derive(PartialEq, Debug, Clone, Serialize, Insertable)]
#[table_name = "activity"]
pub struct NewActivityInternal {
    pub name: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub icon: Option<i32>,
    pub uid: Uuid,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub alert_message: Option<String>,
    pub start_date: DateTime<Utc>,
}

#[derive(PartialEq, Debug, Clone, Serialize, Deserialize, QueryableByName)]
#[table_name = "activity"]
pub struct ActivityPaginated {
    pub id: Uuid,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
}

use crate::common::{IdTsResponse, UpdateResponse};
use crate::error::IqiperDatabaseError;
use crate::schema::iqiper::activity_template;
use chrono::{DateTime, Utc};
use diesel::{ExpressionMethods, Insertable, PgConnection, QueryDsl, RunQueryDsl};

use serde::{Deserialize, Serialize};

use uuid::Uuid;

/// Select every activity templates for an user.
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
///
/// # Return Value
///  A vector of activity templates
pub fn select_all(
    conn: &PgConnection,
    uid: &Uuid,
) -> Result<Vec<SelectActivityTemplate>, IqiperDatabaseError> {
    Ok(activity_template::table
        .filter(activity_template::columns::uid.eq(uid))
        .order((
            activity_template::columns::created_at.asc(),
            activity_template::columns::updated_at.asc(),
        ))
        .load::<SelectActivityTemplate>(conn)?)
}

/// Select a single activity template
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `select_id` - The uuid of the record to pull out of the database
///
/// # Return Value
///  The selected activity template or `None` if not found
pub fn select_by_id(
    conn: &PgConnection,
    uid: &Uuid,
    select_id: &Uuid,
) -> Result<SelectActivityTemplate, IqiperDatabaseError> {
    Ok(activity_template::table
        .filter(activity_template::columns::uid.eq(uid))
        .filter(activity_template::columns::id.eq(select_id))
        .first::<SelectActivityTemplate>(conn)?)
}

/// Create a new activity template in the database
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `new_activity_template` - The struct containing the new activity template informations
///
/// # Return Value
///  The created record
pub fn create(
    conn: &PgConnection,
    new_activity_template: NewActivityTemplate,
) -> Result<IdTsResponse, IqiperDatabaseError> {
    Ok(diesel::insert_into(activity_template::table)
        .values(NewActivityTemplateInternal {
            name: new_activity_template.name,
            icon: new_activity_template.icon,
            uid: new_activity_template.uid,
            alert_message: new_activity_template.alert_message,
        })
        .returning((
            activity_template::columns::id,
            activity_template::columns::created_at,
        ))
        .get_result::<IdTsResponse>(conn)?)
}

/// Update the name of an activity template
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `activity_template_id` - The uuid of the record to pull out of the database
/// - `name` - The new activity template name
///
///
/// # Return Value
///  The uuid and update timestamp of the activity template
pub fn update_name(
    conn: &PgConnection,
    uid: &Uuid,
    activity_template_id: &Uuid,
    name: String,
) -> Result<UpdateResponse, IqiperDatabaseError> {
    Ok(diesel::update(
        activity_template::table
            .filter(activity_template::columns::uid.eq(uid))
            .find(activity_template_id),
    )
    .set(activity_template::columns::name.eq(name.clone()))
    .returning((
        activity_template::columns::id,
        activity_template::columns::updated_at,
    ))
    .get_result::<UpdateResponse>(conn)?)
}

/// Update the message of an activity template
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `activity_template_id` - The uuid of the record to pull out of the database
/// - `alert_message` - The new activity template message, if any
///
///
/// # Return Value
///  The uuid and update timestamp of the activity template
pub fn update_alert_message(
    conn: &PgConnection,
    uid: &Uuid,
    activity_template_id: &Uuid,
    alert_message: Option<String>,
) -> Result<UpdateResponse, IqiperDatabaseError> {
    Ok(diesel::update(
        activity_template::table
            .filter(activity_template::columns::uid.eq(uid))
            .find(activity_template_id),
    )
    .set(activity_template::columns::alert_message.eq(alert_message.clone()))
    .returning((
        activity_template::columns::id,
        activity_template::columns::updated_at,
    ))
    .get_result::<UpdateResponse>(conn)?)
}

/// Update the icon of an activity template
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `activity_template_id` - The uuid of the record to pull out of the database
/// - `icon` - The new activity template icon, if any
///
/// # Return Value
///  The uuid and update timestamp of the activity template
pub fn update_icon(
    conn: &PgConnection,
    uid: &Uuid,
    activity_template_id: &Uuid,
    icon: Option<i32>,
) -> Result<UpdateResponse, IqiperDatabaseError> {
    Ok(diesel::update(
        activity_template::table
            .filter(activity_template::columns::uid.eq(uid))
            .find(activity_template_id),
    )
    .set(activity_template::columns::icon.eq(icon))
    .returning((
        activity_template::columns::id,
        activity_template::columns::updated_at,
    ))
    .get_result::<UpdateResponse>(conn)?)
}

/// Delete an activity template
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `activity_template_id` - The uuid of the record to pull out of the database
///
/// # Return Value
///  The number of affected records
pub fn delete(
    conn: &PgConnection,
    uid: &Uuid,
    activity_template_id: &Uuid,
) -> Result<usize, IqiperDatabaseError> {
    Ok(diesel::delete(
        activity_template::table
            .filter(activity_template::columns::uid.eq(uid))
            .find(activity_template_id),
    )
    .execute(conn)?)
}

#[derive(PartialEq, Debug, Clone, Queryable, Identifiable, Serialize, Associations, Deserialize)]
#[belongs_to(crate::distant_user::UserId, foreign_key = "uid")]
#[table_name = "activity_template"]
pub struct SelectActivityTemplate {
    pub id: Uuid,
    #[serde(skip_serializing)]
    pub uid: Uuid,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub icon: Option<i32>,
    pub name: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub alert_message: Option<String>,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
}

#[derive(PartialEq, Debug, Clone, Serialize, Deserialize)]
pub struct NewActivityTemplate {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub icon: Option<i32>,
    pub name: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub alert_message: Option<String>,
    pub uid: Uuid,
}

#[derive(PartialEq, Debug, Clone, Serialize, Deserialize)]
pub struct NewActivityTemplateWithoutUid {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub icon: Option<i32>,
    pub name: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub alert_message: Option<String>,
}

#[derive(PartialEq, Debug, Clone, Insertable, Serialize)]
#[table_name = "activity_template"]
struct NewActivityTemplateInternal {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub icon: Option<i32>,
    pub name: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub alert_message: Option<String>,
    pub uid: Uuid,
}

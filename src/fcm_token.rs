use crate::error::IqiperDatabaseError;
use crate::schema::iqiper::fcm_token;
use chrono::{DateTime, Utc};
use diesel::{PgConnection, QueryDsl, RunQueryDsl};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Insertable, Associations, AsChangeset)]
#[belongs_to(crate::distant_user::UserId, foreign_key = "uid")]
#[table_name = "fcm_token"]
pub struct NewToken {
    pub uid: Uuid,
    pub device_id: Uuid,
    pub token: String,
}

#[derive(PartialEq, Debug, Clone, Queryable, Deserialize, Serialize)]
pub struct GetTokenRecord {
    pub uid: Uuid,
    pub device_id: Uuid,
    pub updated_at: DateTime<Utc>,
}

#[derive(PartialEq, Debug, Clone, Queryable, Deserialize, Serialize)]
pub struct DeletedTokenRecord {
    pub uid: Uuid,
    pub device_id: Uuid,
}

/// Create or update a `fcm_token` in the database
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `new_token` - The token object
///
/// # Return Value
///  The created/updated record
pub fn upsert_fcm_token(
    conn: &PgConnection,
    new_token: NewToken,
) -> Result<GetTokenRecord, IqiperDatabaseError> {
    Ok(diesel::insert_into(fcm_token::table)
        .values(&new_token)
        .on_conflict((fcm_token::columns::uid, fcm_token::columns::device_id))
        .do_update()
        .set(&new_token)
        .returning((
            fcm_token::columns::uid,
            fcm_token::columns::device_id,
            fcm_token::columns::updated_at, //TODO Also return the updated_at
        ))
        .get_result::<GetTokenRecord>(conn)?)
}

/// Delete an `fcm_token` from the database
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `device_id` - The uuid of the device to delete
///
/// # Return Value
///  The user uuid and the device id
pub fn delete_fcm_token(
    conn: &PgConnection,
    uid: &Uuid,
    device_id: &Uuid,
) -> Result<DeletedTokenRecord, IqiperDatabaseError> {
    Ok(diesel::delete(fcm_token::table.find((uid, device_id)))
        .returning((fcm_token::columns::uid, fcm_token::columns::device_id))
        .get_result::<DeletedTokenRecord>(conn)?)
}

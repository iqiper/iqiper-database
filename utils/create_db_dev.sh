export PGUSER=postgres
export PGPASSWORD=postgres
psql -U postgres -h $1 -d postgres -c "DROP DATABASE IF EXISTS iqiper;"
psql -U postgres -h $1 -d postgres -c "DROP ROLE iqiper;"
psql -U postgres -h $1 -d postgres -c "CREATE ROLE iqiper with LOGIN ENCRYPTED PASSWORD 'iqiper';"
psql -U postgres -h $1 -d postgres -c "CREATE DATABASE iqiper WITH OWNER iqiper;"
psql -U postgres -h $1 -d iqiper -c "CREATE EXTENSION \"uuid-ossp\";"
export PGPASSWORD=iqiper
psql -U iqiper -h $1 -d iqiper -c "CREATE SCHEMA iqiper;"

#!/bin/bash
DIR=$(dirname "$(readlink -f "$0")")
source $(realpath $DIR/../.env.local)
set -x
TMP=$(mktemp)
pg_dump -U postgres -h localhost -p 5432 -v -Fc -s -f $TMP iqiper
dropdb -U postgres -h localhost -p 5432 iqiper
createdb -U postgres -h localhost -p 5432 iqiper
pg_restore -U postgres -h localhost -p 5432 -v -d iqiper $TMP
rm -f $TMP

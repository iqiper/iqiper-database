#!/bin/bash

while diesel migration list | grep "[X]" > /dev/null;
do
    diesel migration revert
done

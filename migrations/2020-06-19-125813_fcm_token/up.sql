CREATE TABLE fcm_token(
	uid			UUID NOT NULL REFERENCES distant_user(id) ON DELETE CASCADE,
	device_id	UUID NOT NULL,
	token		TEXT NOT NULL CHECK (token <> ''),
	created_at	TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	updated_at	TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	PRIMARY KEY (uid, device_id)
);

SELECT diesel_manage_updated_at('fcm_token');

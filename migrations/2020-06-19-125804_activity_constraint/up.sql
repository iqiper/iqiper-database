CREATE TYPE ACTIVITY_CONSTRAINT_TYPE AS ENUM (
		'TIMER');

CREATE TABLE activity_constraint(
	id						UUID NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
	activity_id				UUID NOT NULL REFERENCES activity(id) ON DELETE CASCADE,
	type					ACTIVITY_CONSTRAINT_TYPE NOT NULL,

	alert_message 	VARCHAR(1024),

	start_date				TIMESTAMPTZ NOT NULL,
	end_date				TIMESTAMPTZ NOT NULL,
	
	notify_date		TIMESTAMPTZ,
	CHECK ((type = 'TIMER' AND notify_date IS NOT NULL) OR (type != 'TIMER' AND notify_date IS NULL)),

	created_at				TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	updated_at				TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

SELECT diesel_manage_updated_at('activity_constraint');

CREATE TABLE activity_communication(
	activity_id						UUID NOT NULL REFERENCES activity(id) ON DELETE CASCADE,

	communication_id				UUID NOT NULL REFERENCES communication(id) ON DELETE CASCADE,

	created_at						TIMESTAMPTZ NOT NULL DEFAULT NOW(),

	PRIMARY KEY (activity_id, communication_id)
);

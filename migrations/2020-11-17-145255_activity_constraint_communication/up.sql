CREATE TABLE activity_constraint_communication(
	constraint_id					UUID NOT NULL REFERENCES activity_constraint(id) ON DELETE CASCADE,

	communication_id				UUID NOT NULL REFERENCES communication(id) ON DELETE CASCADE,

	created_at						TIMESTAMPTZ NOT NULL DEFAULT NOW(),

	PRIMARY KEY (constraint_id, communication_id)
);

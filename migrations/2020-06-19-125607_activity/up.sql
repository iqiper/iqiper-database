CREATE TABLE activity(
	id				UUID NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
	uid				UUID NOT NULL REFERENCES distant_user(id) ON DELETE CASCADE,
	name			VARCHAR(100) NOT NULL CHECK (name <> ''),
 	icon			INT,
	alert_message 	VARCHAR(1024),
	start_date		TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	end_date		TIMESTAMPTZ,
	created_at		TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	updated_at		TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

SELECT diesel_manage_updated_at('activity');

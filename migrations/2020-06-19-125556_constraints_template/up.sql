CREATE TYPE ACTIVITY_CONSTRAINT_TEMPLATE_TYPE AS ENUM ('TIMER');

CREATE TABLE activity_constraint_template(
	id						UUID NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
	activity_template_id	UUID NOT NULL REFERENCES activity_template(id) ON DELETE CASCADE,

	type					ACTIVITY_CONSTRAINT_TEMPLATE_TYPE NOT NULL,

	start_offset			INTEGER NOT NULL,
	end_offset				INTEGER NOT NULL,

	alert_message 			VARCHAR(1024),
	
	notify_offset			INTEGER,
	CHECK ((type = 'TIMER' AND notify_offset IS NOT NULL) OR (type != 'TIMER' AND notify_offset IS NULL)),

	created_at				TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	updated_at				TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

SELECT diesel_manage_updated_at('activity_constraint_template');

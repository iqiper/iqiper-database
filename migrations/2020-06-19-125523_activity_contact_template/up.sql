CREATE TABLE activity_contact_template(
	activity_template_id	UUID NOT NULL REFERENCES activity_template(id) ON DELETE CASCADE,
	contact_id				UUID NOT NULL REFERENCES contact(id) ON DELETE CASCADE,
	created_at				TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	PRIMARY KEY (activity_template_id, contact_id)
);

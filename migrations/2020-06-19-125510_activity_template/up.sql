CREATE TABLE activity_template(
	id			UUID NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
	uid			UUID NOT NULL REFERENCES distant_user(id) ON DELETE CASCADE,
	icon		INT,
	name		VARCHAR(100) NOT NULL CHECK (name <> ''), 
	alert_message 			VARCHAR(1024),
	created_at	TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	updated_at	TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

SELECT diesel_manage_updated_at('activity_template');

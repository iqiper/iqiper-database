CREATE TABLE activity_contact(
	activity_id				UUID NOT NULL REFERENCES activity(id) ON DELETE CASCADE,
	contact_id				UUID NOT NULL REFERENCES contact(id) ON DELETE CASCADE,
	created_at				TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	PRIMARY KEY (activity_id, contact_id)
);

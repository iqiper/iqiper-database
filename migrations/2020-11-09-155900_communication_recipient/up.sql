CREATE TABLE communication_recipient(
	communication_id		UUID NOT NULL REFERENCES communication(id) ON DELETE CASCADE,

	contact_id				UUID NOT NULL REFERENCES contact(id) ON DELETE CASCADE,

	PRIMARY KEY (communication_id, contact_id)
);

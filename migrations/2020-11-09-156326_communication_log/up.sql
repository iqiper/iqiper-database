CREATE TYPE COMMUNICATION_STATUS AS ENUM (
		'FAILED',
		'RETRYING',
		'SENT'
	);

CREATE TABLE communication_log(
	id								UUID NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),

	communication_id				UUID NOT NULL REFERENCES communication(id) ON DELETE CASCADE,

	contact_id						UUID NOT NULL REFERENCES contact(id) ON DELETE CASCADE,

	status							COMMUNICATION_STATUS NOT NULL,

	created_at						TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

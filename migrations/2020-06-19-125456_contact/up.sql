CREATE TYPE CONTACT_TYPE AS ENUM (
		'EMAIL',
		'FRIEND');

CREATE TABLE contact(
	id					UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
	nickname			VARCHAR(64) NOT NULL CHECK (nickname <> ''),
	favorite			BOOLEAN NOT NULL DEFAULT FALSE,
	uid					UUID NOT NULL REFERENCES distant_user(id) ON DELETE CASCADE,

	type				CONTACT_TYPE NOT NULL,

	email				VARCHAR(320) CHECK (email <> ''),
	CHECK ((type = 'EMAIL' AND email IS NOT NULL) OR (type != 'EMAIL' AND email IS NULL)),

	target_uid			UUID REFERENCES distant_user(id) ON DELETE CASCADE,
	CHECK (target_uid != uid),
	CHECK ((type = 'FRIEND' AND target_uid IS NOT NULL) OR (type != 'FRIEND' AND target_uid IS NULL)),

	created_at	TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	updated_at	TIMESTAMPTZ NOT NULL DEFAULT NOW(),

	CONSTRAINT unq_email_friend			UNIQUE(uid, email),
	CONSTRAINT unq_target_uid_friend	UNIQUE(uid, target_uid)

);

SELECT diesel_manage_updated_at('contact');

use chrono::{DateTime, Duration, Utc};
use db::activity::{self as activity_db, NewActivity};
use db::activity_constraint::{self as activity_constraint_db, NewActivityConstraint};
use db::activity_contact::{self as activity_contact_db, NewActivityContact};
use db::contact::{self as contact_db, NewContact};
use db::enums::ActivityConstraintType;
use db::enums::ContactType;
use diesel::r2d2::{ConnectionManager, CustomizeConnection};
use diesel::{sql_query, Connection, PgConnection, RunQueryDsl};
use r2d2::Pool;
use uuid::Uuid;

#[derive(Debug)]
struct TestTransaction;

impl CustomizeConnection<diesel::PgConnection, diesel::r2d2::Error> for TestTransaction {
    fn on_acquire(
        &self,
        conn: &mut diesel::PgConnection,
    ) -> ::std::result::Result<(), diesel::r2d2::Error> {
        conn.begin_test_transaction().unwrap();
        Ok(())
    }
}

pub fn establish_connection() -> PgConnection {
    let database_url = std::env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    PgConnection::establish(&database_url).expect(&format!("Error connecting to {}", database_url))
}

pub fn establish_connection_pool() -> Pool<ConnectionManager<PgConnection>> {
    let manager =
        ConnectionManager::new(std::env::var("DATABASE_URL").expect("DATABASE_URL must be set"));
	Pool::builder()
		.connection_customizer(Box::new(TestTransaction))
        .max_size(3)
        .build(manager)
        .expect("the pool should've been built")
}

pub fn create_test_user(connection: &PgConnection) -> Uuid {
    let user_uuid = Uuid::new_v4();
    sql_query("INSERT INTO iqiper.distant_user (id) VALUES ($1);")
        .bind::<diesel::sql_types::Uuid, _>(user_uuid)
        .execute(connection)
        .expect("Inserting user failed");
    user_uuid
}

pub fn create_activity(
    conn: &PgConnection,
    uid: &Uuid,
    check_logic: bool,
	start_date: Option<DateTime<Utc>>,
	alert_message: Option<String>
) -> Uuid {
    let expected = NewActivity {
        name: format!("going to work {}", Uuid::new_v4()),
		icon: None,
		alert_message,
        uid: *uid,
        start_date,
    };
    let created_contact;
    if check_logic {
        created_contact = activity_db::create(conn, expected.clone());
    } else {
        created_contact = activity_db::create_no_logic_check(conn, expected.clone());
    }
    let created = created_contact.expect("activity should've been created");
    created.id
}

pub fn create_activity_constraint(
    conn: &PgConnection,
    uid: &Uuid,
    activity_id: &Uuid,
    check_logic: bool,
    start_date: Option<DateTime<Utc>>,
    notify_date: Option<DateTime<Utc>>,
	end_date: Option<DateTime<Utc>>,
	alert_message: Option<String>,
) -> Uuid {
    let expected_contact = NewActivityConstraint {
        activity_id: activity_id.clone(),
		type_: ActivityConstraintType::Timer,
		alert_message,
        start_date: start_date.unwrap_or(Utc::now()),
        notify_date: Some(notify_date.unwrap_or(Utc::now())),
        end_date: end_date.unwrap_or(Utc::now() + Duration::seconds(10)),
    };
    let created_activity_constraint;
    if check_logic {
        created_activity_constraint =
            activity_constraint_db::create(conn, &uid, expected_contact.clone())
                .expect("activity_constraint should've been created");
    } else {
        created_activity_constraint =
            activity_constraint_db::create_no_logic_check(conn, &uid, expected_contact.clone())
                .expect("activity_constraint should've been created");
    }
    created_activity_constraint.id
}

pub fn create_contact(conn: &PgConnection, uid: &Uuid) -> Uuid {
    let expected_contact = NewContact {
        nickname: String::from("Hello world"),
        favorite: false,
        uid: *uid,
        type_: ContactType::Email,
        email: Some(format!("{}@world", Uuid::new_v4())),
        target_uid: None,
    };
    let created_contact =
        contact_db::create(conn, expected_contact.clone()).expect("contact should've been created");
    assert_eq!(
        expected_contact.nickname, created_contact.nickname,
        "names do not match"
    );
    assert_eq!(
        expected_contact.uid, created_contact.uid,
        "uids do not match"
    );
    assert_eq!(
        expected_contact.favorite, created_contact.favorite,
        "favorite do not match"
    );
    assert_eq!(
        expected_contact.type_, created_contact.type_,
        "type do not match"
    );
    assert_eq!(
        expected_contact.email, created_contact.email,
        "emails do not match"
    );
    assert_eq!(
        expected_contact.target_uid, created_contact.target_uid,
        "target_uids do not match"
    );
    created_contact.id
}

pub fn create_custom_contact_email(conn: &PgConnection, uid: &Uuid, email: String) -> Uuid {
    let expected_contact = NewContact {
        nickname: String::from("Hello world"),
        favorite: false,
        uid: *uid,
        type_: ContactType::Email,
        email: Some(email),
        target_uid: None,
    };
    let created_contact =
        contact_db::create(conn, expected_contact.clone()).expect("contact should've been created");
    created_contact.id
}

pub fn create_custom_contact_friend(conn: &PgConnection, uid: &Uuid, friend: Uuid) -> Uuid {
    let expected_contact = NewContact {
        nickname: String::from("Hello world"),
        favorite: false,
        uid: *uid,
        type_: ContactType::Friend,
        email: None,
        target_uid: Some(friend),
    };
    let created_contact =
        contact_db::create(conn, expected_contact.clone()).expect("contact should've been created");
    created_contact.id
}

pub fn create_activity_contact(
    conn: &PgConnection,
    uid: &Uuid,
    activity_id: &Uuid,
    contact_id: &Uuid,
) -> (Uuid, Uuid) {
    let expected = NewActivityContact {
        activity_id: *activity_id,
        contact_id: *contact_id,
    };
    let created_contact = activity_contact_db::create(conn, uid, expected.clone())
        .expect("activity_report should've been created");
    assert_eq!(
        expected.activity_id, created_contact.activity_id,
        "activity_ids do not match"
    );
    assert_eq!(
        expected.contact_id, created_contact.contact_id,
        "contact_ids do not match"
    );
    (created_contact.activity_id, created_contact.contact_id)
}

pub fn delete_test_user(connection: &PgConnection, user_uuid: &Uuid) {
    sql_query("DELETE FROM iqiper.distant_user WHERE id = $1;")
        .bind::<diesel::sql_types::Uuid, _>(user_uuid)
        .execute(connection)
        .expect("Deleting user failed");
}
